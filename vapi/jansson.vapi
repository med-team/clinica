/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

[CCode(cheader_filename="jansson.h", lower_case_cprefix="json_", cprefix="")]
namespace Jansson {

	[CCode (cname = "size_t", cprefix="JSON_")]
	public enum JsonFlags {
		COMPACT,
		ENSURE_ASCII,
		SORT_KEYS,
		PRESERVE_ORDER,
		ENCODE_ANY,
		ESCAPE_SLASH
	}
	
	[CCode(cname="json_t", unref_function = "json_decref", ref_function = "json_incref", 
	       lower_case_cprefix="json_")]
	public class Json {
		public static Json loads(string text, size_t flags, out Error error);
		public static Json load_file(string filename, size_t flags, out Error error);
		public string dumps(JsonFlags flags = 0);
		public int dump_file (string output, JsonFlags flags = 0);
		public static Json integer(int64 value);
		public static Json real(double value);
		public static Json string(string value);
		public static Json object();
		public static Json array();
		public Json copy();
		public Json deep_copy();
		public bool is_array();
		public int array_size();
		public int array_append(Json value);
		public unowned Json array_get(int index);
		public bool is_object();
		public bool is_string();
		public bool is_integer();
		public bool is_real();
		public bool is_true();
		public bool is_false();
		public bool is_null();
		public bool is_number();
		public bool is_boolean();
		public unowned string string_value();
		public unowned int64 integer_value();
		public unowned double real_value();
		public unowned Json object_get(string field);
		public int object_set(string key, Json value);
	}

	[CCode(cname="json_error_t")]
	public struct Error {
	}
}
