#!/bin/bash
#
# Build a Windows installer for Clinica using NSIS.
# The current version of Clinica is downloaded from the most
# recent one compiled by the OpenSUSE Build Service.
#
# You can check how recent that is by visiting http://build.opensuse.org/
#
# Author: Leonardo Robol <leo@robol.it>
#
# Please bear in mind that this script is not meant to be totally bullet-proof.
# It's more like a guide on the installer creation process.

OPENSUSE_VERSION="openSUSE_12.2"
NSIS=`find ~/.wine/ -name makensis.exe -print0`

if [ "$NSIS" == "" ]; then
  echo "makensis.exe not found in ~/.wine. Please install NSIS"
  exit 1
fi

SCRIPT=$(readlink -f $0)
SCRIPTPATH=$(dirname $SCRIPT)

if [ ! -x "download-mingw-rpm.py" ]; then
  wget -q https://raw.github.com/mkbosmans/download-mingw-rpm/master/download-mingw-rpm.py
  chmod a+x download-mingw-rpm.py
fi

rm -rf usr

python3 download-mingw-rpm.py -p home:lrobol -r "$OPENSUSE_VERSION" --deps mingw32-clinica
python3 download-mingw-rpm.py --no-clean -r "$OPENSUSE_VERSION" --deps \
	mingw32-gtk3 mingw32-sqlite mingw32-glib2 mingw32-rsvg2 mingw32-libgee \
	mingw32-librsvg mingw32-libsoup\

# Compile GSettings schema
glib-compile-schemas usr/i686-w64-mingw32/sys-root/mingw/share/glib-2.0/schemas/

# Create NSIS installer
cd usr/i686-w64-mingw32/sys-root/mingw/
cat $SCRIPTPATH/clinica-installer.nsi | $NSIS -

# Copy the result back  :)
mv SetupClinica.exe ../../../../
