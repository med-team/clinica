; Clinica.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

!include "MUI2.nsh"

!define MUI_COMPONENTSPAGE_SMALLDESC "Clinica medical records manager";
; !define MUI_UI "SetupClinica.exe" ;
!define MUI_INSTFILESPAGE_COLORS "AAAAFF 000000" ;

;--------------------------------

; The name of the installer
Name "Clinica"

; The file to write
OutFile "SetupClinica.exe"

; The default installation directory
InstallDir $PROGRAMFILES\PhcTeam\Clinica

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\NSIS_Clinica" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

Var StartMenuFolder

!define MUI_ABORTWARNING

!insertmacro MUI_LANGUAGE "English"

;--------------------------------

; Pages

  !insertmacro MUI_PAGE_LICENSE "/usr/share/common-licenses/GPL-3"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY

;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\NSIS_Clinica" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Clinica"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------

; The stuff to install
Section "Clinica" SecClinica

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File /r *

  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\NSIS_Clinica "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Clinica" "Clinica" "Clinica"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Clinica" "Uninstall" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Clinica" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Clinica" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

  SetOutPath $INSTDIR\bin

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    CreateDirectory "$SMPROGRAMS\Clinica"
    CreateShortCut "$SMPROGRAMS\Clinica\Uninstall Clinica.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
    CreateShortCut "$SMPROGRAMS\Clinica\Clinica.lnk" "$INSTDIR\bin\clinica.exe"
  !insertmacro MUI_STARTMENU_WRITE_END
  
SectionEnd

; Descriptions
  LangString DESC_SecClinica ${LANG_ENGLISH} "Clinica components selection."
  LangString MUI_TEXT_LICENSE_TITLE ${LANG_ENGLISH} "License terms for Clinica"
  LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ENGLISH} "Please accept the following terms to use Clinica."
  LangString MUI_TEXT_COMPONENTS_TITLE ${LANG_ENGLISH} "Clinica components"
  LangString MUI_TEXT_COMPONENTS_SUBTITLE ${LANG_ENGLISH} "Please select the components of Clinica that you want to install on the system."
  	LangString MUI_INNERTEXT_LICENSE_BOTTOM ${LANG_ENGLISH} "For more information ses http://fsf.org/"
	LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ENGLISH} "Clinica is licensed with GPL3"
	LangString MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE ${LANG_ENGLISH} "Details"
	LangString MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO ${LANG_ENGLISH}  "Base files for the Clinica installation"
	LangString MUI_TEXT_DIRECTORY_TITLE ${LANG_ENGLISH}  "Clinica installation path"
	LangString MUI_TEXT_DIRECTORY_SUBTITLE ${LANG_ENGLISH}  "Select the directory where Clinica will be installed"
	LangString MUI_TEXT_STARTMENU_TITLE ${LANG_ENGLISH}  "Clinica"
	LangString MUI_TEXT_STARTMENU_SUBTITLE ${LANG_ENGLISH}   "Installation of the start menu entries"
	LangString MUI_INNERTEXT_STARTMENU_CHECKBOX ${LANG_ENGLISH} ""
	LangString MUI_INNERTEXT_STARTMENU_TOP  ${LANG_ENGLISH} ""
	LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ENGLISH}  "Clinica is installing on the system"
	LangString MUI_TEXT_INSTALLING_SUBTITLE  ${LANG_ENGLISH} "Please wait while all the files are copied in place"
	LangString MUI_TEXT_FINISH_TITLE ${LANG_ENGLISH}  "Clinica has been installed"
	LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ENGLISH}  "You may now start it from the Windows menu"
	LangString MUI_TEXT_ABORT_TITLE ${LANG_ENGLISH}  "Aborting Clinica installation"
	LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ENGLISH}  "Installation has been interrupted"
	LangString MUI_UNTEXT_CONFIRM_TITLE  ${LANG_ENGLISH} ""
	LangString MUI_UNTEXT_CONFIRM_SUBTITLE ${LANG_ENGLISH} ""
	LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ENGLISH} ""
	LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ENGLISH} ""
	LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ENGLISH} ""
	LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ENGLISH} ""
	LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ENGLISH} ""
	LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ENGLISH} ""
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro        MUI_DESCRIPTION_TEXT ${SecClinica} $(DESC_SecClinica)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END


; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Clinica"
  DeleteRegKey HKLM SOFTWARE\NSIS_Clinica

  ; Remove files and uninstaller

  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  RMDir /r $INSTDIR\bin
  RMDir /r $INSTDIR\lib
  RMDir /r $INSTDIR\share
  RMDir /r $INSTDIR\include
  RMDir /r $INSTDIR\etc

  RMDir $INSTDIR
  
  ; !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder

  ; Remove directories used
  RMDir "$SMPROGRAMS\Clinica"

  DeleteRegKey /ifempty HKCU "Software/NSIS_Clinica"

SectionEnd
