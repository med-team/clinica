#!/bin/bash
#
# Script to generate the clinica.gresource.xml file
#
#

BASEDIR=resources
RESOURCE_FILE=clinica.gresource.xml

cat > "$RESOURCE_FILE" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
  <gresource prefix="/org/phcteam/clinica">
EOF

for file in `find $BASEDIR/ -type f`; do
  filename=$(echo $file | sed "s/resources\///")
  opts=""
  echo $filename | grep "\.glade$" > /dev/null
  if [ "$?" -eq "0" ]; then
    opts=" preprocess=\"xml-stripblanks\""
  fi
  echo "    <file$opts>$filename</file>" >> "$RESOURCE_FILE"
done

cat >> "$RESOURCE_FILE" <<EOF
  </gresource>
</gresources>
EOF


