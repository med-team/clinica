#!/usr/bin/env python
# -*- coding: utf-8 -*-

import SocketServer, md5, threading, os
from gi.repository import Clinica, GObject, Gtk, PeasGtk, Gio
from socket import gethostname

authenticated_clients = []

class ClinicaAndroid (GObject.GObject, Clinica.UserInterfaceActivatable, PeasGtk.Configurable):

    resource_manager = GObject.property (type = Clinica.ResourceManager)
    user_interface = GObject.property (type = Clinica.UserInterface)

    __gtype_name__ = "ClinicaAndroid"

    server = None

    def __init__(self):
        GObject.GObject.__init__(self)

    def do_activate (self):
        self.clinica_db = os.path.join (self.resource_manager.get_data_path (), 
                                        "clinica.db")

        self.server = ClinicaServer (self.clinica_db)
        self.server_thread = threading.Thread (target = self.server.serve_forever)
        self.server_thread.start ()

        self.tcpserver = ClinicaTCPServer (self.clinica_db)
        self.tcpserver_thread = threading.Thread (target = self.tcpserver.serve_forever)
        self.tcpserver_thread.start ()

    def do_deactivate (self):
        self.server.shutdown ()
        self.server_thread.join ()
        self.tcpserver.shutdown ()
        self.tcpserver_thread.join ()

    def do_create_configure_widget (self):
        settings = Gio.Settings.new ("org.phcteam.clinica")
        box = Gtk.Box.new (Gtk.Orientation.HORIZONTAL, 6)
        
        label = Gtk.Label ("Password")
        entry = Gtk.Entry ()

        box.pack_start (label, 6, False, True)
        box.pack_start (entry, 6, True,  True)

        settings.bind ("password", entry, "text", Gio.SettingsBindFlags.DEFAULT)

        return box

    def on_name_entry_changed (self, entry):
        self.server.set_name (entry.get_text ())
        
    def do_update_state (self):
        pass
        

class ClinicaRequestHandler (SocketServer.BaseRequestHandler):
    
    def handle (self):
        password = self.server.settings.get_value ("password").get_string ()
        
        data = self.request[0].strip ()
        socket = self.request[1]

        if data == "GetInfo":
            socket.sendto ("ServerName: %s\n" % self.server.name, self.client_address)

        elif data.startswith ("Authenticate:"):
            pw = data.split(":")[1].strip ()
            
            if self.client_address[0] in authenticated_clients:
                socket.sendto ("Authentication performed\n", self.client_address)
            else:
                if password == pw:
                    authenticated_clients.append (self.client_address[0])
                    socket.sendto ("Authentication performed\n", self.client_address)
                else:
                    socket.sendto ("Authentication failed\n", self.client_address)

        elif data == "GetDB":
            if self.client_address[0] in authenticated_clients:
                try:
                    with open (self.server.db_path) as h : db_content = h.read ()
                    socket.sendto (db_content, self.client_address)
                except Exception, e:
                    socket.sendto ("Sending DB failed : %s" % e, self.client_address)
            else:
                socket.sendto ("Autenticati, scemo\n", self.client_address)

class ClinicaTCPRequestHandler (SocketServer.BaseRequestHandler):

    def handle (self):
        data = self.request.recv(1024).strip()
        if data == "GetDB":
            if self.client_address[0] in authenticated_clients:
                with open (self.server.db_path) as h : db_content = h.read ()
                self.request.sendall (db_content)
        

class ClinicaTCPServer (SocketServer.ThreadingTCPServer):

    allow_reuse_address = True

    def __init__(self, db_path, port = 20802):
        SocketServer.ThreadingTCPServer.__init__(self, ('', port), ClinicaTCPRequestHandler)
        
        self.db_path = db_path
        self.name = gethostname()

class ClinicaServer (SocketServer.ThreadingUDPServer):

    allow_reuse_address = True

    def __init__ (self, db_path, port = 20801):
        SocketServer.ThreadingUDPServer.__init__ (self, ('', port), ClinicaRequestHandler)
        self.db_path = db_path
        self.name = gethostname()
        self.settings = Gio.Settings.new ("org.phcteam.clinica")


if __name__ == "__main__":

    udp_server = ClinicaServer ("Ciao")
    tcp_server = ClinicaTCPServer ("Ciao")

    server.serve_forever ()
