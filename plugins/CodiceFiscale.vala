using Clinica;
using Gtk;

namespace Clinica {

    public class CodiceFiscalePlugin : Peas.ExtensionBase, PatientEditorActivatable {
    
        public PatientEditor patient_editor { get; set; }

        public CodiceFiscalePlugin () {
            GLib.Object ();            
        }
        
        public void activate () {
            patient_editor.check_finished.connect (check_codice);
            patient_editor.identification_code_entry.changed.connect (
                (editable) => { 
                    check_codice (patient_editor);
                    patient_editor.check_input_data ();
           });
        }
        
        /**
         * @brief Controlla se il codice fiscale è corretto.
         *
         * This functiion has been adapted from the Java version
         * found at http://www.icosaedro.it/cf-pi/vedi-codice.cgi?f=cf-java.txt
         */
        public void check_codice (PatientEditor editor) {
            bool correct = true;
            int i, s, c;
            string cf2;
            string cf = editor.identification_code_entry.get_text ();
            
            /* Accept empty code so, if it is desired, it is possible to omit it */
            if (cf == "") {
                Utils.set_alert_state (patient_editor.identification_code_entry, false);
                return;
            }

            int [] setdisp = {1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20,
                11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 };
            if (cf.length == 0) 
                correct = false;
                
            if (cf.length != 16)
                correct = false;
                
            cf2 = cf.up ();
            for(i = 0; i < 16; i++) {
                c = cf2[i];
                if (!(c>='0' && c<='9' || c>='A' && c<='Z' ))
                    correct = false;
            }
            
            s = 0;
            for (i = 1; i <= 13; i += 2) {
                c = cf2[i];
                if (c >= '0' && c <= '9')
                    s = s + c - '0';
                else
                    s = s + c - 'A';
            }
            
            for (i = 0; i <= 14; i += 2) {
                c = cf2[i];
                if (c>='0' && c <= '9')     
                    c = c - '0' + 'A';
                s = s + setdisp[c - 'A'];
            }
            
            if (s % 26 + 'A' != cf2[15])
                correct = false;
            
            if (!correct) {
                Utils.set_alert_state (patient_editor.identification_code_entry, true);
                patient_editor.set_response_sensitive (PatientEditor.Response.SAVE, false);
            }
            else {
                Utils.set_alert_state (patient_editor.identification_code_entry, false);
            }
        }
        
        public void deactivate () {
            debug ("Unloading plugin CodiceFiscale");
        }
        
        public void update_state () {
        }
    }
    
}

[ModuleInit]
public void peas_register_types (GLib.TypeModule module) {
  /* Register the type Clinica.CodiceFiscalePlugin so libpeas will know
   * that it's here */
  var objmodule = module as Peas.ObjectModule;
  objmodule.register_extension_type (typeof (Clinica.PatientEditorActivatable),
                                     typeof (Clinica.CodiceFiscalePlugin));

}
