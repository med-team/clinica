/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

namespace Clinica {

    public class Sidebar : Gtk.Box {
    
        /**
         * @brief Size of the Icon that shall be used when
         * creating a SidebarEntry. 
         *
         * SidebarEntry's pixbuf will be used directly if it 
         * has this size, otherwise it will be scaled down or
         * up to match it. 
         */
        public static const int ICON_SIZE = 24;
        
        /**
         * @brief Store containing the list of pages that can
         * be opened from the menu.
         */
        private Gtk.TreeStore pages_store;
        
        /**
         * @brief TreeView displaying the data of the pages_store
         */
        private Gtk.TreeView pages_view;

        /**
         * @brief ResourceManager associated with this Clinica instance.
         */
        private ResourceManager resource_manager;
        
        public Sidebar (ResourceManager resources) {
            Object (orientation: Gtk.Orientation.VERTICAL);
            resource_manager = resources;
            
            // Create the new store that will store the names of the pages being 
            // loaded, and a pointer to the pages.
            pages_store = new Gtk.TreeStore (3, typeof (Gdk.Pixbuf), typeof (string), typeof (SidebarEntry));
            
            // Create a TreeView to display the Pages loaded in the current
            // UserInterface instance. 
            pages_view = new Gtk.TreeView.with_model (pages_store);
            pages_view.insert_column_with_attributes (-1, "", new Gtk.CellRendererPixbuf (), "pixbuf", 0);
            
            // Set the ellipsize mode on the renderer in a way that prevent long names
            // to break the UI. 
            var pages_renderer = new Gtk.CellRendererText ();
            pages_renderer.ellipsize = Pango.EllipsizeMode.END;
            
            pages_view.insert_column_with_attributes (-1, _("Pages"), pages_renderer, "text", 1);
            pages_view.set_headers_visible (false);
            
            // Create the scrolledwindow for the TreeView in case we get short
            // on vertical space. 
            var scrolled_window = new Gtk.ScrolledWindow (null, null);
            scrolled_window.add (pages_view);
            scrolled_window.set_shadow_type (Gtk.ShadowType.ETCHED_IN);
            scrolled_window.set_policy (Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
            pack_start (scrolled_window, true, true, resource_manager.PADDING);
            
            // Connect callbacks
            pages_view.get_selection ().changed.connect (on_pages_view_selection_changed);
        }
        
        public void expand_all () {
            pages_view.expand_all ();
        }
        
        private void on_pages_view_selection_changed (Gtk.TreeSelection selection) {
            Gtk.TreeIter iter;
            if (selection.get_selected (null, out iter)) {
                Value entry;
                pages_store.get_value (iter, 2, out entry);
                (entry as SidebarEntry).activated ();
            }
        }
        
        public void add_entry (SidebarEntry entry, SidebarEntry? parent = null) {
            Gtk.TreeIter iter;
            Gtk.TreeIter? parent_iter = null;
            
            if (parent != null)
                parent_iter = search_entry (parent);
                
            pages_store.append (out iter, parent_iter);
            
            var pixbuf = entry.pixbuf ();
            if ((pixbuf.width != ICON_SIZE || pixbuf.height != ICON_SIZE)) {
                debug ("Scaling down icon for the sidebar since does not match ICON_SIZE property");
                pixbuf = pixbuf.scale_simple (ICON_SIZE, ICON_SIZE, Gdk.InterpType.BILINEAR);
            }
            
            pages_store.set_value (iter, 0, pixbuf);
            pages_store.set_value (iter, 1, entry.title ());
            pages_store.set_value (iter, 2, entry);
            
            var path = pages_store.get_path (iter);
            pages_view.expand_to_path (path);
        }
        
        public Gtk.TreeIter search_entry (SidebarEntry entry) {
            Gtk.TreeIter found_iter = Gtk.TreeIter ();
            pages_store.foreach ((model, path, iter) => {
                Value _entry;
                model.get_value (iter, 2, out _entry);
                if ((_entry as SidebarEntry) == entry) {
                    found_iter = iter;
                    return true;
                }
                return false;
            });
            return found_iter;
        }
        
        public void select_entry (SidebarEntry entry) {
            Gtk.TreeIter iter = search_entry (entry);
            if (pages_store.iter_is_valid (iter)) {
                    var path = pages_store.get_path (iter);
                    var selection = pages_view.get_selection ();
                    selection.select_path (path);
            }
        }
        
        public void remove_entry (SidebarEntry entry) {
            var iter = search_entry (entry);
            if (pages_store.iter_is_valid (iter)) {
#if VALA_0_18            
                pages_store.remove (ref iter);
#else
                pages_store.remove (iter);
#endif                
            }
        }
    
    }

}
