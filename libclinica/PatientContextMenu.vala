/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

  /**
     * @brief Menu to be displayed when a user right click on a patient
     * in the TreeView
     */
    public class PatientContextMenu : Gtk.Menu {
    
    	/* Patient where that user has right clicked on */
    	Patient patient;
    	
    	/* TreeView associated with this menu */
    	PatientListView view;
    	
    	/* MENU ITEMS */
    	Gtk.MenuItem edit_menuitem;
    	Gtk.MenuItem delete_menuitem;
    	Gtk.MenuItem view_visits_menuitem;
    	
		private ResourceManager resource_manager;
    
    	public PatientContextMenu(ResourceManager resources, PatientListView view, Patient p) {
    		GLib.Object();
    	    resource_manager = resources;
    		this.patient = p;
    		this.view = view;
    		
    		/* Instantiate menu items */
    		edit_menuitem = new Gtk.MenuItem.with_label(_("Edit"));
    		edit_menuitem.activate.connect (on_edit_menuitem_activated);
    		
    		delete_menuitem = new Gtk.MenuItem.with_label(_("Delete"));
    		delete_menuitem.activate.connect(on_delete_menuitem_activated);
    		
    		view_visits_menuitem = new Gtk.MenuItem.with_label(_("Edit visits"));
    		view_visits_menuitem.activate.connect(on_view_visits_menuitem_activated);
    		
    		/* Append menuitems */
    		append(edit_menuitem);
    		append(delete_menuitem);
    		append(view_visits_menuitem);
    		
    		show_all ();
    	}
    	
    	private void on_edit_menuitem_activated (Gtk.MenuItem item) {
    		var patient_editor = new PatientEditor.with_patient (resource_manager, patient);
    		patient_editor.set_transient_for (resource_manager.user_interface.window);
    		patient_editor.run();
    		patient_editor.destroy ();
    	}
    	
    	private void on_delete_menuitem_activated (Gtk.MenuItem item) {
    		view.remove_selected_patient ();
    	}
    	
    	private void on_view_visits_menuitem_activated (Gtk.MenuItem item) {
            resource_manager.user_interface.show_visit_window (patient);
    	}
    }
    
}
