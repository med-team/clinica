/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
using Gtk;
 
namespace Clinica {

    public class SettingsManager : Gtk.Window {
    
        private ResourceManager resource_manager;
        
        private Settings settings;
        
        private Clinica.Builder builder;
        
        private MedicineSearchEngineModel se_model;
        
        private DataProviderModel dp_model;
    
        public SettingsManager (ResourceManager resources) {
            resource_manager = resources;
            settings = resource_manager.settings;
            set_title (_("Clinica settings"));
            
            // Create the Clinica Builder used to retrieve widgets
            builder = new Clinica.Builder.with_filename (resource_manager,
                "settings_window.glade");
            builder.load_into_window (this);
            
            // Load the plugin selector and put it in place
            var plugins_alignment = builder.get_object ("plugins_alignment") as Alignment;            
#if HAVE_PLUGINS
            plugins_alignment.add (new PluginManager (resource_manager));
#else
            plugins_alignment.add (new Label (_("Clinica has been built without plugins support")));
#endif
            
            // Bind the switch in the first page to their corresponding
            // GSettings values. 
            settings.bind ("use-plugins", builder.get_object ("plugins_switch") as Switch, 
                "active", SettingsBindFlags.DEFAULT);
            settings.bind ("show-visit-file-manager", builder.get_object ("browse_files_switch") as Switch,
                "active", SettingsBindFlags.DEFAULT);
                
            // Bind the personal data of the doctor to the related GSettings
            // values.
            personal_details_init ();
                
            // Init the comboboxes for DataProvider and 
            // MedicineSearchEngines selections
            medicine_search_engine_tab_init ();
            data_provider_combobox_init ();
            
            // DataProvider could change without being expressely chosen
            // in the combobox, so in that case update it. 
            resource_manager.notify["data-provider"].connect ((p) => select_active_data_provider ());
            
            // And finally the frames containing the connection data
            // and the server configuration. 
            connection_frame_init ();
            server_frame_init ();
        }
        
        private void personal_details_init () {
            settings.bind ("personal-details-name", builder.get_object ("name_entry") as Entry,
                "text", SettingsBindFlags.DEFAULT);
            settings.bind ("personal-details-address", builder.get_object ("address_entry") as Entry,
                "text", SettingsBindFlags.DEFAULT);
            settings.bind ("personal-details-institution", builder.get_object ("institution_entry") as Entry,
                "text", SettingsBindFlags.DEFAULT);
            settings.bind ("personal-details-email", builder.get_object ("email_entry") as Entry,
                "text", SettingsBindFlags.DEFAULT);
        }
        
        private void medicine_search_engine_tab_init () {
            // Configure the treeview containing the Medicine search engines
            se_model = new MedicineSearchEngineModel (resource_manager);
            var se_view = builder.get_object ("medicine_search_engines_treeview") as TreeView;
            se_view.set_model (se_model);
            
            var active_renderer = builder.get_object ("active_cellrenderer") as Gtk.CellRendererToggle;
            active_renderer.toggled.connect (on_active_cellrenderer_toggled);
        }
        
        private void on_active_cellrenderer_toggled (Gtk.CellRenderer renderer, string path) {
            Gtk.TreePath t_path = new Gtk.TreePath.from_string (path);
            Gtk.TreeIter iter;
            string name;
            GLib.Value value;
            bool active;
            
            se_model.get_iter (out iter, t_path);
            
            se_model.get_value (iter, 0, out value);
            active = (bool) value;
            
            se_model.get_value (iter, 1, out value);
            name = value as string;
            
            se_model.set_value (iter, 0, !active);
            
            se_model.get_value (iter, 2, out value);
            if (! active) {
                resource_manager.settings.selected_search_engines.append (value as MedicineSearchEngine);
                resource_manager.settings.activate_medicine_search_engine (name);
            }
            else {
                resource_manager.settings.selected_search_engines.remove (value as MedicineSearchEngine);
                resource_manager.settings.deactivate_medicine_search_engine (name);
            }        
        }
        
        
        private void data_provider_combobox_init () {
            // ...and the one that allows to choose the data providers
            dp_model = new DataProviderModel (resource_manager);
            var dp_combobox = builder.get_object ("data_sources_combobox") as ComboBox;
            dp_combobox.set_model (dp_model);
            select_active_data_provider ();       
            dp_combobox.changed.connect (on_dp_combobox_changed); 
        }
        
        private void on_dp_combobox_changed () {
            var dp_combobox = builder.get_object ("data_sources_combobox") as ComboBox;
            Gtk.TreeIter active_iter;
            dp_combobox.get_active_iter (out active_iter);
            Value dp_value;
            dp_model.get_value (active_iter, 1, out dp_value);
            
            // Check that the DataProvider selected by the user does not match a NetworkDataProvider.
            // In that case the user should use the Network tab to configure it. 
            if ((dp_value as DataProvider) is NetworkedDataProvider && 
                !resource_manager.networked_data_provider.connected) {
                var info_dialog = new Gtk.MessageDialog (null,
                    Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL,
                    Gtk.MessageType.INFO, 
                    Gtk.ButtonsType.OK,
                    "%s", _("Please use the Network tab to enable the Networked data provider"));
                info_dialog.set_title (_("Use the Network tab"));
                info_dialog.run ();
                info_dialog.destroy ();
                
                select_active_data_provider ();
            }
            else
                resource_manager.select_data_provider (dp_value as DataProvider);
        }
        
        private void connection_frame_init () {
            // Bind the fields to the corresponing GSettings values
            var host_entry = builder.get_object ("host_entry") as Entry;
            settings.bind ("network-data-provider-host", host_entry, "text", SettingsBindFlags.DEFAULT);
            var port_entry = builder.get_object ("port_entry") as Entry;
            settings.bind ("network-data-provider-port", port_entry, "text", SettingsBindFlags.DEFAULT);
            
            // Update the Label inside the connect button and the state of the 
            // DataProvider selector
            update_connect_button_label ();
            
            var connect_button = builder.get_object ("connect_button") as Button;
            connect_button.clicked.connect (on_connect_button_clicked);
        }
        
        private void update_connect_button_label () {
            var button = builder.get_object ("connect_button") as Button;
            var label = button.get_child () as Label;
            
            label.set_text (resource_manager.networked_data_provider.connected ? 
                _("Disconnect") : _("Connect"));
                
            // Update the status of the combobox with the data provider. 
            var dp_combobox = builder.get_object ("data_sources_combobox") as ComboBox;
            dp_combobox.set_sensitive (!resource_manager.networked_data_provider.connected);
        }
        
        private void on_connect_button_clicked (Button button) {
            var host_entry = builder.get_object ("host_entry") as Entry;
            var port_entry = builder.get_object ("port_entry") as Entry;
        
            if (resource_manager.networked_data_provider.connected)
                resource_manager.networked_data_provider.disconnect_from_server ();
                host_entry.set_sensitive (true);
                port_entry.set_sensitive (true);
                
                if (resource_manager.data_provider == resource_manager.networked_data_provider as DataProvider) {
                    resource_manager.select_data_provider (resource_manager.sqlite_data_provider);
                }
            else {
                if (!resource_manager.networked_data_provider.connect_to_server (
                    host_entry.get_text (), int.parse (port_entry.get_text ()))) {
                    debug ("Connection to the given host failed");
                    var connection_failed_dialog = new MessageDialog (this, DialogFlags.MODAL, 
                        MessageType.ERROR, ButtonsType.OK,
                        "%s", _("Connection to the given host failed. Please check your settings."));
                    connection_failed_dialog.set_title (_("Connection failed"));
                    connection_failed_dialog.run ();
                    connection_failed_dialog.destroy (); 
                    
                    update_connect_button_label ();

                    return;
                }
                    
                if (!resource_manager.networked_data_provider.try_authentication (host_entry.get_text (), 
                            port_entry.get_text (), resource_manager.networked_data_provider.username,
                            resource_manager.networked_data_provider.password)) {
                    resource_manager.networked_data_provider.disconnect_from_server ();
                    var dialog = new MessageDialog (this, DialogFlags.MODAL, MessageType.ERROR, ButtonsType.OK,
                        "%s", _("Username or password are wrong. Disconnecting from the server."));
                    dialog.set_title (_("Wrong authentication data has been provided"));
                    dialog.run ();
                    dialog.destroy ();
                }
                else {
                    resource_manager.select_data_provider (resource_manager.networked_data_provider);
                    host_entry.set_sensitive (false);
                    port_entry.set_sensitive (false);
                }
            }
            
            update_connect_button_label ();
        }
        
        private void server_frame_init () {
            // Bind the Switch to select if the DataServer is active
            var data_server_switch = builder.get_object ("data_server_switch") as Switch;
            settings.bind ("data-server-activated", data_server_switch, "active", SettingsBindFlags.DEFAULT);
            
            var username_entry = builder.get_object ("username_entry") as Entry;
            var password_entry = builder.get_object ("password_entry") as Entry;
            
            // Bind username and password, properly hashing the password in the database
            settings.bind ("data-server-username", username_entry, "text", 
                SettingsBindFlags.DEFAULT);
                
            // Setup password changer
            password_entry.set_text ("Password");
            password_entry.focus_in_event.connect ((entry, event) => {
                password_entry.set_text ("");
                return true;
            });
            password_entry.changed.connect ((editable) => {
                var entry = editable as Entry;
                var password = entry.get_text ();
                if (password != "")
                    settings.set_string ("data-server-password",
                        Checksum.compute_for_string (ChecksumType.SHA1, password, -1));
            }); 
        }
        
        private void select_active_data_provider () {
            var dp_combobox = builder.get_object ("data_sources_combobox") as ComboBox;
            Gtk.TreeIter iter;
            if (dp_model.get_iter_first (out iter)) {
                do {
                    Value data_provider_value;
                    dp_model.get_value (iter, 1, out data_provider_value);
                    
                    if ((data_provider_value as DataProvider) == resource_manager.data_provider) {
                        dp_combobox.set_active_iter (iter);
                    }
                } while (dp_model.iter_next (ref iter));
            }
        }
    
    }
    
    private class MedicineSearchEngineModel : Gtk.ListStore {
        
        private ResourceManager resource_manager;
        
        public MedicineSearchEngineModel (ResourceManager resources) {
            Type [] column_headers = { typeof (bool), typeof (string) , typeof (MedicineSearchEngine) };
            set_column_types (column_headers);
            
            resource_manager = resources;
        
            foreach (var search_engine in resource_manager.medicine_search_engines) {
                Gtk.TreeIter iter;
                append (out iter);
                set_value (iter, 0, resource_manager.settings.get_medicine_search_engines ().index (search_engine) != -1);
                set_value (iter, 1, search_engine.get_name ());
                set_value (iter, 2, search_engine);
            }
        }
        
    }
    
    private class DataProviderModel : Gtk.ListStore {
        
        private ResourceManager resource_manager;
    
        public DataProviderModel (ResourceManager resources) {
            resource_manager = resources;
            set_column_types ({ typeof (string), typeof (DataProvider) });
            
            foreach (var data_provider in resource_manager.data_providers) {
                Gtk.TreeIter iter;
                append (out iter);
                set_value (iter, 0, data_provider.get_name ());
                set_value (iter, 1, data_provider);
            }
        }
    }
}
