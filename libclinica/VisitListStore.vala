/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
using Gtk;

namespace Clinica {

	public class VisitListStore : ListStore {
	
		enum Field {
			VISIT,
			DATE,
		}
	
		public signal void error (string message);
		
		private ResourceManager resource_manager;
		
		private DataProvider provider;
	
		public VisitListStore (ResourceManager resources) {
		    resource_manager = resources;
			/* Error callback */
			error.connect ((t,l) => resource_manager.error_callback(t,l));
			
			/* Create headers for the liststore with types
			 * Visit, Date */
			Type [] column_headers = { typeof(Visit), typeof(string) };
			set_column_types (column_headers);
			load_data ();
		}
		
		public void load_data () {
			/* Fill liststore asynchronously */
			Idle.add(() => {
				foreach (Visit v in resource_manager.data_provider.visits ()) {
					add_visit (v.id);
				}
				
    	        /* Setup callbacks to add or remove doctors */
    	        provider = resource_manager.data_provider;
		        update_signals ();
		        
		        resource_manager.notify["data_provider"].connect ((provider) => update_signals ());
				
				/* We don't need to execute any more */
				return false;
			});		
		}
		
		private void update_signals () {
		    resource_manager.data_provider.visit_added.connect (
                    (id) => add_visit (id));		        
            resource_manager.data_provider.visit_changed.connect (
                    (id) => reload_visit (id));
            resource_manager.data_provider.visit_removed.connect (
		            (id) => remove_visit (id));
		            
		    provider = resource_manager.data_provider;
		}
		
		private void add_visit (int64 id) {
		    Visit v = resource_manager.data_provider.get_visit (id);
			TreeIter iter;
			append (out iter);
			set_value (iter, Field.VISIT, v);
			set_value (iter, Field.DATE, "");
		}
		
		private void reload_visit (int64 id) {
		    Visit v = resource_manager.data_provider.get_visit (id);
			TreeIter iter = visit_to_iter (v);
			set_value (iter, Field.VISIT, v);
			set_value (iter, Field.DATE, "");
		}
		
		private void remove_visit (int64 id) {
			TreeIter it;
			Value visit;
			
			if (!get_iter_first (out it)) {
				error(_("Visit database seems corrupted. This is likely to be a bug in the application"));
			}
			do {
				get_value (it, Field.VISIT, out visit);
				if ((visit as Visit).id == id) {
					remove (it);
					return;
				}
			} while (iter_next (ref it));
		
			assert_not_reached ();
		}
		
		public Visit iter_to_visit (TreeIter it) {
			Value visit;
			get_value (it, Field.VISIT, out visit);
			return (visit as Visit);
		}
		
		public TreeIter visit_to_iter (Visit v) {
			Value visit;
			TreeIter iter;
			if (!get_iter_first (out iter))
				assert_not_reached ();
			do {
				get_value (iter, Field.VISIT, out visit);
				if ((visit as Visit).id == v.id) {
					return iter;
				}
			} while (iter_next (ref iter));
			
			assert_not_reached ();
		}
	}

}
