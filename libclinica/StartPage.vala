/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {
    
    public class StartPage : Alignment, Page {
        
        /* Data to coordinate interface creation */
        private Builder builder;
        
        /* Gtk+ stuff */
        private VBox   start_button_box;
        private Label  new_user_button_label;
        private Entry  find_entry;
        private Button browse_patient_list_button;
        private Button browse_doctor_list_button;
        
        public ResourceManager resource_manager { get; set; }
        
        private SidebarEntry sidebar_entry;
        
        public StartPage (ResourceManager resources) {
            resource_manager = resources;
            
            /* Create builder */
            builder = new Builder (resources);
            try {
                builder.add_from_resource ("start_page.glade");
            } catch (Error e) {
                error ("Error loading new-button.glade, aborting StartPage creation.");
            } 
            
            /* Load interesting objects */
            new_user_button_label  = builder.get_object ("new_button_label") as Label;
            start_button_box = builder.get_object ("start_button_box") as VBox;
            
            /* Pack button in myself */
            add (start_button_box);
            
            /* Connect signals */
            find_entry = builder.get_object ("find_entry") as Entry;
            find_entry.changed.connect (on_find_entry_changed);
            find_entry.set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, null);
            
            find_entry.icon_release.connect (on_icon_release);
    	    find_entry.key_press_event.connect (on_escape_key);
            
            /* Create Doctor Entry with autocompletion, and setup a callback
             * to store the Doctor selected with autocompletion when this 
             * happens. */
            var completion = new EntryCompletion ();
            completion.set_model (resource_manager.patient_list_store);
            completion.set_text_column (PatientListStore.Field.COMPLETE_NAME);
            completion.set_match_func (completion_match_function);
            completion.match_selected.connect (on_completion_match_selected);
            
            /* Activate completion */
            find_entry.set_completion (completion);
            
            /* Load and connect the browse_patient_list_button */
	        browse_patient_list_button = builder.get_object ("browse_patient_list_button") as Button;
	        browse_patient_list_button.clicked.connect ((t) => resource_manager.user_interface.set_page ("patients"));
	        
	        /* Load and connect the browse_doctor_list_button */
	        browse_doctor_list_button = builder.get_object ("browse_doctor_list_button") as Button;
	        browse_doctor_list_button.clicked.connect ((t) => resource_manager.user_interface.set_page ("doctors"));
	        
	        /* Connect XML-defined signals */
	        builder.connect_signals (this);
	       
	        /* Hide the hint "Browse the patient to start a visit" in ten seconds */ 
	        GLib.Timeout.add (10000, hide_hint);
	        
	        sidebar_entry = new SidebarPageEntry (resource_manager, this, _("Dashboard"),
	            Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/clinica.svg", 
	            Sidebar.ICON_SIZE, Sidebar.ICON_SIZE));
        }
        
        /**
         * @brief Hide the hint on how to start a visit, with a smooth animation.
         */
        private bool hide_hint () {
        	var hint_hbox = builder.get_object ("hint_hbox") as HBox;
        	var children = hint_hbox.get_children ();
        	if (children.length() == 2) {
        	    var img = children.nth_data (0);
        	    var label = children.nth_data (1);
        	    hint_hbox.remove (img);
        	    hint_hbox.remove (label);
        	    hint_hbox.margin_top = 30;
        	    Timeout.add (40, hide_hint);
        	}
        	else if (children.length() == 0) {
        	    hint_hbox.margin_top -= 1;
        	    if (hint_hbox.margin_top > 0) {
        	        Timeout.add (50, hide_hint);
        	    }
        	}
        	return false;
        }
        
        /**
         * @brief Clear the find_entry. 
         */
    	private void clear_find_entry() {
            find_entry.set_text("");
	    	find_entry.set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, null);
        }
	
		/**
         * @brief Clear search icon in the find_entry. 
         */
    	private void on_icon_release(Gtk.EntryIconPosition pos, Gdk.Event event) {
            if (Gtk.EntryIconPosition.SECONDARY == pos)
	       	clear_find_entry();
        }
        
        /**
         * @brief Possibility to clear the
         * find_entry by typing 'Esc'. 
         */
        private bool on_escape_key(Gdk.EventKey e) { 
            if(Gdk.keyval_name(e.keyval) == "Escape")
				clear_find_entry();
            // Continue processing this event, since the 
            // text entry functionality needs to see it too. 
            return false; 
        }
        
        /**
         * @brief Callback function for the doctor selection that matches the doctor
         * if the key is a substring of its complete name. 
         */
        private bool completion_match_function (EntryCompletion compl, string key, TreeIter iter) {
        	Value value;
        	resource_manager.patient_list_store.get_value(iter, 0, out value);
        	Patient p = value as Patient;
        	if (p == null) {
        		return false;
        	}
        	return (key.up () in p.get_complete_name ().up ());
        }
        
        /**
         * @brief Callback called when a patient get selected from the completion
         * list.
         */
        private bool on_completion_match_selected (EntryCompletion completion, TreeModel model, TreeIter iter) {
        	Value patient;
        	model.get_value (iter, PatientListStore.Field.PATIENT, out patient);
        	
        	// Show the window associated to the patient and clear the textentry
            resource_manager.user_interface.show_visit_window (patient as Patient);
        	(completion.get_entry () as Entry).set_text ("");
        	
        	// Signal to the entry that we have already managed the completion, so
        	// the entry is not filled with the selected patient name. 
        	return true;
        }
        
        public string get_title () {
            return "Clinica";
        }
        
        public SidebarEntry? get_sidebar_entry () {
            return sidebar_entry;
        }
        
        public void on_find_entry_changed (Editable e) {
            set_name (find_entry.get_text ());
            
            if ((e as Entry).get_text () == "")
            	clear_find_entry ();
            else
            	find_entry.set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, "gtk-clear");
        }
        
        /**
         * @brief Set the label on the button to something like
         * "Create a new patient with name ...". 
         * with first char after " " uppercase
         */
        public void set_name (string patient_name) {        	
            if (patient_name != "") {
                new_user_button_label.set_markup (
                    _("Add a new patient \n with name ") + 
                    @"<i>$(Utils.capitalize (patient_name))</i>"
                );
            }
            else {
                new_user_button_label.set_markup (
                    @_("Add a new patient")
                );
            }
        }
        
        [CCode (instance_pos = -1)]
        public void on_medicines_search_page_button_clicked (Button button) {
            resource_manager.user_interface.set_page ("medicines");
        }
        
        [CCode (instance_pos = -1)]
        public void on_calendar_window_button_clicked (Button button) {
            resource_manager.user_interface.show_calendar_window ();
        }
        
        /**
         * @brief Callback for the creation of the new patient
         */
        [CCode (instance_pos = -1)]
        public void on_new_button_clicked (Button button) {
        	var dialog = new PatientEditor.with_name (resource_manager, Utils.capitalize (find_entry.get_text ()));
		dialog.set_transient_for (resource_manager.user_interface.window);
        	if (dialog.run () == PatientEditor.Response.SAVE) {
        		/* Reset find entry */
        		find_entry.set_text ("");
        		// TODO: Go to the patient visit management
        		dialog.destroy ();
        	}
        	
        	dialog.destroy ();
        }
    }
    
}
