/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Gtk;
 
namespace Clinica {

    public class MedicineBrowserDialog : Gtk.Dialog {
    
        private ResourceManager resource_manager { get; set; }
        
        private Clinica.Builder builder;
        
        private TreeView treeview;
        
        public MedicineBrowserDialog (ResourceManager resources) {
            resource_manager = resources;
            
            builder = new Clinica.Builder.with_filename (resources, "medicine_browser.glade");
            builder.load_into_dialog (this);
            
            treeview = builder.get_object ("treeview") as TreeView;
            treeview.set_model (resource_manager.local_medicines_search_engine);
            
            add_buttons (Stock.CLOSE, 0);
            
            set_title (_("Local medicine browser"));
            set_size_request (480, 320);
            
            // Connect callbacks to signals
            var new_toolbutton = builder.get_object ("new_toolbutton") as ToolButton;
            new_toolbutton.clicked.connect (on_new_toolbutton_activated);
            
            var edit_toolbutton = builder.get_object ("edit_toolbutton") as ToolButton;
            edit_toolbutton.clicked.connect (on_edit_toolbutton_activated);
            
            var delete_toolbutton = builder.get_object ("delete_toolbutton") as ToolButton;
            delete_toolbutton.clicked.connect (on_delete_toolbutton_activated);
        }
        
        private void on_new_toolbutton_activated (ToolButton button) {
            var new_medicine_dialog = new MedicineEditor (resource_manager);
            new_medicine_dialog.run ();
            new_medicine_dialog.destroy ();
        }
        
        private void on_edit_toolbutton_activated (ToolButton button) {
            TreeModel model;
            TreeIter iter;
            
            var selected = treeview.get_selection ().get_selected (out model, out iter);
            
            if (selected) {
                var new_medicine_dialog = new MedicineEditor (resource_manager, iter.user_data as LocalMedicineIter);
                new_medicine_dialog.run ();
                new_medicine_dialog.destroy ();
            }
        }
        
        private void on_delete_toolbutton_activated (ToolButton button) {            
            TreeModel model;
            TreeIter iter;
            
            var selected = treeview.get_selection ().get_selected (out model, out iter);
            
            if (selected) {
                resource_manager.local_medicines_search_engine.delete_medicine (iter.user_data as LocalMedicineIter);
            }
        }
        
    }

}
