/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 *            Maxwell Barvian from Maya - http://launchpad.net/maya/
 */

using Gtk;

namespace Clinica {

    public class CalendarToolbar : Gtk.Toolbar {
    
        /**
         * @brief Switcher for the months
         */
        internal DateSwitcher month_switcher;
        
        /**
         * @brief Switcher for the years.
         */
        internal DateSwitcher year_switcher;
        
        internal Button add_event_button;
        
        private ResourceManager resource_manager;
        
        private CalendarWindow calendar_window;
        
        public CalendarToolbar (ResourceManager resources, CalendarWindow calendar) {
            resource_manager = resources;
            this.calendar_window = calendar;
            
            /* Get the look of Ubuntu and elementaries toolbars */
            get_style_context ().add_class ("primary-toolbar");
            
            add_event_button = new Button.from_stock (Stock.ADD);
            var add_item = new ToolItem ();
            add_item.add (add_event_button);
            add_event_button.clicked.connect (on_add_button_clicked);

            /* Request a decent width for the month switcher so it's likely
             * not to resize on month name change */
            month_switcher = new DateSwitcher (100);
            var month_toolitem = new ToolItem ();
            month_toolitem.add (month_switcher);
            
            year_switcher  = new DateSwitcher ();
            var year_toolitem  = new ToolItem ();
            year_toolitem.add (year_switcher);
            
            /* The add button first */
            insert (add_item, 0);
            
            /* Insert a spacer before and after the month and
             * year switcher so they don't end in a corner */
            var left_spacer = new ToolItem ();
            left_spacer.set_expand (true);
            insert (left_spacer, 1);
            insert (month_toolitem, 2);
            
            /* Even some margin between the two selectors */
            month_toolitem.set_margin_right (10);
            insert (year_toolitem,  3);
            
            /* The right spacer that keep our control buttons
             * centered in the toolbar */
            var right_spacer = new ToolItem ();
            right_spacer.set_expand (true);
            insert (right_spacer, 4);
        }
        
        public void on_add_button_clicked (Button button) {
            DateTime? selected_date = calendar_window.calendar_view.calendar.get_selected_date ();
            EventEditor event_editor;
            if (selected_date == null)
                event_editor = new EventEditor (resource_manager);
            else
                event_editor = new EventEditor.with_date (resource_manager, selected_date);
            
            event_editor.set_transient_for (resource_manager.user_interface.calendar_window);
            if (event_editor.run () == EventEditor.Response.SAVE) {
                resource_manager.data_provider.save_event (event_editor.selected_event);
            }
            event_editor.destroy ();
        }
    }
}
