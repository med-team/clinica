/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
 namespace Clinica {
 
    /**
     * @brief Generic File obtained through the FileStore
     * interface. 
     */
    public class FileObject {
    
        /**
         * @brief The store from which this FileObject was generated.
         */
        private FileStore store;
        
        /**
         * @brief The real path of the file in the filtestore.
         */
        string path;
        
        /**
         * @brief The ID of the file store where this file is saved.
         */
        int64 id;
        
        /**
         * @brief Generate a FileObject starting from an existing file.
         * A copy of that filed will be stored in the FileStore specified.
         */
        public FileObject (FileStore store, int64 id, string filename, bool in_store = false) {
            this.store = store;
            this.id = id;
            if (!in_store)
                path = store.store_file (id, filename);
            else
                path = filename;
        }
        
        /**
         * @brief Obtain the ID in the file store where the file can
         * be retrieved.
         */
        public int64 get_id () {
            return id;
        }
        
        /**
         * @brief Get a path where the file will be accessible. This will be
         * the real path of the file in the database, so the user is responsible
         * for overwriting it.
         */
        public string get_file_path () {
            return path;
        }
        
        /**
         * @brief Return the name of the file (without the full path).
         */
        public string get_file_name () {
            return File.new_for_path (path).get_basename ();
        }
        
        /**
         * @brief get a GFile associated with this file.
         */
        public File get_file () {
            return File.new_for_path (path);
        }
        
    }
 }
