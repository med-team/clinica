/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
  
using Jansson;
 
namespace Clinica {

    public enum RpcType {
        STRING = 1,
        INTEGER,
        JSON_OBJECT
    }
 
    internal class NetworkedDoctorIterator : Object, DoctorIterator {
    
        private Json array;
        
        private uint counter;
        
        private NetworkedDataProvider provider;
    
        public class NetworkedDoctorIterator (NetworkedDataProvider provider, Json node) {
            this.provider = provider;
            array = node;
            counter = array.array_size ();
        }
        
        public bool next () {
            if (counter == 0) {
                return false;
            }
            else {
                counter -= 1;
                return true;
            }
        }
        
        public new Doctor get () {
            var id_element = array.array_get ((int) counter);
            return provider.get_doctor (id_element.integer_value ());
        }
    
    }
 
    internal class NetworkedPatientIterator : Object, PatientIterator {
    
        private Json array;
        
        private uint counter;
        
        private NetworkedDataProvider provider;
        
        public NetworkedPatientIterator (NetworkedDataProvider provider, Json node) {
            this.provider = provider;
            array = node;
            counter = array.array_size ();
        }
        
        public bool next () {
            if (counter == 0) {
                return false;
            }
            else {
                counter -= 1;
                return true;
            }
        }
        
        public new Patient get () {
            var id_element = array.array_get ((int) counter);
            return provider.get_patient (id_element.integer_value ());
        }
    
    }
 
    internal class NetworkedVisitIterator : Object, VisitIterator {
    
        private Json array;
        
        private uint counter;
        
        private NetworkedDataProvider provider;
    
        public NetworkedVisitIterator (NetworkedDataProvider provider, Json node) {
            this.provider = provider;
            array = node;
            counter = array.array_size ();
        }
        
        public bool next () {
            if (counter == 0) {
                return false;
            }
            else {
                counter -= 1;
                return true;
            }
        } 
        
        public new Visit get () {
            var id_element = array.array_get ((int) counter);
            return provider.get_visit (id_element.integer_value ());
        }
        
    }
    
    internal class NetworkedEventIterator : Object, EventIterator {
    
        private Json array;
        
        private uint counter;
        
        private NetworkedDataProvider provider;
    
        public NetworkedEventIterator (NetworkedDataProvider provider, Json node) {
            this.provider = provider;
            array = node;
            counter = array.array_size ();
        }
        
        public bool next () {
            if (counter == 0) {
                return false;
            }
            
            counter -= 1;
            return true;
        }
        
        public new Event get () {
            var id_element = array.array_get ((int) counter);
            return provider.get_event (id_element.integer_value ());
        }
    
    }
 
    public class NetworkedDataProvider : GLib.Object , DataProvider {
    
        /**
         * @brief Return the name of the DataProvider. 
         */
        public string get_name () { return "Network Data Provider"; }
        
        /**
         * @brief True if a connection was established with the remote Clinica
         * server. 
         * 
         * If connected is false, i.e. if connect() method has never
         * been called or the connection has been dropped for some reason then
         * the NetworkedDataProvider will not return any result.
         */
        internal bool connected { get; private set; }
        
        /**
         * @brief The SoupSession used to communicate with the server. 
         */
        private Soup.Session? session = null;
        
        /**
         * @brief The base URL used for the request. Could be, for example, 
         * http://192.168.0.4:20843/. 
         */
        private string base_url;
        
        /**
         * @brief The URL used for RPC requests.
         */
        private string rpc_url;
        
        private ResourceManager resource_manager;
        
        internal string username = "";
        
        internal string password = "";
        
        private int64 request_id = 0;
        
        public NetworkedDataProvider (ResourceManager resources) {
            resource_manager = resources;
        }
        
        ~NetworkedDataProvider () {
            debug ("Freeing NetworkedDataProvider");
        }
        
        public void disconnect_from_server () {
            connected = false;
            session = null;
        }
        
        public bool connect_to_server (string hostname, int port = 20843) {
            if (session != null) {
                warning ("connect_to_server() has been called without calling disconnect_from_server() first, calling it now");
                disconnect_from_server ();
            }
            
            session = new Soup.SessionAsync ();
            
            session.authenticate.connect (this.perform_authentication);
            
            /* Try the connection to the server to make sure that it is
             * reachable */
            base_url = "http://" + hostname + ":" + port.to_string () + "/";
            rpc_url = base_url + "rpc/";
            var msg = new Soup.Message ("GET", base_url);
            session.send_message (msg);

            if (msg.response_body.data == null) {
                connected = false;
                return false;
            }
            
            connected = true;
            
            debug ("Connected to the clinica server that answered: %s", (string) msg.response_body.data);
            
            // Perform authentication
            var auth_dialog = new AuthenticationDialog (resource_manager);
            if (auth_dialog.run () == Gtk.ResponseType.OK) {
                username = auth_dialog.get_username ();
                password = auth_dialog.get_password ();
                auth_dialog.destroy ();
            }
            else {
                auth_dialog.destroy ();
                debug ("Switching to Sqlite database");
                var info_dialog = new Gtk.MessageDialog (null,
                    Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL,
                    Gtk.MessageType.INFO, Gtk.ButtonsType.OK,
                    "%s", _("You canceled the authentication process on the remote server.\nFrom now on Clinica will use the local database. If you want to connect to\nremote server please re-enable networking in the settings dialog."));
                info_dialog.run ();
                info_dialog.destroy ();
                resource_manager.select_data_provider (resource_manager.sqlite_data_provider);
                disconnect_from_server ();
            }
            
            return connected;
        }
        
        private void perform_authentication (Soup.Session session, Soup.Message msg, Soup.Auth auth, 
            bool retrying) {
            auth.authenticate (username, password);
        }
         
        public bool try_authentication (string host, string port, string username, string password) {
                // Try to authenticate to the remote server to check if the password
                // is correct
                bool pw_correct = true;
                var session = new Soup.SessionAsync ();
                var msg = new Soup.Message ("POST", "http://%s:%s/rpc/".printf (
                    host, port));
                session.authenticate.connect ((session, msg, auth, retrying) => {
                    if (retrying) {
                        pw_correct = false;
                        session.cancel_message (msg, 500);
                    }
                    else
                        auth.authenticate (username, password);
                });
                session.send_message (msg);        
                
                return pw_correct;
        }   
        
        private bool check_connection () {
            if (!connected) {
                warning ("Calling a method of NetworkedDataProvider before connect_to_server(), aborting");
                return false;
            }
            
            return true;
        }
        
        private string api_request (string method, ...) {
            Json? params = Json.object ();
            var l = va_list ();
            
            while (true) {
                RpcType type = l.arg ();
                
                if (type == 0)
                    break;
                
                string param_name = l.arg ();
                switch (type) {
                    case RpcType.INTEGER:
                        int64 value = l.arg ();
                        params.object_set (param_name, Json.integer (value));
                        break;
                    case RpcType.STRING:
                        string value = l.arg ();
                        params.object_set (param_name, Json.string (value));
                        break;
                    case RpcType.JSON_OBJECT:
                        Json? value = l.arg ();
                        if (value != null)
                            params.object_set (param_name, value);
                        break;
                }
            }

            Json? root = Json.object ();
            
            root.object_set ("jsonrpc", Json.string ("2.0"));
            root.object_set ("method", Json.string (method));
            root.object_set ("params", params);
            root.object_set ("id", Json.integer (request_id++));
            
            return post_message (root.dumps ());
        }
        
        private string post_message (string json_data) {
            var msg = new Soup.Message ("POST", rpc_url);
            msg.set_request ("text/html", Soup.MemoryUse.COPY, 
                json_data.data);
            session.send_message (msg);
            return (string) msg.response_body.data;
        }
        
        public Visit? get_visit (int64 id) {
            if (!check_connection()) {
                return null;
            }
            
            var response = api_request ("get_visit", 
                RpcType.INTEGER, "id", id);
                
            Jansson.Error error;
            return new Visit.from_json (Json.loads (response, 0, out error).object_get ("result"), this);
        }
        
        public int64 save_visit (Visit visit) {
        
            var response = api_request ("save_visit", 
                RpcType.JSON_OBJECT, "visit", visit.to_json ());
                
            Jansson.Error error;
            var root = Json.loads (response, 0, out error).object_get ("result");
            var id = root.object_get ("id").integer_value ();
            if (visit.id == 0) {
                visit.id = id;
                visit_added (id);
            }
            else
                visit_changed (id);
            return id;
        }
        
        public int64 remove_visit (Visit visit) {
            if (!check_connection ())
                return -1;

            var response = api_request ("remove_visit", RpcType.INTEGER, "id", visit.id);
            
            Jansson.Error error;
            Json? response_root = Json.loads (response, 0, out error);
            if (response_root == null)
                return -1;
            
            Json? error_el = response_root.object_get ("error");
            if (! error_el.is_null ())
                return -1;
                
            visit_removed (visit.id);
            return 0;
        }
        
        public VisitIterator visits (Patient? patient = null, DateTime? start = null, 
                                     DateTime? end = null, DataProvider.SortOrder order = DataProvider.SortOrder.DESCENDING) {
            if (!check_connection ()) {
                var empty_array = Json.array ();
                return new NetworkedVisitIterator (this, empty_array);
            }
            
            string response = "";
            if (patient == null)
                response = api_request ("visits",
                    RpcType.STRING, "start_date", (start == null) ? "" : SqliteDataProvider.datetime_to_string (start),
                    RpcType.STRING, "end_date", (end == null) ? "" : SqliteDataProvider.datetime_to_string (end));
            else
                response = api_request ("visits",
                    RpcType.INTEGER, "patient", patient.id,
                    RpcType.STRING, "start_date", (start == null) ? "" : SqliteDataProvider.datetime_to_string (start),
                    RpcType.STRING, "end_date", (end == null) ? "" : SqliteDataProvider.datetime_to_string (end));
            
            Jansson.Error error;
            var node = Json.loads (response, 0, out error);
            
            return new NetworkedVisitIterator (this, node.object_get ("result"));
        }
        
        public Patient? get_patient (int64 id) {
            if (!check_connection ())
                return null;

            var response = api_request ("get_patient", RpcType.INTEGER, "id", id);
            
            Jansson.Error error;
            var node = Json.loads (response, 0, out error);
            
            var patient = new Patient.from_json (node.object_get ("result"), this);
            return patient;
        }
        
        public int64 save_patient (Patient patient) {
            var response = api_request ("save_patient", 
                RpcType.JSON_OBJECT, "patient", patient.to_json ());
            Jansson.Error error;
            var id = Json.loads (response, 0, out error).object_get ("result")
                .object_get ("id").integer_value ();
            if (patient.id == 0) {
                patient.id = id;
                patient_added (id);
            }
            else
                patient_changed (id);
            return id;
        }
        
        public int64 remove_patient (Patient patient) {
            if (!check_connection ())
                return -1;

            var response = api_request ("remove_patient", RpcType.INTEGER, "id", patient.id);
            
            Jansson.Error error;
            Json? response_root = Json.loads (response, 0, out error);
            if (response_root == null)
                return -1;
            
            Json? error_el = response_root.object_get ("error");
            if (! error_el.is_null ())
                return -1;
                
            patient_removed (patient.id);
            return 0;
        }
        
        public PatientIterator patients (Doctor? doctor = null) {
            if (!check_connection ()) {
                var node = Json.array ();
                return new NetworkedPatientIterator (this, node);
            }
            
            string response;
            if (doctor != null)
                response = api_request ("patients", 
                    RpcType.INTEGER, "doctor", doctor.id);
            else
                response = api_request ("patients");
                
            Jansson.Error error;
            var node = Json.loads (response, 0, out error);
                
            if (node == null) {
                return new NetworkedPatientIterator (this, Json.array ());
            }
            
            return new NetworkedPatientIterator (this, node.object_get ("result"));
        }
        
        public Doctor? get_doctor (int64 id) {
            if (!check_connection ())
                return null;

            var response = api_request ("get_doctor", RpcType.INTEGER, "id", id);
            Jansson.Error error;
            var node = Json.loads (response, 0, out error);
            var doctor = new Doctor.from_json (node.object_get ("result"), this);
            
            return doctor;
        }
        
        public int64 save_doctor (Doctor doctor) {
            var response = api_request ("save_doctor", RpcType.JSON_OBJECT, "doctor", 
                doctor.to_json ());
            Jansson.Error error;
            var id = Json.loads (response, 0, out error).object_get ("result")
                .object_get ("id").integer_value ();
            if (doctor.id == 0) {
                doctor.id = id;
                doctor_added (id);
            }
            else
                doctor_changed (id);
            return id;
        }
        
        public int64 remove_doctor (Doctor doctor) {
            if (!check_connection ())
                return -1;
                
            var response = api_request ("remove_doctor", RpcType.INTEGER, "id", 
                doctor.id);
            
            Jansson.Error error;
            Json? response_root = Json.loads (response, 0, out error);
            if (response_root == null)
                return -1;
            
            Json? error_el = response_root.object_get ("error");
            if (! error_el.is_null ())
                return -1;
                
            doctor_removed (doctor.id);
            return 0;
        }
        
        public DoctorIterator doctors () {
            if (!check_connection ()) {
                var node = Json.array ();
                return new NetworkedDoctorIterator (this, node);
            }
            
            var response = api_request ("doctors");
            Jansson.Error error;
            var node = Json.loads (response, 0, out error);
            
            if (node == null) {
                return new NetworkedDoctorIterator (this, Json.array ());
            }
            
            return new NetworkedDoctorIterator (this, node.object_get ("result"));
        }
        
        public Event? get_event (int64 id) {
            if (!check_connection ())
                return null;

            var response = api_request ("get_event", RpcType.INTEGER, "id", id);
            
            Jansson.Error error;
            var event = new Event.from_json (Json.loads (response, 0, out error).object_get ("result"), this);
            
            return event;
        }
        
        public int64 save_event (Event event) {
            var response = api_request ("save_event", RpcType.JSON_OBJECT,
                "event", event.to_json ());
            Jansson.Error error;
            var id = Json.loads (response, 0, out error).object_get ("result")
                .object_get ("id").integer_value ();
            if (event.id == 0) {
                event.id = id;
                event_added (id);
            }
            else
                event_changed (id);
            return id;
        }
        
        public int64 remove_event (Event event) {
            if (!check_connection ())
                return -1;

            var response = api_request ("remove_event", RpcType.INTEGER, "id", event.id);
            
            Jansson.Error error;
            Json? response_root = Json.loads (response, 0, out error);
            if (response_root == null)
                return -1;
            
            Json? error_el = response_root.object_get ("error");
            if (! error_el.is_null ())
                return -1;
            
            event_removed (event.id);
            return 0;
        }
        
        public EventIterator events (DateTime? from = null, DateTime? to = null) {
            if (!check_connection ()) {
                var empty_array = Json.array ();
                return new NetworkedEventIterator (this, empty_array);
            }
            
            var response = api_request ("events", 
                RpcType.STRING, "start_date", (from == null) ? "" : SqliteDataProvider.datetime_to_string (from),
                RpcType.STRING, "end_date", (to == null) ? "" : SqliteDataProvider.datetime_to_string (to));

            Jansson.Error error;
            var node = Json.loads (response, 0, out error);
                
            if (node == null) {
                return new NetworkedEventIterator (this, Json.array ());
            }
            
            return new NetworkedEventIterator (this, node.object_get ("result"));
        }
    } 
}
