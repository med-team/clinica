/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
namespace Clinica {

    public interface MedicineSearchEngine : Object {
    
        /**
         * @brief Return the name of the search engine.
         */
        public abstract string get_name ();
    
        /**
         * @brief This method should be implemented by the MedicalSearchEngine
         * and should lookup for medicines matching the given key and
         * call push_result for each one.
         */ 
        public abstract void search_medicine (string key, MedicineTreeView treeview);
        
        /**
         * @brief Abort any search that the search engine is currently doing.
         */
        public abstract void abort_search ();
    }
}
