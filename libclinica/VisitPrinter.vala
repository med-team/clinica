/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Cairo;
 
 namespace Clinica {
 
    public class VisitPrinter : GLib.Object {
    
        private enum State {
            HEADER,
            ANAMNESIS,
            PHYSICAL_EXAMINATION,
            LABORATORY_EXAM,
            HISTOPATHOLOGY,
            DIAGNOSIS,
            TOPICAL_THERAPY,
            SYSTEMIC_THERAPY,
            SUBSEQUENT_CHECKS,
            DONE
        }
        
        private State current_state;

        public Visit visit;
        
        public double width  { get; set; default = 595.275; }
        public double height { get; set; default = 841.889; }
        public double margin { get; set; default = 35; }
        private int page_number { get; set; default = 1; }
        
        /* Fonts */
        public Pango.FontDescription font_description { get; set; }
        public Pango.FontDescription field_empty_font { get; set; }
        
        private double y_level;
        
        public bool handle_pagination { get; set; default = true; }
        
        private ResourceManager resource_manager;
        
        public VisitPrinter (ResourceManager resources, Visit v) {
            resource_manager = resources;
            visit = v;
            current_state = State.HEADER;
        }
        
        public void print_report_to_pdf (string destination) {
            /* Create surface and context so we can draw anything we want on
             * the pdf */
            var surface = new PdfSurface (destination, width, height);
            var ctx = new Context (surface);
            
            print_report (ctx);
        }
        
        /**
         * @brief Print the report to surface.
         * @params ctx The context that must be used for printing.
         * @returns The total number of pages printed or -1 if there are still pages
         * to be printed. 
         */
        public int print_report (Context ctx, int max_pages = 65536) {
            int starting_page = page_number;
        
            /* Start from the top of the sheet */
            y_level = 0;
            
            /* Create a font description for the content text */
            font_description = new Pango.FontDescription ();
            font_description.set_family ("Helvetica");
            font_description.set_size (9 * Pango.SCALE);
            
            /* And a font description used for field empty message */
            field_empty_font = new Pango.FontDescription ();
            field_empty_font.set_family ("Helvetica");
            field_empty_font.set_size (9 * Pango.SCALE);
            field_empty_font.set_style (Pango.Style.ITALIC);
            
            while (page_number - starting_page < max_pages) {
                
                switch (current_state) {
                    case State.HEADER:
                        /* The main header */
                        write_header (ctx);
                        current_state = State.ANAMNESIS;
                        break;
                    case State.ANAMNESIS:
                        /* Anamnesis */
                        write_block (ctx, _("Anamnesis"), visit.anamnesis);
                        current_state = State.PHYSICAL_EXAMINATION;
                        break;
                    case State.PHYSICAL_EXAMINATION:
                        /* Physical examination */
                        write_block (ctx, _("Physical examination"), visit.physical_examination);                
                        current_state = State.LABORATORY_EXAM;
                        break;
                    case State.LABORATORY_EXAM:
                        /* Laboratory Exam */
                        write_block (ctx, _("Laboratory exam"), visit.laboratory_exam);                    
                        current_state = State.HISTOPATHOLOGY;
                        break;
                    case State.HISTOPATHOLOGY:
                        /* Histopathology */
                        write_block (ctx, _("Histopathology"), visit.histopathology);
                        current_state = State.DIAGNOSIS;              
                        break;
                    case State.DIAGNOSIS:
                        /* Diagnosis */
                        write_block (ctx, _("Diagnosis"), visit.diagnosis);
                        current_state = State.TOPICAL_THERAPY;
                        break;
                    case State.TOPICAL_THERAPY:
                        /* Topical therapy */
                        write_block (ctx, _("Topical therapy"), visit.topical_therapy);                
                        current_state = State.SYSTEMIC_THERAPY;
                        break;
                    case State.SYSTEMIC_THERAPY:            
                        /* Systemic therapy */
                        write_block (ctx, _("Systemic therapy"), visit.systemic_therapy);                
                        current_state = State.SUBSEQUENT_CHECKS;
                        break;
                    case State.SUBSEQUENT_CHECKS:
                        /* Subsequent checks */
                        write_block (ctx, _("Subsequent checks"), visit.subsequent_checks);                    
                        current_state = State.DONE;
                        break;
                    case State.DONE:
                        finalize_page (ctx);
                        return page_number - 1;
                    default:
                        warning ("Visit printer is in an unrecognized state. Aborting print");
                        return -1;
                }
            }
            
            return -1;
        }
        
        /**
         * @brief Finalize the page that is being written adding final touches, such
         * as page number and signature on the bottom, and prepare cairo to write to
         * the next one
         */
        private void finalize_page (Context ctx) {
            TextExtents extents;
        
            /* Ready to write on the next page */
            y_level = margin;
            
            /* Page number on the bottom */
            string text = "%d".printf (page_number);
            ctx.set_font_size (10.0);
            ctx.select_font_face ("Helvetica", FontSlant.NORMAL, FontWeight.NORMAL);
            ctx.text_extents (text, out extents);
            ctx.move_to (width - margin - extents.x_bearing - extents.width, 
                         height - margin + 10 - extents.y_bearing + extents.height);
            ctx.show_text (text);
            page_number++;
            
            /* Create next page */
            if (handle_pagination)
                ctx.show_page ();
        }
        
        /**
         * @brief Write the header of the visit
         */
        private void write_header (Context ctx) {
            TextExtents extents;
            string text;
            
            var base_level = y_level + margin;
            
            /* On the right, write the information about the doctor */
            var name = resource_manager.settings.get_string ("personal-details-name");
            var address = resource_manager.settings.get_string ("personal-details-address"); 
            var institution = resource_manager.settings.get_string ("personal-details-institution"); 
            var email = resource_manager.settings.get_string ("personal-details-email"); 
            if (name != "") {
                ctx.set_font_size (14.0);
                ctx.select_font_face ("Helvetica", FontSlant.NORMAL, FontWeight.BOLD);
                text = name;
                ctx.text_extents (text, out extents);
                ctx.move_to (width - margin - extents.width - extents.x_bearing,
                             base_level - extents.y_bearing + extents.height);
                ctx.show_text (text);
                base_level += extents.height + 6;
                
                ctx.set_font_size (11.0);
                ctx.select_font_face ("Helvetica", FontSlant.NORMAL, FontWeight.NORMAL);
                
                if (institution != "") {
                    text = institution;
                    ctx.text_extents (text, out extents);
                    ctx.move_to (width - margin - extents.width - extents.x_bearing,
                                 base_level - extents.y_bearing + extents.height);
                    ctx.show_text (text);
                    base_level += extents.height + 6;
                }
                
                if (address != "") {
                    text = address;
                    ctx.text_extents (text, out extents);
                    ctx.move_to (width - margin - extents.width - extents.x_bearing,
                                 base_level - extents.y_bearing + extents.height);
                    ctx.show_text (text);
                    base_level += extents.height + 6;
                }
                
                if (email != "") {
                    text = email;
                    ctx.text_extents (text, out extents);
                    ctx.move_to (width - margin - extents.width - extents.x_bearing,
                                 base_level - extents.y_bearing + extents.height);
                    ctx.show_text (text);
                    base_level += extents.height + 6;                
                }
                
                base_level += 20;
            }
            
            /* Create the logo of Clinica on the left. */
            try {
                var resource = Config.RESOURCE_BASE + "ui/icons/clinica.png";
                var stream = GLib.resources_open_stream (resource, ResourceLookupFlags.NONE);
                
                // Read the image from the stream obtained by the GResource
                var image = new Cairo.ImageSurface.from_png_stream ((data) => {
                    try {
                        stream.read (data);
                    } catch (Error e) {
                        return Cairo.Status.READ_ERROR;
                    }
                    return Cairo.Status.SUCCESS;
                });
                
                ctx.move_to (0, margin);
                ctx.set_source_surface (image, 48, 48);
                ctx.paint ();
                y_level += 48 + margin + 30;
             } catch (Error e) {
                warning ("Cannot load clinica icon from resources: %s".printf (e.message));
             }
            
            ctx.set_source_rgb (0.0, 0.0, 0.0);
            y_level = (y_level > base_level) ? y_level : base_level;
            
            /* Create an horizontal line that separates the header with doctor information
             * from the one with the doctor information */
            ctx.move_to (margin, y_level);
            ctx.set_line_width (1.0);
            ctx.line_to (width - margin, y_level);
            ctx.stroke ();
            y_level += 10;

            /* Begin by writing on the top left the information about the patient */
            text = visit.patient.get_complete_name ();
            ctx.select_font_face ("Helvetica", FontSlant.NORMAL, FontWeight.BOLD);
            ctx.set_font_size (14.0);
            ctx.text_extents (text, out extents);
            ctx.move_to (margin, 
                         y_level - extents.y_bearing + extents.height);
            ctx.show_text (text);
            y_level += extents.height + 5;
            
            /* Now write the date of the visit after that */
            text = _("Report of the visit of %s").printf (visit.date.format ("%F"));
            ctx.select_font_face ("Helvetica", FontSlant.NORMAL, FontWeight.NORMAL);
            ctx.set_font_size (11.0);
            ctx.text_extents (text, out extents);
            ctx.move_to (margin,
                         y_level - extents.y_bearing + extents.height);
            ctx.show_text (text);
            y_level += extents.height + 50;
        }
        
        /**
         * @brief Write the title of a section on the PDF, followed by the content
         * of the block. If not enough space is present in the page, a new one
         * is started.
         */
        private void write_block (Context ctx, string title, string content) {
            TextExtents extents;
            Pango.Rectangle logical_rect, ink_rect;
            
            /* Create the title */
            ctx.select_font_face ("Helvetica", FontSlant.NORMAL, FontWeight.BOLD);
            ctx.set_font_size (12.0);
            ctx.text_extents (title, out extents);
            
            /* And the pango rendering for the content */
            var layout = Pango.cairo_create_layout (ctx);
            layout.set_font_description (font_description);
            layout.set_width ((int) (width - 2 * margin) * Pango.SCALE);
            if (content != "")
                layout.set_text (content, -1);
            else {
                layout.set_text (_("Field empty"), -1);
                layout.set_font_description (field_empty_font);
            }
            layout.get_extents (out ink_rect, out logical_rect);
            
            /* Show text now, but only if it fits in the page, otherwise we need to
             * create a new page to hold the content */
            if (y_level - extents.y_bearing + extents.height +  
                (logical_rect.height/Pango.SCALE) + margin > height) {
                finalize_page (ctx);
                
                /* Reselect font face since it will be reset by finalize_page () to write
                 * the page number on the bottom */
                ctx.select_font_face ("Helvetica", FontSlant.NORMAL, FontWeight.BOLD);
                ctx.set_font_size (12.0);
            }
            
            ctx.move_to (margin, y_level - extents.y_bearing + extents.height);
            y_level += extents.height - extents.y_bearing + 5;
            ctx.show_text (title);
            ctx.move_to (margin, y_level);
            Pango.cairo_show_layout (ctx, layout);
            y_level += logical_rect.height/Pango.SCALE + 10;
        }
    }
 }
