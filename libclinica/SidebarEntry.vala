/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

namespace Clinica {

    /**
     * @brief This interface represent a generic entry in the Sidebar
     * of Clinica. 
     */
    public interface SidebarEntry : Object {
    
        /**
         * @brief The name to be displayed in the Sidebar for this
         * element.
         */
        public abstract string title ();
        
        /**
         * @brief The Pixmap element to be drawn in the sidebar, or 
         * null if no icon is needed.
         */
        public abstract Gdk.Pixbuf? pixbuf ();
        
        /**
         * @brief This signal is fired when the sidebar entry gets
         * activated by the user.
         */
        public signal void activated ();
    
    }

}
