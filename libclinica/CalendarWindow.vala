/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 
 namespace Clinica {
 
    public class CalendarWindow : Gtk.Window {
    
        internal ResourceManager resource_manager { get; set; }
        internal CalendarView    calendar_view    { get; set; }
        internal CalendarEventList event_widget     { get; set; }
        internal  CalendarToolbar toolbar { get; private set; }
    
        public CalendarWindow (ResourceManager resources) {
            GLib.Object (type: WindowType.TOPLEVEL);
            resource_manager = resources;
            
            set_title (_("Clinica calendar"));
            
            /* The Toolbar used to switch */
            toolbar = new CalendarToolbar (resource_manager, this);
            
            /* Create the calendar view */
            calendar_view = new CalendarView (resource_manager, toolbar);
            
            /* And the widget that display the events present in the calendar.
             * This is a label that does nothing for the moment being. */
            event_widget = new CalendarEventList (resource_manager);
            var event_viewport = new ScrolledWindow (null, null);
            event_viewport.set_policy (PolicyType.NEVER, PolicyType.AUTOMATIC);
            event_viewport.add_with_viewport (event_widget);
            
            /* Pack widgets in the box */
            var main_table = new Grid ();
            main_table.attach (toolbar, 0, 0, 2, 1);
            toolbar.hexpand = true;
            toolbar.vexpand = false;
            main_table.attach (calendar_view, 0, 1, 1, 1);
            calendar_view.expand = true;
            main_table.attach (event_viewport, 1, 1, 1, 1);
            event_viewport.vexpand = true;
            event_viewport.hexpand = false;
            
            /* Create an alignment to set the right padding of the window */
            var alignment = new Alignment (0.5F, 0.5F, 1.0F, 1.0F);
            alignment.add (main_table);
            add (alignment);
            
            /* Connect events for date chagend */
            calendar_view.day_selected.connect (on_day_selected);
        }
        
        public void on_day_selected (DateTime date) {
            event_widget.set_day (date);
        }
    }
 }
