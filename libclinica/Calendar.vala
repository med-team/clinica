/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 *            Maxwell Barvian from Maya - http://launchpad.net/maya/
 */

namespace Clinica {

	public class Calendar : Gtk.Grid {
	
		public DateHandler handler { get; private set; }
		private CalendarView calendar_view;
		private ResourceManager resource_manager { get; set; }
		
		internal Day[] days;
		
		internal int days_to_prepend {
			get {
				int days = 1 - handler.first_day_of_month + 0; // window.prefs.week_starts_on;
				return days > 0 ? 7 - days : -days;
			}
		}
	
		public Calendar (ResourceManager resources, CalendarView view) {
		    this.resource_manager = resources;
		    this.calendar_view = view;
		
			handler = new DateHandler ();
		
			/* Gtk.Table properties */
			int n_rows = 6;
			int n_columns = 7;
			column_spacing = 0;
			row_spacing = 0;
			row_homogeneous = column_homogeneous = true;
			
			// Initialize days
			days = new Day[n_rows * n_columns];
			for (int row = 0; row < n_rows; row++)
				for (int col = 0; col < n_columns; col++) {
					var day = new Day (calendar_view);
					days[row * n_columns + col] = day;
					attach (day, col, row, 1, 1);
				}
			
			/* Signals and handlers */
			handler.changed.connect (update_month);
			
			/* Change today when it changes */
			var today = new DateTime.now_local ();		
			var tomorrow = today.add_full (0, 0, 1, -today.get_hour (), -today.get_minute (), -today.get_second ());
			var difference = tomorrow.to_unix() - today.to_unix();
			
			Timeout.add_seconds ((uint) difference, () => {
				if (handler.current_month == tomorrow.get_month () && handler.current_year == tomorrow.get_year ())
					update_month ();
				
				tomorrow = tomorrow.add_days (1);
				
				Timeout.add (1000 * 60 * 60 * 24, () => {
					if (handler.current_month == tomorrow.get_month () && handler.current_year == tomorrow.get_year ())
						update_month ();
				
					tomorrow = tomorrow.add_days (1);
					
					return true;
				});
				
				return false;
			});
			
			resource_manager.data_provider.event_added.connect (on_event_added);
			resource_manager.data_provider.event_changed.connect (on_event_changed);
			resource_manager.data_provider.event_removed.connect (on_event_removed);
			resource_manager.data_provider.visit_added.connect (on_visit_added);
			resource_manager.data_provider.visit_changed.connect (on_visit_changed);
			resource_manager.data_provider.visit_removed.connect (on_visit_removed);
			realize.connect (() => set_date (today));
		}
		
		public void focus_today () {
			set_date (new DateTime.now_local ());
		}
		
		public void set_date (DateTime date) {
			if (handler.current_month != date.get_month () || handler.current_year != date.get_year ())
				handler.add_full_offset (date.get_month () - handler.current_month, date.get_year () - handler.current_year);
				
			days[date_to_index (date.get_day_of_month ())].grab_focus ();
		}
		
		internal void update_month () {
		
			int month = handler.current_month;
			int year = handler.current_year;
			
			foreach (var day in days) {
				/* Clear events of the day */
			    day.reset_events ();
			}
			
      		var date = new DateTime.local (year, month, 1, 0, 0, 0).add_days (-days_to_prepend);
			
    	    foreach (var visit in resource_manager.data_provider.visits (null, date, date.add_days (42))) {
			        /* Display visits in the day */
			        days[visit.date.difference (date) / 86400000000].add_visit (visit);
		    }
       	    foreach (var event in resource_manager.data_provider.events (date, date.add_days (42))) {
			        /* Display visits in the day */
			        days[event.date.difference (date) / 86400000000].add_event (event);
		    }
		}
		
		private int date_to_index (int day_of_month) {
			return days_to_prepend + day_of_month - 1;
		}
		
		/**
		 * @brief Get the selected day or null if no day
		 * selected. 
		 */
		public DateTime? get_selected_date () {
		    return calendar_view.selected_date;
		}
		
		/**
		 * @brief Callback called when an event is pushed to the EventListStore
		 *
		 * It should find the day of the event, if present in the view, and 
		 * insert the events in it.
		 */
		private void on_event_added (int64 event_id) {
		    update_month ();
		}
		
		private void on_event_changed (int64 event_id) {
		    update_month ();
		}
		
		private void on_visit_added (int64 visit_id) {
		    update_month ();
		}
		
		private void on_visit_removed (int64 visit_id) {
		    update_month ();
		}
		
		private void on_visit_changed (int64 visit_id) {
		    update_month ();
		}
		
		/**
		 * @brief Callback called when an event is removed from the EventListStore
		 *
		 * It should find the day that contained the event and remove it from the
		 * view. 
		 */
		private void on_event_removed (int64 event_id) {
		    update_month ();
		}
		
	}

}

