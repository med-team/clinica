/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
 namespace Clinica {
 
    /**
     * @brief Generic store that allows to store files 
     * with a key based approach. 
     *
     * One key correspond to one or more files (or even no 
     * one).
     */
    public class LocalFileStore : GLib.Object, FileStore {
    
        /**
         * @brief The resource manage assoiated with this Clinica application instance.
         */
        private ResourceManager resource_manager;
        
        /**
         * @brief List of directories that are monitored by the
         * GioFileMonitor, so we don't monitor the twice.
         */
        private GLib.List<int64?> monitored_ids;
        
        public LocalFileStore (ResourceManager resources) {
            resource_manager = resources;
            error.connect ((t,l) => resource_manager.error_callback (t,l));
            
            monitored_ids = new GLib.List<int64?> ();
            
            /* Setup some monitored folders */
            try {
                File st = File.new_for_path (resource_manager.get_filestore_dir ());
                FileEnumerator enumerator = st.enumerate_children (FileAttribute.STANDARD_NAME, FileQueryInfoFlags.NONE);
                
                FileInfo info;
                while ((info = enumerator.next_file ()) != null) {
                    int64 id = int64.parse (info.get_name ());
                    if (id.to_string () == info.get_name ()) {
                        setup_monitor (id);
                    }
                }

            } catch (GLib.Error e) {
                warning (_("Unable to setup file monitors for the filestore folder: %s").printf (e.message));
            }
        }
        
        /**
         * @brief Get the path where the files related to the given ID
         * are stored.
         */
        internal string get_id_path (int64 id) {
            string path = Path.build_filename (resource_manager.get_filestore_dir (),
                                               id.to_string ());                      
                                                
            /* Check that the path exists before returning */
            File dir_path = File.new_for_path (path);
            if (!dir_path.query_exists ()) {
                DirUtils.create_with_parents (path, -1);
            }
            
            /* Setup the monitor on the file so we will get notifications
             * for created files, that are likely to appear since someone
             * requested the path explicitly. */
            setup_monitor (id);
            
            return path;
        }
        
        /**
         * @brief Setup a FileMonitor on the given directory ID, so we
         * may check for files created in there.
         */
        private void setup_monitor (int64 id) {
            if (monitored_ids.find (id) != null) {
                return;
            }
        
            File dir_file = File.new_for_path (Path.build_filename (resource_manager.get_filestore_dir (),
                                                id.to_string ()));
            
            try {
                FileMonitor monitor = dir_file.monitor (FileMonitorFlags.NONE);
                monitor.changed.connect (on_dir_changed);
                    
                monitored_ids.append (id);
            } catch (GLib.Error e) {
                warning (_("Unable to setup a file monitor on directory %s").printf (dir_file.get_path ()));
            }
        }
        
        private void on_dir_changed (File monitored_file, File? other_files, FileMonitorEvent event) {
            File feasible_dir = monitored_file.get_parent ();
            int id = int.parse (feasible_dir.get_basename ());
            if (id != 0)
                changed (id);
        }
        
        /** 
         * @brief Get the list of files associated to an ID.
         * This may even be an empty List.
         */
        public GLib.List<FileObject> get_files (int64 id) {
            // Get the local data directory
            string this_id_path = get_id_path (id);
            File d = File.new_for_path (this_id_path);
            
            // Prepare the list with the output files
            GLib.List<FileObject> results = new GLib.List<FileObject> ();
            
            if (!d.query_exists ())
                return results;
            
            try {
                FileEnumerator enumerator = d.enumerate_children (FileAttribute.STANDARD_NAME, FileQueryInfoFlags.NONE);
            
                FileInfo info;
                while ((info = enumerator.next_file ()) != null) {
                    results.append (new FileObject (this, id,
                        Path.build_filename (this_id_path, info.get_name ()), true));
                }
            } catch (GLib.Error e) {
                error (_("Error while listing the files for the visit identified by id %d").printf (id));
            }
            
            return results;
        }
        
        /**
         * @brief Store a file called filename in the FileStore.
         */
        public string store_file (int64 id, string filename) {
            // Source file
            File source_file = File.new_for_path (filename);
            
            // Get the local data directory
            string this_id_path = Path.build_filename (resource_manager.get_filestore_dir (), id.to_string ());
            File d = File.new_for_path (this_id_path);
            
            if (!d.query_exists ()) {
                debug ("Creating directory %s".printf (d.get_path ()));
                GLib.DirUtils.create_with_parents (d.get_path (), -1);
                
                setup_monitor (id);
            }
                
            File dest_file = File.new_for_path (Path.build_filename (d.get_path (), source_file.get_basename ()));
            if (dest_file.query_exists ())
                warning (_("Destination file already exists"));
                
            try {
                source_file.copy (dest_file, FileCopyFlags.OVERWRITE);
            } catch (GLib.Error e) {
                error (_("Error while adding the file %s to the selected visit").printf (source_file.get_basename ()));
            }
            
            /* Emit the changed signal */
            changed (id);
            
            return dest_file.get_path ();
        }
        
        /**
         * @brief Remove a file from the store.
         */
        public void remove_file (int64 id, string filename) {
            FileUtils.remove (Path.build_filename (resource_manager.get_filestore_dir (),
                                                   id.to_string (), filename));
                                                               
            /* Emit the changed signal */
            changed (id);
        }
    
    
    }
 
 }
