/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 *            Maxwell Barvian from Maya - http://launchpad.net/maya/
 */

using Gtk;
using Gdk;

namespace Clinica {

    const string css_data = """
@define-color bg_color rgba(255, 255, 255, 0.75);

/* Header Styles */
.header {
        background-image: -gtk-gradient (linear,
                        left top, left bottom,
                        from(shade(@bg_color, 0.9)), to(mix(@bg_color, rgb(255, 255, 255), 0.5)));
}

/* Cell Styles */
.cell {
        background-color: @bg_color;
        border-radius: 0;
        border-color: #000000;
}

.cell:insensitive {
        background-color: shade(@bg_color, 0.97);
}

.cell:focused {
        background-color: shade(@bg_color, 0.9);
}

#today, #today:focused {
        background-color: mix(@bg_color, rgb(0, 0, 255), 0.08); /* today date has nice shade of blue */
}

        .cell > #date {
                font-size: 8;
        }    
""";

	public class CalendarView : Gtk.VBox {
	
	    /* Signal emitted when a day gets selected */
	    public signal void day_selected (DateTime date);
	
		public CalendarHeader header { get; private set; }
		public Calendar calendar { get; private set; }
		public CssProvider style_context { get; private set; }
		public CalendarToolbar toolbar   { get; set; }
		internal ResourceManager resource_manager { get; set; }
		
		/**
		 * @brief The date selected in the calendar, or null.
		 */
		internal DateTime? selected_date = null;
	
		public CalendarView (ResourceManager resources, CalendarToolbar toolbar) {
		    Object (spacing: 0, homogeneous: false, resource_manager: resources,
		            toolbar: toolbar);
		            
   		    create_interface ();
		}
		    
		/**
		 * @brief Create the various pieces of the interface-
		 */
		private void create_interface () {
		    /* Create CssProvider */
			style_context = new CssProvider ();
			try {
    			style_context.load_from_data (css_data, -1);
    		} catch (GLib.Error error) {
    		    assert_not_reached ();
    		}
			
			/* Create elements of the calendar view */
			header = new CalendarHeader (this);
			calendar = new Calendar (resource_manager, this);
			
			pack_start (header, false, false, 0);
			pack_end (calendar, true, true, 0);
			
			/* Connect some callbacks */
			if (calendar != null)
    			calendar.handler.changed.connect (update_month);
    			
    	    if (toolbar != null) {
			    toolbar.month_switcher.left_clicked.connect (() => calendar.handler.add_month_offset (-1));
                toolbar.month_switcher.right_clicked.connect (() => calendar.handler.add_month_offset (1));
                toolbar.year_switcher.left_clicked.connect (() => calendar.handler.add_year_offset (-1));
                toolbar.year_switcher.right_clicked.connect (() => calendar.handler.add_year_offset (1));
            }
            
            /* Trigger an update, otherwise the label will be void */
            if (resource_manager != null)
                update_month ();
		}
		
		private void on_day_selected (DateTime date) {
		    /* Re-emit the day_selected signal */
		    day_selected (date);
		    
		    selected_date = date;
		}
		
		
		private void update_month () {
		    /* Deselect the date, since we are changing month */
		    selected_date = null;
		
            var today = new DateTime.now_local ();
            int month = calendar.handler.current_month;
            int year = calendar.handler.current_year;
	    
            var date = new DateTime.local (year, month, 1, 0, 0, 0).add_days (-calendar.days_to_prepend);

            foreach (var day in calendar.days) {
                    if (date.get_day_of_year () == today.get_day_of_year () && date.get_year () == today.get_year ()) {
                            day.name = "today";
                            day.can_focus = true;
                            day.sensitive = true;
                            
                    } else if (date.get_month () != month) {
                            day.name = null;
                            day.can_focus = false;
                            day.sensitive = false;
                    } else {
                            day.name = null;
                            day.can_focus = true;
                            day.sensitive = true;
                    }
                    
                    if (day.can_focus) {
                        /* Connect the day focus to the callback on the
                         * EventList */
                        day.selected.connect (on_day_selected);
                    }

                    day.date = date;
                    date = date.add_days (1);
            }
            
            /* Update events displayed in the calendar */
            calendar.update_month ();
            
            /* Update switcher text */
            toolbar.month_switcher.text = calendar.handler.format ("%B");
            toolbar.year_switcher.text = calendar.handler.format ("%Y");
		}
		
	}

}

