/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using GLib;
using Sqlite;
using Gtk;

namespace Clinica {
 
    /**
     * @brief Type representing the callback for the error
     * signal.
     */
    public delegate void ErrorCallback(GLib.Object source, string message);

    /**
     * @brief Grant access to resources such as
     * UI files, configuration and database
     */
    public class ResourceManager : GLib.Object {
    
        /**
         * @brief The padding the should be used in windows
         * in Clinica.
         */
        public int PADDING = 6;
        
        /**
         * @brief GtkApplication of Clinica.
         */
        internal Clinica.Application application;
    
        /**
         * @brief Callback that manages errors
         * notification to the end user.
         */
        public unowned ErrorCallback error_callback;
        
        /**
         * @brief PatientListStore that holds the list
         * of the patients
         */
        public PatientListStore patient_list_store;
        
        /**
         * @brief DoctorListStore that holds the list
         * of the doctors.
         */
        public DoctorListStore doctor_list_store;
        
        /**
         * @brief VisitListStore that holds the list
         * of the visits.
         */
        public VisitListStore visit_list_store;
        
        /**
         * @brief EventListStore that holds the list of
         * the events scheduled in clinica.
         */
        public EventListStore event_list_store;
        
        /**
         * @brief This is the object used to retrieve all the data
         * about patients and which events should be connected
         * to the views to make them react to changes in the database
         * or whatever the data source is.
         */
        internal DataProvider data_provider { get; private set; }
        
        /**
         * @brief A NetworkedDataProvider that can communicate with other clinica
         * instances on the LAN. 
         */
        internal NetworkedDataProvider networked_data_provider;
        
        /**
         * @brief DataProvider bounded to the local database.
         */
        internal SqliteDataProvider sqlite_data_provider;
        
        /**
         * @brief Reference to a user interface
         */
        public UserInterface user_interface;
        
        /**
         * @brief Clinica settings
         */
        public Settings settings;
    
        /**
         * @brief Signal emitted when an error
         * occurr. The user should be notified
         * with message by the application
         */
        public signal void error (string message);
    
        /**
         * @brief Local configuration directory
         * obtained at runtime.
         */
        internal string local_config_directory;
        
        /**
         * @brief Local data directory obtained 
         * at runtime.
         */
        internal string local_data_directory;

#if HAVE_PLUGINS
        /**
         * @brief PluginEngine for Clinica
         */
        public PluginEngine plugin_engine;
#endif
        
        /**
         * @brief Array containing the valid medicine search engines that
         * are available at the moments. This should be used by plugins to
         * register themselves as valid search engines
         */
        internal GLib.List<unowned MedicineSearchEngine> medicine_search_engines;
        
        /**
         * @brief Instance of the local Medicines search engine.
         */
        internal LocalMedicinesDatabase local_medicines_search_engine;
        
        /** 
         * @brief List containing the available DataProviders that 
         * can be used to retrieve data in Clinica. 
         */
        internal List<DataProvider> data_providers;
        
        /**
         * @brief A pointer to the patient that is been dragged. Should
         * be set on drag_begin by the treeview and reset to null by the
         * drag destination handler. 
         */
		internal Patient? dragging_patient = null;
		
		/**
		 * @brief A pointer to the DataServer instance if is activated, or null
		 * if is disabled.
		 */
		internal DataServer? data_server = null;
		
#if HAVE_PLUGINS
		private Peas.ExtensionSet core_set;
#endif

        internal bool persistency_required = false;
        
        /**
         * @brief Prefix where Clinica is installed. 
         */
        internal string prefix;
        
        public const string doctor_table = "doctors";
        public const string patient_table = "patients";
        public const string visits_table = "visits";
        public const string events_table = "events";
        
        /** SIGNALS **/
        internal signal void medicine_search_engines_added (MedicineSearchEngine engine);
        internal signal void medicine_search_engines_removed (MedicineSearchEngine engine);
        internal signal void data_provider_added (DataProvider provider);
        internal signal void data_provider_removed (DataProvider provider);

        public ResourceManager (Clinica.Application? application, 
                                string program_name = "clinica", 
                                ErrorCallback? callback = null){
            if (callback != null)
                error_callback = callback;
            else {
                error_callback = (source, message) => debug (message);
            }
            
            /* Save the GtkApplication */
            this.application = application;
        
            /* Connect error callback */
            error.connect ((t,l) => callback(t,l));
        
            /* Init databases and configuration files
             * if needed. */
            local_config_directory = Environment.get_user_config_dir ();
            local_config_directory = Path.build_filename (local_config_directory,
                                                          "clinica");
            /* Set local configuration directory */                                    
            local_data_directory = Environment.get_user_data_dir ();
            local_data_directory = Path.build_filename (local_data_directory, "clinica");
                                                
            /* Check if configuration directory exists */
            if (!FileUtils.test (local_config_directory, 
                                  FileTest.IS_DIR)) {
                DirUtils.create_with_parents (local_config_directory, 0755);
            }
            
            if (!FileUtils.test (local_data_directory, FileTest.IS_DIR)) {
                DirUtils.create_with_parents (local_data_directory, 0755);
            }
            
            /* Check if the directory for the FileStore exists */
            File fsd = File.new_for_path (get_filestore_dir ());
            if (!fsd.query_exists ())
                DirUtils.create_with_parents (get_filestore_dir (), 0755);
                                                
            /* Find prefix */
            prefix = "/usr/local";
            if (!FileUtils.test (Path.build_filename(prefix, "bin", "clinica"), FileTest.IS_EXECUTABLE)) {
            	prefix = "/usr";
            }
            
            /* Try setting GSETTINGS_SCHEMA_DIR to this directory so if this is 
             * a local build or the Windows version we are able to use the in-place
             * compiled schema. 
             *
             * Do this only if the file "gschemas.compiled" is found.
             */
            if (FileUtils.test ("gschemas.compiled", FileTest.EXISTS)) {
                debug ("Settings GSETTINGS_SCHEMA_DIR to '.' since gschemas.compiled is found");
                Environment.set_variable ("GSETTINGS_SCHEMA_DIR", ".", true);
            }
            
            /* Create configuration */
            settings = new Settings (this);
            
            /* SqliteDataProvider must always be the first DataProvider
             * in the list */
            data_providers = new List<DataProvider> ();
            sqlite_data_provider = new SqliteDataProvider (this);
            register_data_provider (sqlite_data_provider);
            
            // Create the DataServer used to share data from the local DB
            // on the local network.
            data_server = new DataServer (this, data_provider);
            
            /* Start the Server if required and subscribe to change in the settings.
             * to adjust its behaviour. */
            if (settings.get_boolean ("data-server-activated"))
                data_server.start ();
            
            settings.changed["data-server-activated"].connect ((settings, key) => {
                if (settings.get_boolean ("data-server-activated"))
                    data_server.start ();
                else
                    data_server.stop (); 
            });

            /* Instantiate a NetworkDataProvider to communicate with other clinica on the
             * network. */
            networked_data_provider = new NetworkedDataProvider (this);
            register_data_provider (networked_data_provider);
            
            if (settings.get_string ("data-provider") == networked_data_provider.get_name ()) {
                networked_data_provider.connect_to_server (
                    settings.get_string ("network-data-provider-host"),
                    int.parse (settings.get_string ("network-data-provider-port")));
                    
                // Check if the given authentication data is correct or not. 
                if (!networked_data_provider.try_authentication (
                    settings.get_string ("network-data-provider-host"),
                    settings.get_string ("network-data-provider-port"),
                    networked_data_provider.username, networked_data_provider.password)) {
                    
                    // Check if the networked data provider is still the current one. If that's
                    // the case then disconnect and warn the user abount wrong credentials. 
                    if (data_provider == networked_data_provider as DataProvider) {
                        var dialog = new MessageDialog (null, DialogFlags.MODAL, MessageType.ERROR, ButtonsType.OK,
                           "%s",  _("Username or password are wrong. Disconnecting from the server."));
                        dialog.set_title (_("Error while authenticating on the server"));
                        dialog.run ();
                        dialog.destroy ();
                        
                        select_data_provider (sqlite_data_provider);
                        networked_data_provider.disconnect_from_server ();
                    }
                }
            }
                 
            load_stores.begin ();
                  
#if HAVE_PLUGINS
             if (settings.get_boolean ("use-plugins"))
                 load_plugins.begin ();
#endif
        }


#if HAVE_PLUGINS
        private extern Peas.ExtensionSet setup_extension_set (PluginEngine engine);
        
        /**
         * @brief Instantiate the plugin engine and load
         * all the plugins present in the directory
         */
        public async void load_plugins () {
        
            /* Load our internal medicine search engine */
            local_medicines_search_engine = new LocalMedicinesDatabase (this);
            register_medicine_search_engine (local_medicines_search_engine);
        
            /* Setup plugin engine */
            plugin_engine = new PluginEngine (this);   
            plugin_engine.enable_loader ("python");
            
            string [] active_plugins = settings.get_active_plugins ();
            
            foreach (Peas.PluginInfo info in plugin_engine.get_plugin_list ()) {
                if (info.get_module_name () in active_plugins) {
                    debug ("Activating plugin %s".printf (info.get_name ()));
                    plugin_engine.load_plugin (info);
                }
                else {
                    debug ("Plugin %s found but disabled".printf (info.get_name ()));
                }
            }
            
            /* Setup the CoreActivatable extension set, so new DataProviders will be available */
            core_set = setup_extension_set (plugin_engine);
        }
#endif
        
        private async void load_stores () {
            /* Create the store that will be attached to the data provider selected 
             * above */         
            if (doctor_list_store != null)
                return;
            doctor_list_store = new DoctorListStore (this);
            patient_list_store = new PatientListStore (this);
            visit_list_store = new VisitListStore (this);
            event_list_store = new EventListStore (this);
        }
        
        private async void reload_stores () {
            // Reload the stores only if they are not already loaded. 
            debug ("Reloading stores");
            if (doctor_list_store == null) {
                load_stores.begin ();
                return;
            }
            doctor_list_store.clear ();
            doctor_list_store.load_data ();
            patient_list_store.clear ();
            patient_list_store.load_data ();
            visit_list_store.clear ();
            visit_list_store.load_data ();
            event_list_store.clear ();
            event_list_store.load_data ();
        }
        
        public void register_data_provider (DataProvider provider) {
            data_providers.append (provider);
            data_provider_added (provider);
            
            if (data_provider == null)
                data_provider = provider;
                
            if (provider.get_name () == settings.get_string ("data-provider"))
                data_provider = provider;
        }
        
        public void unregister_data_provider (DataProvider provider) {
            if (provider == data_provider) {
                data_provider = data_providers.nth_data (0);
                settings.set_string ("data-provider", "");
            }
        }
        
        internal void select_data_provider (DataProvider provider) {
            if (data_provider == provider)
                return;

            data_provider = provider;
            settings.set_string ("data-provider", 
                provider.get_name ());

            reload_stores.begin ();
        }
        
        public void register_medicine_search_engine (MedicineSearchEngine engine) {
            medicine_search_engines.append (engine);
            
            var name = engine.get_name ();
            var active_names = settings.get_strv ("medicine-search-engines");
            
            foreach (var active_name in active_names) {
                if (active_name == name) {
                    settings.selected_search_engines.append (engine);
                    break;
                }
            }
            
            medicine_search_engines_added (engine);
        }
        
        public bool unregister_medicine_search_engine (MedicineSearchEngine engine) {
            var index = settings.selected_search_engines.index (engine);
            if (index != -1) {
                settings.selected_search_engines.remove (engine);
                
                string [] new_selected_search_engines = {};
                foreach (var e in settings.get_strv ("medicine-search-engines")) {
                    if (e != engine.get_name ()) {
                        new_selected_search_engines += e.dup ();
                    }
                }
                
                settings.set_value ("medicine-search-engines", new_selected_search_engines);
            }
            
            foreach (MedicineSearchEngine e in medicine_search_engines) {
                if (e == engine) {
                    medicine_search_engines.remove (e);
                    medicine_search_engines_removed (e);
                    return true;
                }
            }
            
            return false;
        }
        
        public List<string> get_plugins_dirs () {
            List<string> plugin_dirs = new List<string> ();
            plugin_dirs.append (Path.build_filename (prefix, "lib", "clinica", "plugins"));
            
            // Load custom dirs if defined in the appropriate Environment variable. 
            string user_defined_dirs = Environment.get_variable ("CLINICA_PLUGINS_PATH");
            if (user_defined_dirs != null) {
                foreach (var path in user_defined_dirs.split(":")) {
                    plugin_dirs.append (path);
                }
            }
            
            return plugin_dirs;
        }
        
        /**
         * @brief Get the directory where the data of the user shall be stored. 
         */
        public string get_data_path () {
            return local_data_directory;
        }
        
        /**
         * @brief Get the directory in which the FileStore can store its files.
         */
        public string get_filestore_dir () {
            return Path.build_filename (local_data_directory, "filestore");
        }
        
        
    }
   

}
