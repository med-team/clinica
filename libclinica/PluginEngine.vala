/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

namespace Clinica {

    public class PluginEngine : Peas.Engine {
    
		private ResourceManager resource_manager;
        
        public PluginEngine (ResourceManager resources) {
            GLib.Object ();
            resource_manager = resources;
            
            /* If plugins are globally disabled do not use them */
            if (!resource_manager.settings.get_boolean ("use-plugins"))
                return;
         
            /* Add a search path useful to us */
            foreach (var path in resource_manager.get_plugins_dirs ()) {
                debug ("Adding %s to the search path for plugins", path);
                add_search_path (path, null);
            }
            
            /* Connect callback to make loading and unloading persistent */
            load_plugin.connect (on_load_plugin);
            unload_plugin.connect (on_unload_plugin);
        }
        
        private void on_load_plugin (Peas.PluginInfo info) {
            resource_manager.settings.activate_plugin (info.get_module_name ());
        }
        
        private void on_unload_plugin (Peas.PluginInfo info) {
            resource_manager.settings.deactivate_plugin (info.get_module_name ());
        }
    }

}
