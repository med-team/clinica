/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    public class PatientEditor : Dialog  {
    
    	public enum Response {
    	    SAVE = 0,
    	    CANCEL = 1,
    	}
    	
    	public enum DoctorCompletionAction {
    		CREATE_NEW = 0,
    	}
    
        public signal void error (string message);
        
        /* Emitted when the dialog has finished checking 
         * for the correctness of the data in the fields */
        public signal void check_finished ();
        
        private Builder builder;
        
        /* Gtk entries from where data is taken when creating
         * the new patient */
        public Entry given_name_entry;
        public Entry surname_entry;
        public Entry phone_entry;
        public Entry residence_address_entry;
        public Entry identification_code_entry;
        public Entry day_entry;
        public Entry month_entry;
        public Entry year_entry;
        public Entry doctor_entry;
        public ComboBox gender_combobox;
		
        /* Selected doctor is set from matched signal
         * of the autocompletion. */
        private Doctor? selected_doctor = null;
        
        /* Set to a patient that is the existing object for the
         * one represented here. */
        private Patient? existing_patient { get; set; default = null; }
		
		/* Completion for doctors */
		private EntryCompletion completion;

#if HAVE_PLUGINS
		/**
		 * @brief Extensions loaded for this PatientEditor
		 */
		private Peas.ExtensionSet extensions;
		
		/* This function is implemented in PatientEditorExtensionSet.c because
		 * I can't see an obvious way to do it in Vala... since the vapi seem
		 * not working it out and the gobject introspections seems to be
		 * failing on this */
		extern Peas.ExtensionSet setup_extension_set (Peas.Engine engine);
#endif
		
		/**
		 * @brief Color used to notify the user about fields in which
		 * the content is inconsistent. 
		 */
		public Gdk.Color alert_color;
		
		private ResourceManager resource_manager;
		
		internal Patient created_patient;
    	
        public PatientEditor (ResourceManager resources) {
            resource_manager = resources;
        	error.connect ((t,l) => resource_manager.error_callback (t,l));
            
            builder = new Builder.with_filename (resource_manager, "patient_editor.glade");
            builder.load_into_dialog (this);
            
            /* Add buttons to the Dialog */
            add_buttons (Stock.CANCEL, Response.CANCEL, Stock.SAVE, Response.SAVE);

            /* Prepare for drag and drop if the patient is changed */
            notify["existing_patient"].connect (update_draggable_patient_status);
            
            /* Load entries into local object */
            doctor_entry = builder.get_object ("doctor_entry") as Entry;
            given_name_entry = builder.get_object ("given_name_entry") as Entry;
            surname_entry = builder.get_object ("surname_entry") as Entry;
            phone_entry = builder.get_object ("phone_entry") as Entry;
            residence_address_entry = builder.get_object ("residence_address_entry") as Entry;
            identification_code_entry = builder.get_object ("codice_fiscale_entry") as Entry;
            day_entry = builder.get_object ("day_entry") as Entry;
            month_entry = builder.get_object ("month_entry") as Entry;
            year_entry = builder.get_object ("year_entry") as Entry;
            gender_combobox = builder.get_object ("gender_combobox") as ComboBox;

            /* Make save_button insensitive until user does not select a valid doctor. */
            set_response_sensitive (Response.SAVE, false);
            
            /* Create Doctor Entry with autocompletion, and setup a callback
             * to store the Doctor selected with autocompletion when this 
             * happens. */
            completion = new EntryCompletion ();
            completion.set_model (resource_manager.doctor_list_store);
            completion.set_text_column (3);
            completion.set_match_func (completion_match_function);
            completion.insert_action_text (DoctorCompletionAction.CREATE_NEW, "Create a new doctor");
            
            /* Callback that stores the selected doctor */
            completion.match_selected.connect (on_match_selected);
            
            /* Callback for action selected */
            completion.action_activated.connect (on_action_activated);
            
            /* Activate completion */
            doctor_entry.set_completion (completion);
            doctor_entry.changed.connect (on_doctor_entry_changed);
            
            /* Create alert_color */
            Gdk.Color.parse ("#ffcccc", out alert_color);
            
            /* Connect date change */
            day_entry.changed.connect (on_date_entry_changed);
            month_entry.changed.connect (on_date_entry_changed);
            year_entry.changed.connect (on_date_entry_changed);
            
            set_title ("Create a new patient");
            
            builder.connect_signals (this);
#if HAVE_PLUGINS
	        extensions = setup_extension_set (resource_manager.plugin_engine);
#endif
        }
        
        private void update_draggable_patient_status () {  
            /* Enable drag and drop on the patient icon */
            if (existing_patient == null)
                return;
            EventBox patient_eventbox = builder.get_object ("patient_eventbox") as EventBox;
            TargetEntry patient_target = { "PATIENT", 0, 1 };
            Gtk.drag_source_set (patient_eventbox, Gdk.ModifierType.BUTTON1_MASK, { patient_target }, Gdk.DragAction.COPY);  
            patient_eventbox.drag_begin.connect ((widget, context) => { 
                resource_manager.dragging_patient = existing_patient;
                Gtk.Image image = new Gtk.Image.from_pixbuf (
                    Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/patient.svg", 64, 64));
                Gtk.drag_set_icon_pixbuf (context, image.get_pixbuf (), 0, 0);
            });
        }
        
        private void on_doctor_entry_changed (Editable editable) {
        	Entry entry = (editable as Entry);
	        string doc_name = entry.get_text ();
	    	
	    	/* Replace action */
	    	completion.delete_action (DoctorCompletionAction.CREATE_NEW);
	    	if (doc_name != "")
	        	completion.insert_action_text (DoctorCompletionAction.CREATE_NEW,
	        		@"Create a new doctor named $(Utils.capitalize (doc_name))");
	        else 
	        	completion.insert_action_text (DoctorCompletionAction.CREATE_NEW,
	        		@"Create a new doctor");

			/* If this is a valid doctor make "save" clikable, otherwise set it to inactive */
			check_input_data ();
        }        
        
        private void on_date_entry_changed (Editable editable) {
			check_input_data ();
		}

        /**
         * @brief Check if patient age and doctor have been input
         * correctly by the user and set the sensitivity of the
         * save_button according to this. 
         */
		public void check_input_data () {
		    bool data_is_ok = true;
			if (!is_doctor_valid ())
			    data_is_ok = false;
			if (!is_birth_date_valid ())
			    data_is_ok = false;
			    
			set_response_sensitive (Response.SAVE, data_is_ok);
				
			/* Emit check_finished so additional checks can
			 * be added here by external plugins. */
		    check_finished ();
        }
        
        /**
         * @brief Check if the doctor selected is vaild, i.e.
         * if a doctor is selected and if it exists in the database
         */
        public bool is_doctor_valid () {
			/* Check for the doctor validity */
			if (selected_doctor == null && doctor_entry.get_text () == "")
			{
    			Utils.set_alert_state (doctor_entry, false);
			    return true;
			}
			if (selected_doctor == null) {
			    Utils.set_alert_state (doctor_entry, true);
			    return false;
			}
			if (selected_doctor.get_complete_name () != doctor_entry.get_text ()) 
		    {
		        Utils.set_alert_state (doctor_entry, true);
				return false;
			}
			
			Utils.set_alert_state (doctor_entry, false);
			return true;
        }
        
        /**
         * @brief Check if the selected date is valid, i.e. if it
         * can be parsed and correctly stored in a DateTime object.
         *
         * This routine also checks if the DateTime is reasonable
         * (precisely it will return false if the patient has more
         * than 150 years). 
         */
        public bool is_birth_date_valid () {
            int year = int.parse (year_entry.get_text ());
            int month = int.parse (month_entry.get_text ());
            int day = int.parse (day_entry.get_text ());
    		
    		var now = new DateTime.now_local ();
 		    int current_year = now.get_year ();
    		
            if ((current_year - year > 150) || 
            	(month < 1) || (month > 12) ||
            	(day < 1)   || (day   > 31) ||
				!Utils.check_date (day, month, year))
			{
			    /* Some color to make the user notice that this field is 
			     * not correct */
			    Utils.set_alert_state (day_entry, true);
			    Utils.set_alert_state (month_entry, true);
			    Utils.set_alert_state (year_entry, true);
				return false;
			}
				
			/* Reset colors since the field is now correct */
		    Utils.set_alert_state (day_entry, false);
		    Utils.set_alert_state (month_entry, false);
		    Utils.set_alert_state (year_entry, false);
		    return true; 
        }
        
        /*
         * @brief NewPatientDialog with an existing patient, to edit it
         */
        public PatientEditor.with_patient (ResourceManager resources, Patient p) {
        	this (resources);
        	
        	// Load doctors and existing patient
        	selected_doctor = p.doctor;
        	existing_patient = p;
        	update_draggable_patient_status ();
        	
        	if (p.doctor != null)
            	doctor_entry.set_text (p.doctor.get_complete_name ());
        	
        	// Pre fill the fields in the interface
        	given_name_entry.set_text (p.given_name);
        	surname_entry.set_text (p.surname);
        	phone_entry.set_text (p.phone);
        	residence_address_entry.set_text (p.residence_address);
        	identification_code_entry.set_text (p.identification_code);
        	
        	// Parse gender into combobox
        	if (p.gender == Gender.MALE)
        		gender_combobox.set_active (0);
        	else
        		gender_combobox.set_active (1);
        		
        	// Parse date
        	var birth_date = p.birth_date;
        	day_entry.set_text (birth_date.get_day_of_month ().to_string ());
        	month_entry.set_text (birth_date.get_month ().to_string ());
        	year_entry.set_text (birth_date.get_year ().to_string ());
        	
        	// Set title to edit patient * instead of create new patient
        	set_title (_("Edit patient named %s").printf (p.get_complete_name ()));
        }
        
        /**
         * @brief Capitalize Patient's name
         */
        [CCode (instance_pos = -1)]
        public bool on_name_focus_out_event (Gtk.Widget source, Gdk.Event event) {
        	string name = given_name_entry.get_text ();    	
        	if (name != "")
        		given_name_entry.set_text ( Utils.capitalize (name) );
        	return false;
        }
        
        /**
         * @brief Capitalize Patient's surname
         */
        [CCode (instance_pos = -1)]
        public bool on_surname_focus_out_event (Gtk.Widget source, Gdk.Event event) {
        	string surname = surname_entry.get_text ();    	
        	if (surname != "")
        		surname_entry.set_text ( Utils.capitalize (surname) );
        	return false;
        }

        /**
         * @brief Open a NewPatientDialog precompiled with the name
         * of the patient.
         */
        public PatientEditor.with_name (ResourceManager resources, string name) {
        	this (resources);
        	string [] name_pieces = name.split (" ");
        	if (name_pieces.length > 0) {
        		given_name_entry.set_text (name_pieces[0]);
        	}
        	if (name_pieces.length > 1) {
        		surname_entry.set_text (string.joinv(" ", name_pieces[1:name_pieces.length]));
        	}
        }
        
        /**
         * @brief Create a new doctor on the fly with the name specifed in the 
         * doctor_entry
         */
        private void on_action_activated (EntryCompletion editable, int action) {
        	if (action != DoctorCompletionAction.CREATE_NEW)
            	return;
            	
            /* Create a new dialog precompiled with the user input until now */
            string doc_name = doctor_entry.get_text ();
            var dialog = new DoctorEditor.with_name (resource_manager, Utils.capitalize (doc_name));
            dialog.set_transient_for (this);
            if (dialog.run () == DoctorEditor.Response.CANCEL) {
            	dialog.destroy ();
            	return;
            }
            
            /* Set the newly created doctor as selected doctor */
            selected_doctor = dialog.created_doctor;
            doctor_entry.set_text (selected_doctor.get_complete_name ());
            
            /* Destroy the dialog */
            dialog.destroy ();
            
            /* Check if all the data is now correct */
            check_input_data ();
        }
        
        /**
         * @brief Callback function for the doctor selection that matches the doctor
         * if the key is a substring of its complete name. 
         */
        private bool completion_match_function (EntryCompletion compl, string key, TreeIter iter) {
        	Value value;
        	resource_manager.doctor_list_store.get_value(iter, 0, out value);
        	Doctor doc = value as Doctor;
        	if (doc == null)
        		return false;
        	return (key.up () in doc.get_complete_name ().up ());
        }
        
        private bool on_match_selected (EntryCompletion compl, TreeModel model, TreeIter iter) {
        	/* Get doctor associated with the selected completion */
        	Value value;
        	model.get_value (iter, 3, out value);
        	doctor_entry.set_text (value as string);
        	model.get_value (iter, 0, out value);
        	
        	/* Set selected doctor in this object to use it later */
        	selected_doctor = value as Doctor;
        	
        	/* Check if all the input data is now correct and make save_button
        	 * sensitive according to this */
        	check_input_data ();
        	return true;
        }
        
        public new Response run () {
            if (base.run () == 0) {
            /* Check if there exists a selected doctor and if that
         	 * doctor is still the selected one. */
            	if (selected_doctor != null && 
            	    doctor_entry.get_text() != selected_doctor.get_complete_name ()) {
            		
		       		error (_("You must select a doctor for this patient.") + 
			        	_("If you need to create a new one type his name in the entry and select Create new doctor from the completion list."));
		        	return Response.CANCEL;
		        }
            	
            	/*  Save the new patient, creating a new one if this is a new patient, or
            	 *  reusing the existing one if we are only modifying a patient. */
            	if (existing_patient == null) {
	            	created_patient = new Patient ();
	            	created_patient.doctor = selected_doctor;
	            }
	            else
					created_patient = existing_patient;
					
            	created_patient.given_name = given_name_entry.get_text ();
            	created_patient.surname = surname_entry.get_text ();
            	created_patient.phone = phone_entry.get_text ();
            	created_patient.identification_code = identification_code_entry.get_text ();
            	created_patient.residence_address = residence_address_entry.get_text ();
            	created_patient.doctor = selected_doctor;
            	
            	/* Create new DateTime */
            	int year = int.parse (year_entry.get_text ());
            	int month = int.parse (month_entry.get_text ());
            	int day = int.parse (day_entry.get_text ());
    			DateTime t;
    					
            	if ((1 <= year) &&  (year <= 9999) && 
            		(1 <= month) && (month <= 12)  &&
            		(1 <= day) && (day <= 31))
            	{
	            	t = new DateTime (new TimeZone.utc (), year, month, day, 0, 0, 0);
	            	created_patient.birth_date = t;
	            }
	            else
	            {
                        set_response_sensitive (Response.SAVE, false);
	            		error (_("Date inserted is invalid, aborting patient editing"));
	            		return Response.CANCEL;
	            }
	            
            	/* Set gender here */
            	if (gender_combobox.get_active () == 0)
	            	created_patient.gender = Gender.MALE;
	            else
	            	created_patient.gender = Gender.FEMALE;
	            
            	resource_manager.data_provider.save_patient (created_patient);
            	
                return Response.SAVE;
            }
            
            return Response.CANCEL;
        }
    }
}

