/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

using Sqlite;

namespace Clinica {
   
    public class SqlitePatientIterator : Object, PatientIterator {
        
		private SqliteDataProvider provider;
		private Statement stmt;

        public SqlitePatientIterator (SqliteDataProvider provider, string clause = "") {
            this.provider = provider;
            provider.db.prepare ("SELECT id from %s %s;".printf (provider.patient_table, clause), 
                                 -1, out stmt);
        }
        
        public bool next () {
            return (stmt.step () == ROW);
        }
        
        public new Patient get () {
            int id = stmt.column_int (0);
            return provider.get_patient (id); 
        }
    }
    
}
