/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Jansson;
 
namespace Clinica {

    public class BackupEngine : Object {
    
        private ResourceManager resource_manager;
        
        private WaitDialog wait_dialog;
    
        public BackupEngine (ResourceManager resources) {
            resource_manager = resources;
        }
        
        /**
         * @brief Backup the data contained in the given DataProvider
         * to the file specified in destination. 
         */
        public void backup (DataProvider provider, string destination) {
        
            var file_store = provider.get_file_store ();
            if (file_store != null) {
                // That means that we have to backup the files, too.        
                DirUtils.create (Path.build_filename (destination, "file_store"), -1);

                foreach (var visit in provider.visits ()) {
                    GLib.List<FileObject> visit_files = file_store.get_files (visit.id);
                    if (visit_files.length () > 0) {
                        string base_dir = Path.build_filename (destination, "file_store",
                            visit.id.to_string ());
                        DirUtils.create (base_dir, -1);
                        foreach (var file in visit_files) {
                            File source_file = file.get_file ();
                            try {
                                source_file.copy (
                                    File.new_for_path (Path.build_filename (base_dir, file.get_file_name ())),
                                    FileCopyFlags.NONE);
                            } catch (GLib.Error e) {
                                warning ("Error while copying %s", Path.build_filename (base_dir, file.get_file_name ()));
                            }
                        }
                    }
                }
            }

            var root = Json.object ();
            
            // Store the date of the Json backup. 
            root.object_set ("date", 
                Json.string (SqliteDataProvider.datetime_to_string (new DateTime.now_utc ())));
            
            // Create a Json Array containing all the doctors
            var doctors = Json.array ();
            foreach (var doc in provider.doctors ()) {
                doctors.array_append (doc.to_json ());
            }
            root.object_set ("doctors", doctors);
            
            // Create a Json array containing all the patients
            var patients = Json.array ();
            foreach (var patient in provider.patients ()) {
                patients.array_append (patient.to_json ());
            }
            root.object_set ("patients", patients);
            
            // Create a Json array containing all the visits
            var visits = Json.array ();
            foreach (var visit in provider.visits ()) {
                visits.array_append (visit.to_json ());
            }
            root.object_set ("visits", visits);
            
            // Create a Json array containing all the visits
            var events = Json.array ();
            foreach (var event in provider.events ()) {
                events.array_append (event.to_json ());
            }
            root.object_set ("events", events);
            
            // Finally dump the resulting file to destination
            root.dump_file (Path.build_filename (destination, "backup.json"));
            
            // Backup medicines.db, if present
            string medicines_path = Path.build_filename (resource_manager.get_data_path (), 
                "medicines.db");
            File medicines_db = File.new_for_path (medicines_path);
            if (medicines_db.query_exists ()) {
                debug ("Copying the database of local medicines");
                try {
                    medicines_db.copy (File.new_for_path (Path.build_filename (destination, 
                        "medicines.db")), FileCopyFlags.NONE);
                } catch (GLib.Error e) {
                    warning ("Error while copying the medicines.db file");
                }
            }
        }
        
        /**
         * @brief Import the data from an older backup. 
         * @param filename The filename of the backup that shall be imported. 
         */
        public async void import (string directory) {
            if (directory == null) {
                var dialog = new Gtk.MessageDialog (resource_manager.user_interface.window, 
                    Gtk.DialogFlags.MODAL, 
                    Gtk.MessageType.ERROR,
                    Gtk.ButtonsType.OK,
                    "%s", _("Please select a valid backup file"));
                dialog.run ();
                dialog.destroy ();
                return;
            }
            
            string details = "Details of the operation:\n\n";
            
            var filename = Path.build_filename (directory, "backup.json");
        
            debug ("Importing the data contained in %s", filename);
		        
            wait_dialog = new WaitDialog (resource_manager, _("Importing data"));
	        wait_dialog.set_message (_("Please wait while Clinica imports the data..."));
	        wait_dialog.show_all ();
	   
	        wait_dialog.set_message (_("Importing the JSON data.."));
            
            // Grab the provider from the resource_manager
            var provider = resource_manager.data_provider;
            
            Jansson.Error error;
            var data = Json.load_file (filename, 0, out error);            
            if (data == null || !data.is_object ()) {
                warning (_("Error while parsing the backup JSON data"));
                
                var dialog = new Gtk.MessageDialog (resource_manager.user_interface.window, 
                    Gtk.DialogFlags.MODAL, 
                    Gtk.MessageType.ERROR,
                    Gtk.ButtonsType.OK,
                    "%s", _("An error occurred while parsing the JSON file"));
                dialog.run ();
                dialog.destroy ();
                wait_dialog.destroy ();
                return;
            }
            
            // Load the data from the JSON input
            Json doctors = data.object_get ("doctors");
            Json patients = data.object_get ("patients");
            Json visits = data.object_get ("visits");
            Json events = data.object_get ("events");
            
            int64 total_steps = doctors.array_size () + patients.array_size () + 
                visits.array_size () + events.array_size ();
                
            int64 steps_done = 0;
            
            // Dome some basic checks about data consistency before dropping
            // all the entries in the database. 
            if (!doctors.is_array () || !patients.is_array () || 
                !visits.is_array () || !events.is_array ()) {
                warning (_("The doctors, patients, visits and events field in the JSON backup should be arrays, aborting."));
                var dialog = new Gtk.MessageDialog (resource_manager.user_interface.window, 
                    Gtk.DialogFlags.MODAL, 
                    Gtk.MessageType.ERROR,
                    Gtk.ButtonsType.OK,
                    "%s", _("An error occurred while parsing the JSON file"));
                dialog.run ();
                dialog.destroy ();
                wait_dialog.destroy ();
                return;
            }

            var doctor_id_mapping = new HashTable<int64?, int64?> 
                (GLib.int64_hash, GLib.int64_equal);
            var patient_id_mapping = new HashTable<int64?, int64?>
                (GLib.int64_hash, GLib.int64_equal); 
            var visit_id_mapping = new HashTable<int64?, int64?>
                (GLib.int64_hash, GLib.int64_equal);
            var event_id_mapping = new HashTable<int64?, int64?>
                (GLib.int64_hash, GLib.int64_equal);
            
            // Clear the database and put the data back in it
            // in such a way that won't break relationships. 
            wait_dialog.set_message (_("Clearing the old data in the database..."));
            clear_data();
            
            int i = 0;
            wait_dialog.set_message (_("Loading doctors..."));
            for (i = 0; i < doctors.array_size (); i++) {
                var doctor = new Doctor.from_json (doctors.array_get (i));
                int64 previous_id = doctor.id;
                doctor.id = 0;
                doctor_id_mapping[previous_id] = provider.save_doctor (doctor);
                wait_dialog.set_progress ((1.0 * steps_done++) / total_steps);
            }
            
            details += _("- %d doctors have been imported\n").printf (doctors.array_size ());
            
            wait_dialog.set_message (_("Loading patients..."));
            for (i = 0; i < patients.array_size (); i++) {
                var patient_json = patients.array_get (i);
                var patient = new Patient.from_json (patient_json);
                int64 previous_id = patient.id;
                patient.id = 0;
                int64 doctor_id = patient_json.object_get ("doctor").integer_value ();
                if (doctor_id != 0)
                    patient.doctor = provider.get_doctor (doctor_id_mapping[doctor_id]);
                patient_id_mapping[previous_id] = provider.save_patient (patient);
                wait_dialog.set_progress ((1.0 * steps_done++) / total_steps);
            }
            
            details += _("- %d patients have been imported\n").printf (patients.array_size ());
            
            wait_dialog.set_message (_("Loading visits..."));
            for (i = 0; i < visits.array_size (); i++) {
                var visit_json = visits.array_get (i);
                var visit = new Visit.from_json (visit_json);
                int64 previous_id = visit.id;
                visit.id = 0;
                int64 patient_id = visit_json.object_get ("patient").integer_value ();
                if (patient_id != 0)
                    visit.patient = provider.get_patient (patient_id_mapping[patient_id]);
                visit_id_mapping[previous_id] = provider.save_visit (visit);
                wait_dialog.set_progress ((1.0 * steps_done++) / total_steps);
            }
            
            details += _("- %d visits have been imported\n").printf (visits.array_size ());
            
            wait_dialog.set_message (_("Loading events..."));
            for (i = 0; i < events.array_size (); i++) {
                var event_json = events.array_get (i);
                var event = new Event.from_json (event_json);
                int64 previous_id = event.id;
                event.id = 0;
                int64 visit_id = event_json.object_get ("visit").integer_value ();
                int64 patient_id = event_json.object_get ("patient").integer_value ();
                if (visit_id != 0)
                    event.visit = provider.get_visit (visit_id_mapping[visit_id]);
                if (patient_id != 0)
                    event.patient = provider.get_patient(patient_id_mapping[patient_id]);
                event_id_mapping[previous_id] = provider.save_event (event);
                wait_dialog.set_progress ((1.0 * steps_done++) / total_steps);
            }
            
            details += _("- %d events have been imported\n").printf (events.array_size ());
            
            // Now import the data in the filestore, if present
            var store_path = Path.build_filename (directory, "file_store");
            int counter = 0;
            if (FileUtils.test (store_path, FileTest.EXISTS | FileTest.IS_DIR)) {
                debug ("Import the file store");
                var store = resource_manager.data_provider.get_file_store ();
                if (store == null) {
                    details += _("- A FileStore was found but is not available in the current provider\n");
                }
                else {
                    try {
                        var store_dir = Dir.open (store_path);
                        string visit_dir;
                        while ((visit_dir = store_dir.read_name ()) != null) {
                            int64 visit_id = int64.parse (visit_dir);
                            int64 real_id = visit_id_mapping[visit_id];
                            
                            try {
                                var v_dir = Dir.open (Path.build_filename (store_path, visit_dir));
                                string v_filename;
                                while ((v_filename = v_dir.read_name ()) != null) {
                                    store.store_file (real_id, Path.build_filename (store_path, visit_dir, v_filename));
                                    counter++;
                                }
                            } catch (GLib.Error e) {
                                warning ("Error while opening %s", Path.build_filename (store_path, visit_dir));
                            }
                        }
                    } catch (GLib.Error e) {
                        warning ("Error while opening %s", store_path);
                    }
                    
                    details += _("- %d files have been imported\n").printf (counter);
                }
            }
            else {
                details += _("- No FileStore was found in the backup\n");
            }
            
            // Try to import the Medicines database, if found
            var medicines_db_path = Path.build_filename (directory, "medicines.db");
            var medicines_db = File.new_for_path (medicines_db_path);
            if (medicines_db.query_exists ()) {
                try {
                    var destination = File.new_for_path (Path.build_filename (
                        resource_manager.get_data_path (), "medicines.db"));
                    if (destination.query_exists ()) {
                        destination.delete ();
                    }
                    medicines_db.copy (destination, FileCopyFlags.NONE);
                    
                    resource_manager.unregister_medicine_search_engine (resource_manager.local_medicines_search_engine);
                    resource_manager.local_medicines_search_engine = new LocalMedicinesDatabase (resource_manager);
                    resource_manager.register_medicine_search_engine (resource_manager.local_medicines_search_engine);
                    
                } catch (GLib.Error e) {
                    warning ("Error copying the medicines database: %s", e.message);
                    details += _("- Error while copying the medicines database\n");
                }
            }            
            
            debug ("Importing finished successfully");
            
            wait_dialog.destroy ();
            
            var dialog = new Gtk.MessageDialog (resource_manager.user_interface.window, Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.INFO,
                Gtk.ButtonsType.OK,
                "%s", _("The import of the data has been completed successfully"));
            dialog.set_title ("Import finished");
            dialog.format_secondary_text (details);
            dialog.run ();
            dialog.destroy ();
        }
        
        /**
         * @brief Destroy all the data in the current data provider. This is needed
         * before inflating the data from the backup into the database, so conflicts
         * won't be possible. 
         *
         * A more advanced merge strategy could be added in the future. 
         */
        private void clear_data () {
            debug ("Clearing all the data in the DataProvider");
            DataProvider provider = resource_manager.data_provider;
               
            // Remove data in the FileStore, if present
            var store = provider.get_file_store ();
            if (store != null) {
                foreach (var visit in provider.visits ()) {
                    foreach (var file in store.get_files (visit.id)) {
                        store.remove_file (visit.id, file.get_file_name ());
                    }
                }
            }
            
            // Remove Events
            List<Event> events = new List<Event> ();
            foreach (var event in provider.events ())
                events.append (event);
            foreach (var event in events)
                provider.remove_event (event);
                
            // Remove Visits
            List<Visit> visits = new List<Visit> ();
            foreach (var visit in provider.visits ())
                visits.append (visit);
            foreach (var visit in visits)
                provider.remove_visit (visit);
                
            // Remove Patients
            List<Patient> patients = new List<Patient> ();
            foreach (var patient in provider.patients ())
                patients.append (patient);
            foreach (var patient in patients)
                provider.remove_patient (patient);
                
            // Remove Doctors
            List<Doctor> doctors = new List<Doctor> ();
            foreach (var doctor in provider.doctors ())
                doctors.append (doctor);
            foreach (var doctor in doctors)
                provider.remove_doctor (doctor);
        }
    
    }

}
