/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

	public class DoctorEditor : Dialog {
	
		public enum Response {
			SAVE = 0,
			CANCEL = 1,
		}
	
		public signal void error (string message);
	
		/* Gtk stuff */
		private Builder     builder;
	
		/* Entries */
		private Entry given_name_entry;
		private Entry surname_entry;
		private Entry phone_entry;
		private Entry mobile_entry;
		
		/* For external tools */
		public  Doctor created_doctor;
		private Doctor existing_doctor = null;
		
		private ResourceManager resource_manager;
	
		public DoctorEditor (ResourceManager resources) {
		    resource_manager = resources;
			error.connect ((me, message) => resource_manager.error_callback (me, message));
		
			builder = new Clinica.Builder.with_filename (resources, "doctor_editor.glade");
			builder.load_into_dialog (this);
			
			/* Pack buttons and content */
			add_buttons (Stock.CANCEL, Response.CANCEL, Stock.SAVE, Response.SAVE);
		
			/* Load entries from the builder */
			given_name_entry = builder.get_object ("given_name_entry") as Entry;
			surname_entry    = builder.get_object ("surname_entry") as Entry;
			phone_entry      = builder.get_object ("phone_entry") as Entry;
			mobile_entry     = builder.get_object ("mobile_entry") as Entry;
			
			builder.connect_signals (this);
		}
		
		public DoctorEditor.with_name (ResourceManager resources, string complete_name) {
			this (resources);
			string [] pieces = complete_name.split (" ");
			if (pieces.length > 0)
				given_name_entry.set_text (pieces[0]);
			if (pieces.length > 1)
				surname_entry.set_text (string.joinv(" ", pieces[1:pieces.length]));
		}
		
		public DoctorEditor.with_doctor (ResourceManager resources, Doctor doctor) {
			this (resources);
			existing_doctor = doctor;
			
			/* Fill in the details of the doctor in the interface */
			given_name_entry.set_text (doctor.given_name);
			surname_entry.set_text (doctor.surname);
			phone_entry.set_text (doctor.phone);
			mobile_entry.set_text (doctor.mobile);
			
			/* Title of edit doctor dialog */
			set_title (_("Edit doctor named %s").printf (doctor.get_complete_name ()));
		}
		
		/**
         * @brief Capitalize Doctor's name and surname
         */
        [CCode (instance_pos = -1)]
        public bool on_name_focus_out_event (Gtk.Widget source, Gdk.Event event) {
        	string name = given_name_entry.get_text ();    	
        	if (name != "")
        		given_name_entry.set_text ( Utils.capitalize (name) );
        	return false;
        }
        
        [CCode (instance_pos = -1)]
        public bool on_surname_focus_out_event (Gtk.Widget source, Gdk.Event event) {
        	string surname = surname_entry.get_text ();    	
        	if (surname != "")
        		surname_entry.set_text ( Utils.capitalize (surname) );
        	return false;
        }
	
		public new Response run () {
			if (base.run () == Response.SAVE) {
				Doctor doc;
				if (existing_doctor == null)		
					doc = new Doctor ();
				else
					doc = existing_doctor;
				
				/* Fill doctor details */
				doc.given_name = given_name_entry.get_text ();
				doc.surname    = surname_entry.get_text ();
				doc.phone      = phone_entry.get_text ();
				doc.mobile     = mobile_entry.get_text ();
				int64 new_id = resource_manager.data_provider.save_doctor (doc);
				
				created_doctor = resource_manager.data_provider.get_doctor (new_id);
				
				return Response.SAVE;
			}
			
			return Response.CANCEL;
		}
	
	}
}
