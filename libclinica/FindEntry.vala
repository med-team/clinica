/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    public class FindEntry : Entry {
        
        public FindEntry () {
            /* Connect callback signals */
            changed.connect (on_find_entry_changed);
            set_icon_from_stock (Gtk.EntryIconPosition.PRIMARY,   "gtk-find");
            set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, null);
            
            icon_release.connect (on_icon_release);
    	    key_press_event.connect (on_escape_key);
        }

        private void on_find_entry_changed (Editable e) {
            if ((e as Entry).get_text () == "")
            	clear_find_entry ();
            else
            	set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, "gtk-clear");
        }
        
        /**
         * @brief Clear the find_entry. 
         */
    	private void clear_find_entry() {
            set_text("");
	    	set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, null);
        }
        
        /**
         * @brief Clear search icon in the find_entry. 
         */
    	private void on_icon_release(Gtk.EntryIconPosition pos, Gdk.Event event) {
            if (Gtk.EntryIconPosition.SECONDARY == pos)
	       	clear_find_entry();
        }
        
        /**
         * @brief Possibility to clear the
         * find_entry by typing 'Esc'. 
         */
        private bool on_escape_key(Gdk.EventKey e) { 
            if(Gdk.keyval_name(e.keyval) == "Escape")
				clear_find_entry();
            // Continue processing this event, since the 
            // text entry functionality needs to see it too. 
            return false; 
        }
        
    }
}
