/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Gtk;
 
namespace Clinica {

    public class ImportDialog : Dialog {
    
        private ResourceManager resource_manager;
        
        private FileChooserButton file_chooser_button;
    
        public ImportDialog (ResourceManager resources) {
            this.resource_manager = resources;
            
            add_buttons (Gtk.Stock.CANCEL, ResponseType.CANCEL, Gtk.Stock.OK, ResponseType.ACCEPT);
            
            var builder = new Clinica.Builder.with_filename (
                resource_manager,
                "import_dialog.glade");
            builder.load_into_dialog (this);
            
            set_title (_("Import data from backup file"));
            
            // Load the needed objects from the Builder. 
            file_chooser_button = builder.get_object ("filechooserbutton") as FileChooserButton;
        }
        
        public string get_filename () {
            return file_chooser_button.get_filename ();
        }
    
    }

}
