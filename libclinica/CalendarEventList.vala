/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 
 namespace Clinica {
 
    public class CalendarEventList : Alignment {
    
        private ResourceManager resource_manager { get; private set; }
        
        /**
         * @brief The day that is displayed in the eventlist.
         */
        private DateTime day;
    
        public CalendarEventList (ResourceManager resources) {
            resource_manager = resources;
            
            /* Set the right padding, according to what is set in
             * the resource_manager */
            set_padding (resource_manager.PADDING, resource_manager.PADDING, 
                resource_manager.PADDING, resource_manager.PADDING);
                
            /* Set the day to today */
            set_day (new DateTime.now_local());
            
            set_size_request (280, -1);
            
            /* Connect event added and or removed with the reload of the day */
            resource_manager.data_provider.event_added.connect ((event) => set_day (this.day));
            resource_manager.data_provider.event_changed.connect ((event) => set_day (this.day));
            resource_manager.data_provider.event_removed.connect ((event) => set_day (this.day));
            resource_manager.data_provider.visit_added.connect ((visit) => set_day (this.day));
            resource_manager.data_provider.visit_changed.connect ((visit) => set_day (this.day));
            resource_manager.data_provider.visit_removed.connect ((visit) => set_day (this.day));
        }
        
        public void set_day (DateTime date) {
            /* Save the day :) */
            day = date;
        
            /* Create the VBox that will hold the data */
            var box = new Box (Gtk.Orientation.VERTICAL,
                               resource_manager.PADDING);
            
            /* Place the title in there */
            string title = "<small>%s</small>".printf (Utils.capitalize (day.format("%A, %d %B %Y")));
            var title_label = new Label ("");
            title_label.set_alignment (1.0f, 0.5f);
            title_label.set_markup (title);
            title_label.set_margin_bottom (25);
            box.pack_start (title_label, false, true);
            
            /* Start of events */
            string events_title = "<big><b>" + _("Events scheduled") + "</b></big>";
            var events_title_label = new Label ("");
            events_title_label.set_markup (events_title);
            events_title_label.set_alignment (0.0f, 0.5f);
            box.pack_start (events_title_label, false, true);
            
            /* Show all the events of the day */
            bool no_events = true;
            foreach (Event event in resource_manager.data_provider.events (date, date.add_days (1))) {
                no_events = false;
                var event_box = new EventDetail (resource_manager, event);
                box.pack_start (event_box, false, true);
            }
            
            /* If no events are present show a helpful message */
            if (no_events) {
                Label no_event_label = new Label ("");
                no_event_label.set_alignment (0.5f, 0.5f);
                no_event_label.set_markup ("<i>" + 
                    _("No events on this day.\nYou can create a new event\nby clicking on the top-left button.") +
                    "</i>");
                no_event_label.set_justify (Justification.CENTER);
                box.pack_start (no_event_label, false, true);
            }
            
            /* Select the visit title in a different mode if we are in the future or in the past. */
            string visit_title;
            DateTime today = new DateTime.now_local ();
            
            if (day.compare (today) <= 0)
                visit_title = "<big><b>" + _("Visits performed") + "</b></big>";
            else
                visit_title = "<big><b>" + _("Visits scheduled") + "</b></big>";
                
            var visit_title_label = new Label ("");
            visit_title_label.set_markup (visit_title);
            visit_title_label.set_margin_top (15);
            visit_title_label.set_alignment (0.0f, 0.5f);
            box.pack_start (visit_title_label, false, true);
            
            /* And now the visits of the day */
            bool no_visits = true;
            foreach (Visit visit in resource_manager.data_provider.visits (null, date, date.add_days(1))) {
                no_visits = false;
                var visit_detail = new VisitDetail (resource_manager, visit);
                box.pack_start (visit_detail, false, true);
            }
            
            /* If no visits were performed show a helpful message */
            if (no_visits) 
            {
                Label no_visit_label = new Label ("");
                no_visit_label.set_markup ("<i>" +
                    _("No visits performed on this day") +
                    "</i>");
                box.pack_start (no_visit_label, false, true);
            }
            
            /* Add the box in the widget, and if there is something in
             * there keep it away */
            Widget child = get_child();
            if (child != null)
               remove (child);
            add (box);
            box.show_all ();
        }
    }
 }
