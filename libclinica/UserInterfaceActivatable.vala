/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

namespace Clinica {

    /**
     * @brief This is the interface that must be implemented by
     * a plugin to be loaded in the UserInterface instances
     */
    public interface UserInterfaceActivatable : GLib.Object {
        public abstract UserInterface user_interface { get; set; }
        public abstract ResourceManager resource_manager { get; set; }
        public abstract void activate ();
        public abstract void deactivate ();
        public abstract void update_state ();
    }

}
