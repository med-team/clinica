/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Tommaso Bianucci <churli@gmail.com>
 */
 
using Jansson;
 
 namespace Clinica {
 
    /**
     * @brief An instance of this class provides a service on the local
     * network that allow other instances of Clinica to attach to the
     * given DataProvider. 
     */
    public class DataServer : GLib.Object {
    
        /**
         * @brief The ResouceManager of this Clinica instance.
         */
        private ResourceManager resource_manager;
        
        /**
         * @brief The DataProvider from which the data will
         * be taken. 
         */
        protected DataProvider provider;
        
        /**
         * @brief Authentication domain used to authenticate the clients
         * that make RPC requests. 
         */
        internal AuthDomain auth_domain;
        
        /**
         * @brief The port on which this server will be available.
         */
        protected int PORT;
        
        /**
         * @brief The Soup.Server used to instantiate the service.
         */
        protected Soup.Server server;
        
        /**
         * @brief The name of this instance of the DataServer that will be
         * sent in the response to the default handler.
         */
        private string name = "Clinica DataServer";
        
        /**
         * @brief The version of clinica that is running this DataServer.
         */
        private string version = Config.VERSION;
        
        /** 
         * @brief The current processing id. 
         */
        private string processing_id = "null";
    
        public DataServer (ResourceManager resources, DataProvider provider, int PORT = 20843) {
            this.resource_manager = resources;
            this.provider = provider;
            this.PORT = PORT;
            
            // Create the Soup.Server object and start the server
            server = new Soup.Server (Soup.SERVER_PORT, PORT);
            
            // Create the AuthDomain for the RPC part. Support disabling authentication
            // via the CLINICA_DISABLE_AUTHENTICATION environment variable.
            var disable_authentication_env = Environment.get_variable ("CLINICA_DISABLE_AUTHENTICATION");
            auth_domain = new AuthDomain (resource_manager);
            if (disable_authentication_env == null || disable_authentication_env != "yes")
                server.add_auth_domain (auth_domain);
            
            // Connect the handlers
            server.add_handler ("/", default_handler);
            server.add_handler ("/rpc", rpc_handler);
        }
        
        /**
         * @brief This handler is called when no other handlers match and will
         * return an error for every request except "/", that will give
         * some basic details on the Clinica server. 
         */
        protected void default_handler (Soup.Server server, Soup.Message msg, string path, 
                                        HashTable? query, Soup.ClientContext client) {
            if (path != "/") {
                msg.set_status (500);
                msg.set_response ("text/html", Soup.MemoryUse.COPY, 
                    "Clinica network server cannot handle your request\n".data);
            }
            else {
                msg.set_status (200);
                msg.set_response ("text/html", Soup.MemoryUse.COPY, 
                    """{ "name": %s, "version": %s }""".printf (name, version).data);
                    
            }
        }
        
        /**
         * @brief Handle an RPC request.
         */
        protected void rpc_handler (Soup.Server server, Soup.Message msg, string path,
                                    HashTable? query, Soup.ClientContext client) {
            
            // Parsing using the Jansson library
            Jansson.Error error;
            Json root = Json.loads ((string) msg.request_body.data, 0, out error);
            
            if (root == null) {
                warning ("The document passed is not a JSON document");
                msg.set_status (500);
                msg.set_response ("text/html", Soup.MemoryUse.COPY, 
                    "Request failed, please pass a JSON document in the POST request".data);
                    
                // TODO: Add the reporting of the precise error that happened. 
                    
                return;
            }
            
            /* Read the type of request that the client has made */
            Json request_element = root.object_get ("method");
            
            /* Check that the given request is a string and is present */
            if (request_element == null || !request_element.is_string ()) {
                warning ("No method field in the JSON document, aborting");
                msg.set_status (500);
                msg.set_response ("text/html", Soup.MemoryUse.COPY, 
                    "Request failed, please pass a JSON document containing the method field in the POST request".data);
                return;
            }
            
            Json? req_id_element = root.object_get ("id");
            if (req_id_element == null) {
                warning ("No id in the request, aborting");
                msg.set_status (500);
                msg.set_response ("text/html", Soup.MemoryUse.COPY, 
                    "Request failed, please pass a JSON document containing the id field in the POST request".data);
                return;
            }
            else {
                if (req_id_element.is_integer ())
                    processing_id = req_id_element.integer_value ().to_string ();
                else if (req_id_element.is_string ())
                    processing_id = """ "%s" """.printf (req_id_element.string_value ());
                else if (req_id_element.is_null ())
                    processing_id = "null";
                else if (req_id_element.is_object ()) {
                    processing_id = req_id_element.dumps ();
                }
            }
            
            string request = request_element.string_value ();
            
            // Get the ID now, if it exists
            Json? params_element = root.object_get ("params");
            Json? id_element = null;
            if (params_element != null)
                id_element = params_element.object_get ("id");
            else
                params_element = Json.object ();
            
            /* Many requests need an associated id, so get it now */
            int64 id = 0;
            if (id_element != null && id_element.is_integer ()) {
                id = id_element.integer_value ();
            }
            
            /* If we got till here than we have good change of giving a good 
             * response to the client. */
            msg.set_status (200);
            
            switch (request) {
                case "get_patient":
                    var patient = provider.get_patient (id);
                    msg.set_response ("text/html", Soup.MemoryUse.COPY, handle_rpc_get_patient (patient).data);
                    break;
                case "patients":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_patients (params_element).data);
                    break;
                case "get_doctor":
                    var doctor = provider.get_doctor (id);
                    msg.set_response ("text/html", Soup.MemoryUse.COPY, 
                        handle_rpc_get_doctor (doctor).data);
                    break;
                case "doctors":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_doctors (params_element).data);
                    break;
                case "get_visit":
                    var visit = provider.get_visit (id);
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_get_visit (visit).data);
                    break;
                case "visits":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_visits (params_element).data);
                    break;
                case "get_event":
                    var event = provider.get_event (id);
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_get_event (event).data);
                    break;
                case "events":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_events (params_element).data);
                    break;
                case "remove_doctor":
                    msg.set_response ("text/html/", Soup.MemoryUse.COPY,
                        handle_rpc_remove_doctor (params_element).data);
                    break;
                case "remove_patient":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_remove_patient (params_element).data);
                    break;
                case "remove_visit":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_remove_visit (params_element).data);
                    break;
                case "remove_event":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_remove_event (params_element).data);
                    break;
                case "save_doctor":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_save_doctor (params_element).data);
                    break;
                case "save_patient":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_save_patient (params_element).data);
                    break;
                case "save_visit":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_save_visit (params_element).data);
                    break;
                case "save_event":
                    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                        handle_rpc_save_event (params_element).data);
                    break;
                default:
                    msg.set_status (500);
                    msg.set_response ("text/html", Soup.MemoryUse.COPY, 
                        format_response (null).data);
                    break;
            }
        }
        
        private string handle_rpc_get_patient (Patient? patient) {
            if (patient != null) {
                return format_response (patient.to_json ().dumps ());
            }
            else {
                return format_response (null);
            }
        }
        
        private string handle_rpc_patients (Json root) {
            Doctor? doctor = null;
            Json? doctor_element = root.object_get ("doctor");
            if (doctor_element != null && doctor_element.is_integer ()) {
                doctor = provider.get_doctor (doctor_element.integer_value ());
            }
        
            var builder = new StringBuilder ();
            builder.append_unichar ('[');
            foreach (var patient in provider.patients (doctor)) {
                builder.append (patient.id.to_string ());
                builder.append_unichar (',');
            }
            if (builder.len > 1)
                builder.truncate (builder.len - 1);
            builder.append_unichar (']');
            
            return format_response (builder.str);
        }
        
        private string handle_rpc_get_doctor (Doctor? doctor) {
            if (doctor != null) {
                return format_response (doctor.to_json ().dumps ());
            }
            else {
                return format_response (null);
            }
        }
        
        private string handle_rpc_doctors (Json root) {
            var builder = new StringBuilder ();
            
            builder.append_unichar ('[');
            foreach (var doctor in provider.doctors ()) {
                builder.append (doctor.id.to_string ());
                builder.append_unichar (',');
            }
            if (builder.len > 1)            
                builder.truncate (builder.len - 1);
            builder.append_unichar (']');
            
            return format_response (builder.str);
        }
        
        private string handle_rpc_get_visit (Visit? visit) {
            if (visit != null)
                return format_response (visit.to_json ().dumps ());
            else
                return format_response ();
        }
        
        private string handle_rpc_visits (Json root) {
            var builder = new StringBuilder ();
            
            DateTime? start_date = null;
            DateTime? end_date = null;
            
            Json? start_date_element = root.object_get ("start_date");
            if (start_date_element != null && start_date_element.is_string ()) {
                start_date = SqliteDataProvider.string_to_datetime (start_date_element.string_value ());
            }
            
            Json? end_date_element = root.object_get ("end_date");
            if (end_date_element != null && end_date_element.is_string ()) {
                end_date = SqliteDataProvider.string_to_datetime (end_date_element.string_value ());
            }
            
            Patient? patient = null;
            Json? patient_element = root.object_get ("patient");
            if (patient_element != null && patient_element.is_integer ()) {
                patient = provider.get_patient (patient_element.integer_value ());
                
                if (patient == null) {
                    return format_response ();
                }
            }
            
            builder.append_unichar ('[');
            foreach (var visit in provider.visits (patient, start_date, end_date)) {
                builder.append (visit.id.to_string ());
                builder.append_unichar (',');
            }
            if (builder.len > 1)            
                builder.truncate (builder.len - 1);
            builder.append_unichar (']');
            
            return format_response (builder.str);
        }
        
        private string handle_rpc_get_event (Event? event) {
            if (event != null) {
                return format_response (event.to_json ().dumps ());
            }
            else {
                return format_response ();
            }
        }
        
        private string handle_rpc_events (Json root) {
            // Get bounds
            DateTime? start_date = null;
            DateTime? end_date = null;
            
            Json? start_date_element = root.object_get ("start_date");
            if (start_date_element != null && start_date_element.is_string ()) {
                start_date = SqliteDataProvider.string_to_datetime (start_date_element.string_value ());
            }
            
            Json? end_date_element = root.object_get ("end_date");
            if (end_date_element != null && end_date_element.is_string ()) {
                end_date = SqliteDataProvider.string_to_datetime (end_date_element.string_value ());
            }      
            
            var builder = new StringBuilder ();
            builder.append_unichar ('[');
            foreach (var event in provider.events (start_date, end_date)) {
                builder.append (event.id.to_string ());
                builder.append_unichar (',');
            }
            if (builder.len > 1)
                builder.truncate (builder.len - 1);
            builder.append_unichar (']');
            
            return format_response (builder.str);
        }
        
        private string handle_rpc_remove_doctor (Json root) {
            // Get ID of the doctor that needs to be removed
            int64 id = 0;
            Json? id_element = root.object_get ("id");
            if (id_element != null && id_element.is_integer ()) {
                id = id_element.integer_value ();
            }
            else {
                return format_response (null);
            }
            
            var doc = provider.get_doctor (id);
            if (doc != null) {            
                provider.remove_doctor (doc);
            }
            else {
                warning (_("Trying to removing doctor with ID = %ld failed"), id);
                return format_response (null);
            }
            
            return format_response ("{}");
        }
        
        private string handle_rpc_remove_patient (Json root) {
            // Get ID of the patient that needs to be removed
            int64 id = 0;
            Json? id_element = root.object_get ("id");
            if (id_element != null && id_element.is_integer ()) {
                id = id_element.integer_value ();
            }
            else {
                return format_response (null);
            }
            
            var patient = provider.get_patient (id);
            if (patient != null) {            
                provider.remove_patient (patient);
            }
            else {
                warning (_("Trying to removing patient with ID = %ld failed"), id);
                return format_response (null);
            }
            
            return format_response ("{}");  
        }
        
        private string handle_rpc_remove_visit (Json root) {
            // Get ID of the visit that needs to be removed
            int64 id = 0;
            Json? id_element = root.object_get ("id");
            if (id_element != null && id_element.is_integer ()) {
                id = id_element.integer_value ();
            }
            else {
                return format_response (null);
            }
            
            var visit = provider.get_visit (id);
            if (visit != null) {            
                provider.remove_visit (visit);
            }
            else {
                warning (_("Trying to removing visit with ID = %ld failed"), id);
                return format_response (null);
            }
            
            return format_response ("{}");  
        }
        
        private string handle_rpc_remove_event (Json root) {
            // Get ID of the event that needs to be removed
            int64 id = 0;
            Json? id_element = root.object_get ("id");
            if (id_element != null && id_element.is_integer ()) {
                id = id_element.integer_value ();
            }
            else {
                return format_response (null);
            }
            
            var event = provider.get_event (id);
            if (event != null) {            
                provider.remove_event (event);
            }
            else {
                warning (_("Trying to removing event with ID = %ld failed"), id);
                return format_response (null);
            }
            
            return format_response ("{}");  
        }
        
        private string handle_rpc_save_doctor (Json root) {
            var doctor_node = root.object_get ("doctor");
            if (doctor_node == null) {
                return format_response (null);
            }
            else {
                int64 new_id = provider.save_doctor (new Doctor.from_json (doctor_node, provider));
                return format_response ("""{"id":%s}""".printf (new_id.to_string ()));
            }
        }
        
        private string handle_rpc_save_patient (Json root) {
            var patient_node = root.object_get ("patient");
            if (patient_node == null) {
                return format_response (null);
            }
            else {
                int64 new_id = provider.save_patient (new Patient.from_json (patient_node, provider));
                return format_response ("""{"id":%s}""".printf (new_id.to_string ()));
            }
        }
        
        private string handle_rpc_save_visit (Json root) {
            var visit_node = root.object_get ("visit");
            if (visit_node == null) {
                return format_response (null);
            }
            else {
                int64 new_id = provider.save_visit (new Visit.from_json (visit_node, provider));
                return format_response ("""{"id":%s}""".printf (new_id.to_string ()));
            }
        }
        
        private string handle_rpc_save_event (Json root) {
            var event_node = root.object_get ("event");
            if (event_node == null) {
                return format_response (null);
            }
            else {
                int64 new_id = provider.save_event (new Event.from_json (event_node));
                return format_response ("""{"id":%s}""".printf (new_id.to_string ()));
            }
        }
        
                
        
        protected string format_response (string? result = null) {
            bool successful = true;
            if (result == null) {
                successful = false;
                result = "0";
            }
            
            return """{"jsonrpc": "2.0", "id": %s, "result": %s, "error": %s}""".printf (
                processing_id, 
                result, (successful == true) ? "null" : """{ "message": "Internal server error", "code": -1 }""");
        }
                
        public void start () {
            debug ("Starting DataServer");
            server.run_async ();
        }
        
        public void stop () {
            debug ("Stopping DataServer");
            server.quit ();
        }
    
    }
 
 }
