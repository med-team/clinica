/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
namespace Clinica {

    public class Toolbar : Gtk.Alignment {
    
        protected ResourceManager resource_manager;
        
        protected Gtk.Box toolbar;
    
        public Toolbar (ResourceManager resources) {
			GLib.Object (xalign: 1.0F, yalign: 0.0F, xscale: 1F, yscale: 0F);
			set_padding (0, 0, resources.PADDING, 0);
            resource_manager = resources;
            
            toolbar = new Gtk.Box (Gtk.Orientation.HORIZONTAL, resource_manager.PADDING);
            add (toolbar);
        }
        
        public void insert (Gtk.ToolItem item, int pos) {
            toolbar.pack_start (item, false, true, 0);
        }
    
    }

}
