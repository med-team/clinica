/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

using Gtk;

namespace Clinica {
    
    public class MedicineTreeView : Gtk.TreeView {
    
        private ListStore medicine_store { get; set; }
        
        private ResourceManager resource_manager { get; set; }
        
        public MedicineTreeView (ResourceManager resources) {
            resource_manager = resources;
            medicine_store = new ListStore (3, typeof (string), typeof (string), typeof (Medicine));
            set_model (medicine_store);
            
            /* Create necessary columns */
            var name_renderer = new CellRendererText ();
            insert_column_with_attributes (0, _("Medicine name"), name_renderer, "text", 0);
            
            var price_renderer = new CellRendererText ();
            insert_column_with_attributes (1, _("Price"), price_renderer, "text", 1);
            
            row_activated.connect (on_row_activated);
            set_rules_hint (true);
        }
        
        private void on_row_activated (TreePath path) {
            TreeIter iter;
            Value medicine;
            
            /* Retrieve the selected row */
            medicine_store.get_iter (out iter, path);
            medicine_store.get_value (iter, 2, out medicine);
            
            /* Display the detail dialog */
            var dialog = new MedicineDetailDialog (resource_manager, medicine as Medicine);
	    dialog.set_transient_for (resource_manager.user_interface.window);            
            dialog.run ();
            dialog.destroy ();
        }
        
        public void clear () {
            medicine_store.clear ();
        }
        
        /**
         * @brief Push a new medicine into the treeview. 
         * 
         * MUST BE CALLED FROM ANOTHER THREAD!
         */
        public void push_medicine (Medicine medicine) {
            GLib.Idle.add (() => {
                TreeIter iter;
                medicine_store.append (out iter);
                
                medicine_store.set_value (iter, 0, medicine.name);
                medicine_store.set_value (iter, 1, medicine.price);
                medicine_store.set_value (iter, 2, medicine);
                return false;
            });
        }
    }
}
