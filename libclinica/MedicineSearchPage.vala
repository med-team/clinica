/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

using Gtk;

namespace Clinica {
    
    public class MedicineSearchPage : Alignment, Page {
    
        private delegate void SearchFunction();
    
        /**
         * @brief Entry used to insert search keys for the 
         * medicines.
         */
        private FindEntry find_entry;
        
        /**
         * @brief Treeview used to display the list of the medicines
         * that have been found by the search engine.
         */
        public MedicineTreeView treeview;
        
        /**
         * @brief The key that is being searched.
         */
        private string key;
        
        public ResourceManager resource_manager { get; set; }
        
        /**
         * @brief This is the selected search engine, used to retrieve the result
         * that the user asks. 
         */
        private unowned List<MedicineSearchEngine> selected_search_engines;
        
        private SidebarEntry sidebar_entry;
        
        public MedicineSearchPage (ResourceManager resources) {
            resource_manager = resources;
            
            var main_vbox = new Box (Orientation.VERTICAL, resource_manager.PADDING);
            
            /* Add a search bar on top */
            find_entry = new FindEntry ();
            find_entry.activate.connect (on_find_entry_activate);
            main_vbox.pack_start (find_entry, false);
            
            /* And the treeview just below */
            treeview = new MedicineTreeView (resource_manager);
            var scrolled_window = new ScrolledWindow (null, null);
            scrolled_window.set_shadow_type (ShadowType.IN);
            scrolled_window.add (treeview);
            main_vbox.pack_start (scrolled_window);
            
            /* Show all the items added to this VBox */
            add (main_vbox);
            show_all ();
            
            sidebar_entry = new SidebarPageEntry (resource_manager, this, _("Search medicines"),
                Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/search.svg", 
                Sidebar.ICON_SIZE, Sidebar.ICON_SIZE));
        }
        
        /**
         * @brief Callback called when the user want to search for a Medicine.
         */
        private void on_find_entry_activate (Entry e) {
            /* Select the right search engine */
            selected_search_engines = resource_manager.settings.get_medicine_search_engines ();
            if (selected_search_engines.length () == 0) {
                debug ("No search engine available");
                
                /* Tell to the user that a plugin is needed */
                var dialog = new MessageDialog.with_markup (null, 
                    DialogFlags.MODAL,
                    MessageType.INFO,
                    ButtonsType.OK,
                    "%s",
                    _("It's not possible to perform a search for medicine
since no search engine is available.
You should enable a search engine in the plugins
to solve this issue."));
                dialog.set_title (_("No search engine available"));
                dialog.set_transient_for (resource_manager.user_interface.window);
                dialog.run ();
                dialog.destroy ();    
                return;
            }
            
            key = find_entry.get_text ();
            find_entry.set_text (_("Searching for %s...").printf (key));
            find_entry.set_sensitive (false);
            treeview.clear ();
            /* And start the research in another thread */
            try {
                new Thread<void*>.try ("MedicineSearchPage", this.do_search);
            } catch (GLib.Error e) {
                warning ("Failed to start the Medicine search thread");
            }
        }
        
        /**
         * @brief Function that performs the search, spawned in another thread.
         */
        private void* do_search () {
            GLib.Idle.add (() => {
                find_entry.set_sensitive (true);
                find_entry.set_text ("");
                return false;
            });
            
            // Trigger searches
            foreach (var search_engine in selected_search_engines) {
                search_engine.search_medicine (key, treeview);
            }
            return null;
        }
        
        public string get_title () {
            return _("Medicines");
        }
        
        public SidebarEntry? get_sidebar_entry () {
            return sidebar_entry;
        }

    }
}
