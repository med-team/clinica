/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

namespace Clinica {

    public interface VisitIterator : Object {
        public abstract Visit get ();
        public abstract bool next ();
        public VisitIterator iterator () {
            return this;
        }
    }

    public interface PatientIterator : Object {
        public abstract Patient get ();
        public abstract bool next ();
        public PatientIterator iterator () {
            return this;
        }
    }

    public interface DoctorIterator : Object {
        public abstract Doctor get ();
        public abstract bool next ();
        public DoctorIterator iterator () {
            return this;
        }
    }

    public interface EventIterator : Object {
        public abstract Event get ();
        public abstract bool next ();
        public EventIterator iterator () {
            return this;
        }
    }

    public interface DataProvider : Object {

        public enum SortOrder {
            ASCENDING,
            DESCENDING,
        }

        /**
         * @brief Get the name of the DataProvider object. This must be a unique
         * name since it will be used to identify it and to select the right
         * DataProvider on startup.
         */
        public abstract string get_name ();
        
        /**
         * @brief Return the FileStore associated to this DataProvider. 
         * The default implementation returns null, meaning that no file
         * storage system is available. Override this in a custom implementation
         * to change the behaviour. 
         */
        public virtual FileStore? get_file_store () { return null; }

        /**
         * @brief Get the visit with the given id.
         */
        public abstract Visit? get_visit (int64 id);

        /**
         * @brief Save a Visit. If the ID associated is 0
         * than a new ID will be created and will be returned.
         */
        public abstract int64 save_visit (Visit visit);

        /**
         * @brief Remove a Visit from the database. This routine
         * will return 0 on success.
         */
        public abstract int64 remove_visit (Visit visit);

        /**
         * @brief Emitted when a visit changes in the Database; this means
         * that all Visit object with this ID around must be invalidated and
         * reloaded.
         */
        public signal void visit_changed (int64 id);

        /**
         * @brief Emitted when a visit is removed from the database. This means
         * that all the Visit with this id must be removed. Please note that you
         * can't call get_visit () with this ID since the visit has already been
         * removed.
         */
        public signal void visit_removed (int64 id);

        /**
         * @brief Emitted when a new visit is added to the database.
         */
        public signal void visit_added (int64 id);

        /**
         * @brief Iterate on all the visits
         *
         * @param patient If not null then pick up only the visits of the given patient.
         * @param start If not null pick up only visits from this starting date.
         * @param end If not null stop iterating on visits when this date is reached.
         */
        public abstract VisitIterator visits (Patient? patient =
                null, DateTime? start = null, DateTime? end =
                null, SortOrder order = SortOrder.DESCENDING);

        /**
         * @brief Get the Patient with the given id.
         */
        public abstract Patient? get_patient (int64 id);

        /**
         * @brief Save a Patient. If the ID associated is 0
         * than a new ID will be created and will be returned.
         */
        public abstract int64 save_patient (Patient patient);

        /**
         * @brief Remove a Patient from the database. This routine
         * will return 0 on success.
         *
         * This routine must take care of removing all the patients
         * visits and deassociate all the events from this patient.
         */
        public abstract int64 remove_patient (Patient patient);

        /**
         * @brief Emitted when a patient changes in the database; this means
         * that all Patient objects with this ID around must be invalidated and
         * reloaded.
         */
        public signal void patient_changed (int64 id);

        /**
         * @brief Emitted when a patient is removed from the database. This means
         * that all the Patients with this id must be removed. Please note that you
         * can't call get_patient () with this ID since the patient has already been
         * removed.
         */
        public signal void patient_removed (int64 id);

        /**
         * @brief Emitted when a new patient is added to the database.
         */
        public signal void patient_added (int64 id);

        /**
         * @brief Iterate over all the patients in the database.
         *
         * @param doctor If doctor is not null iterate only on patients associated
         * on this doctor.
         */
        public abstract PatientIterator patients (Doctor? doctor = null);

        /**
         * @brief Get the doctor associatied with the given id.
         */
        public abstract Doctor? get_doctor (int64 id);

        /**
         * @brief Save a Doctor. If the ID associated is 0
         * than a new ID will be created and will be returned.
         */
        public abstract int64 save_doctor (Doctor doctor);

        /**
         * @brief Remove a Doctor from the database. This routine
         * will return 0 on success.
         *
         * This routine must take care of deassociating of the
         * patients of this doctor from him/her.
         */
        public abstract int64 remove_doctor (Doctor doctor);

        /**
         * @brief Emitted when a doctor changes in the database; this means
         * that all Doctor objects with this ID around must be invalidated and
         * reloaded.
         */
        public signal void doctor_changed (int64 id);

        /**
         * @brief Emitted when a doctor is removed from the database. This means
         * that all the Doctor with this id must be removed. Please note that you
         * can't call get_doctor () with this ID since the doctor has already been
         * removed.
         */
        public signal void doctor_removed (int64 id);

        /**
         * @brief Emitted when a new doctor is added to the database.
         */
        public signal void doctor_added (int64 id);

        /**
         * @brief Iteratate over all the doctors in the database.
         */
        public abstract DoctorIterator doctors ();

        /**
         * @brief Get the event associated to the given ID.
         */
        public abstract Event? get_event (int64 id);

        /**
         * @brief Save an Event. If the ID associated is 0
         * than a new ID will be created and will be returned.
         */
        public abstract int64 save_event (Event event);

        /**
         * @brief Remove the event from the database.
         */
        public abstract int64 remove_event (Event event);

        /**
         * @brief Emitted when an event changes in the database; this means
         * that all Event objects with this ID around must be invalidated and
         * reloaded.
         */
        public signal void event_changed (int64 id);

        /**
         * @brief Emitted when an event is removed from the database. This means
         * that all the Event with this id must be removed. Please note that you
         * can't call get_event () with this ID since the event has already been
         * removed.
         */
        public signal void event_removed (int64 id);

        /**
         * @brief Emitted when a new event is added to the database.
         */
        public signal void event_added (int64 id);

        /**
         * @brief Iterate over all the events in the database.
         *
         * @param from If from is not null then iterate only from events starting after
         * from.
         * @param to If to is not null then iterate only until events before to.
         */
        public abstract EventIterator events (DateTime? from =
                null, DateTime? to = null);

    }

}
