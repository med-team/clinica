/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {
    
    public class DoctorListStore : ListStore {
    
    	enum Field {
    		DOCTOR,
    		GIVEN_NAME,
    		SURNAME,
    		COMPLETE_NAME,
    	}
    
        public signal void error (string message);
        
		private ResourceManager resource_manager { get; set; }
       
        public class DoctorListStore (ResourceManager resources) {
            resource_manager = resources;
            error.connect ((t,l) => resources.error_callback (t,l));
            
            /* Column headers doctor object, name, surname, complete name */
            Type [] column_headers = { typeof (Doctor) , typeof (string), 
                                       typeof (string), typeof (string)};
                                       
            set_column_types (column_headers);
            
            load_data ();
        }
        
        public void load_data () {            
            /* Asynchronous loading of doctors... */
            Idle.add (() => {
	            foreach (Doctor doc in resource_manager.data_provider.doctors ()) {
    	        	add_doctor (doc.id);
    	        }
    	        
    	        /* Setup callbacks to add or remove doctors */
		        resource_manager.data_provider.doctor_added.connect (
		            (id) => add_doctor (id));
		        resource_manager.data_provider.doctor_changed.connect (
		            (id) => reload_doctor (id));
		        resource_manager.data_provider.doctor_removed.connect (
		            (id) => remove_doctor (id));
    	        
    	        /* We don't need to execute any more */
    	        return false;
    	    });        
        }
        
        private TreeIter id_to_iter (int64 id) {
            TreeIter it;
            Value doctor;
        	if (!get_iter_first (out it)) {
        		error (_("Doctors database seems corrupted."));
        	}
        	do {
        		get_value (it, Field.DOCTOR, out doctor);
        		if ((doctor as Doctor).id == id) {
		            return it;
        		}
        	} while (iter_next (ref it));
        	
        	return it;
        }
        
        private void add_doctor (int64 id) {
            Doctor doc = resource_manager.data_provider.get_doctor (id);
        	TreeIter it;
            append(out it);
            set_value (it, Field.DOCTOR, doc);
            set_value (it, Field.GIVEN_NAME, doc.given_name);
            set_value (it, Field.SURNAME, doc.surname);
            set_value (it, Field.COMPLETE_NAME, doc.get_complete_name ());
        }
        
        private void reload_doctor (int64 id) {
            Doctor doc = resource_manager.data_provider.get_doctor (id);
            TreeIter it = id_to_iter (id);		
            set_value (it, Field.DOCTOR, doc);
		    set_value (it, Field.GIVEN_NAME, doc.given_name);
		    set_value (it, Field.SURNAME, doc.surname);
		    set_value (it, Field.COMPLETE_NAME, doc.get_complete_name ());
        }
        
        private void remove_doctor (int64 id) {
            TreeIter iter = id_to_iter (id);
            remove (iter);
        }
    }
}
