/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 using Gdk;
 
 namespace Clinica {
 
    /**
     * @brief An EventBox is a small widget that represent
     * a Event in a discrete way.
     *
     * It is thought to be stacked in a list of events
     * for a day, or any similar use.
     */
    public class EventDetail : Gtk.EventBox {
    
        private ResourceManager resource_manager { get; private set; }
        
        private Event associated_event { get; private set; }
        
        public EventDetail (ResourceManager resources, Event event) {
            GLib.Object ();
            resource_manager = resources;
            associated_event = event;
            
            var main_box = new Box (Orientation.VERTICAL, resources.PADDING);
			events |= EventMask.BUTTON_PRESS_MASK;
            
            /* First adding hour and title in a row */
            var box = new Box (Orientation.HORIZONTAL, resources.PADDING);
            var hour_label = new Label("%.2d:%.2d".printf (event.date.get_hour(), 
                event.date.get_minute()));
            hour_label.set_valign (Align.START);
            box.pack_start (hour_label, false, true);
            var title_label = new Label("");
            title_label.set_markup ("<b>%s</b>".printf (event.title));
            title_label.set_alignment (0.0f, 0.5f);
            title_label.set_size_request (120, -1);
            title_label.set_line_wrap (true);
            title_label.set_valign (Align.START);
            box.pack_start (title_label);
            
            /* Adding action buttons, such as edit and delete */
            var edit_button = new Button ();
            edit_button.add (new Image.from_resource (Config.RESOURCE_BASE + "ui/icons/calendar_edit.png"));
            edit_button.activate.connect ((button) => on_edit_menu_item_activate ());
            edit_button.set_tooltip_text (_("Edit event"));
            edit_button.clicked.connect ((button) => on_edit_menu_item_activate ());
            edit_button.set_relief (ReliefStyle.NONE);
            edit_button.set_valign (Align.START);
            box.pack_start (edit_button, false, false);
            
            var remove_button = new Button ();
            remove_button.add (new Image.from_resource (Config.RESOURCE_BASE + "ui/icons/calendar_delete.png"));
            remove_button.activate.connect ((button) => on_remove_menu_item_activate ());
            remove_button.clicked.connect ((button) => on_remove_menu_item_activate ());
            remove_button.set_tooltip_text (_("Delete event"));            
            remove_button.set_relief (ReliefStyle.NONE);
            remove_button.set_valign (Align.START);
            box.pack_start (remove_button, false, false);
            
            box.set_valign (Align.START);
            
            /* Packing first line up */
            main_box.pack_start (box, false, true);
            
            /* And then adding the description */
            var description_label = new Label (event.description);
            description_label.set_alignment(0.0f, 0.5f);
            description_label.set_margin_left (15);
            description_label.set_line_wrap (true);
            description_label.set_size_request (145, -1);
            
            
            /* Packing it up */
            main_box.pack_start (description_label, false, true);
            
            
            
            /* Connect the right click to make event deletable and/or modifiable */
            button_press_event.connect (on_button_press_event);
            
            add (main_box);
        }
        
        /**
         * @brief Callback called on the button_press_event of the widget, that pops
         * the menu up and shows options to edit and/or delete the event.
         */
        private bool on_button_press_event (Widget widget, EventButton event) {
            if (event.button == 3 && event.type == EventType.BUTTON_PRESS) {
                var popup_menu = new Gtk.Menu ();
                var edit_menu_item = new Gtk.MenuItem.with_label (_("Edit"));
                var remove_menu_item = new Gtk.MenuItem.with_label (_("Remove"));
                
                /* Connect callbacks for the item */
                edit_menu_item.activate.connect (on_edit_menu_item_activate);
                remove_menu_item.activate.connect (on_remove_menu_item_activate);
                
                /* Attach items to the menu and the menu to the widget */
                popup_menu.append (edit_menu_item);
                popup_menu.append (remove_menu_item);
                popup_menu.attach_to_widget (this, null);
                
                /* Show the menu */
                popup_menu.show_all ();
                popup_menu.popup (null, null, null, event.button, event.time);
            }
            
            return false;
        }
        
        private void on_edit_menu_item_activate (Gtk.MenuItem? item = null) {
            var event_editor = new EventEditor (resource_manager, associated_event);
            event_editor.set_transient_for (resource_manager.user_interface.calendar_window);            
            if (event_editor.run () == EventEditor.Response.SAVE) {
                resource_manager.data_provider.save_event (event_editor.selected_event);
            }
            
            event_editor.destroy ();
        }
        
        private void on_remove_menu_item_activate (Gtk.MenuItem? item = null) {
        	var question = new MessageDialog (null, 
        				DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT, 
        				MessageType.QUESTION, ButtonsType.YES_NO, "%s", 
      					_("Really delete this event?"));
	        question.set_transient_for (resource_manager.user_interface.calendar_window);      					
        	if (question.run () != ResponseType.YES) {
        		question.destroy ();
       			return;
       		}
       		question.destroy ();
        		
            resource_manager.data_provider.remove_event (associated_event);
        }
    }
 }
