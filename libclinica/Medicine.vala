

namespace Clinica {

    public class Medicine : GLib.Object {

        /**
         * @brief Identification code of the medicine. This
         * is to be used in the catalog to retrieve data
         * from the medicine.
         */
        public string id { get; set; }
        
        /**
         * @brief Name of the medicine, that is to
         * be used for displaying in list and as title of
         * more details dialog and similar
         */
        public string name { get; set; }
        
        /**
         * @brief Long description of the medicine, 
         * shown when the details of it are requested
         */
        public string description { get; set; }
        
        /**
         * @brief Active ingredient contained in the medicine.
         * The common name shall be used here, and not the 
         * formula.
         */
        public string active_ingredient { get; set; }
        
        /**
         * @brief Particular recommendation on the storage of
         * the medicine. 
         */
        public string storage_reccomendations { get; set; }
        
        /**
         * @brief Price of the medicine, if present.
         */
        public string price { get; set; }
        
        /**
         * @brief Other notes that are not contained in the previous
         * fields and are likely to be specific of the search engine.
         * 
         * Examples may include local codes, annotations for laws in
         * a particular country and so on.
         * 
         * The provided notes should be formatted as <b>Note name:</b> Note content
         */
        public string other_notes { get; set; }
        
        public Medicine () {
            /* Leave all the fields set to empty string so we
             * can read them without trying for a null value */
            name = "";
            description = "";
            id = "";
            active_ingredient = "";
            storage_reccomendations = "";
            price = "";
            other_notes = "";
        }
    }
}
