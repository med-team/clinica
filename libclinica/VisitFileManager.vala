/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
 using Gtk;
 
 namespace Clinica {
 
    public class VisitFileManager : Gtk.VBox {
    
        /**
         * @brief The resource manager of the Clinica application, used to retrieve
         * information and data.
         */
        private ResourceManager resource_manager;
        
        /**
         * @brief The Visit whose files we are displaying.
         */
        private Visit visit;
        
        /**
         * @brief The button used to attach a new file.
         */
        private Button attach_button;
        
        /** 
         * @brief Button that triggers the opening
         * of a file manager window of the location
         * where the file are stored.
         */
        private Button browser_button;
        
        /**
         * @brief The maximum width of this column. This is enforced with
         * set_size_request() on the VBox widget.
         */
        private int width = 140;
        
        /**
         * @brief A list of all the files displayed.
         */
        GLib.List<FileDetail> files;
        
        /**
         * @brief The VisitWindow in which we are, that will be the parent of all
         * Dialogs.
         */
        private VisitPage visit_page;
        
        /**
         * @brief The FileStore associated with the current DataProvider. 
         */
        private FileStore? file_store;
    
        public VisitFileManager (ResourceManager resources, VisitPage visit_page, Visit? visit) {
            GLib.Object (homogeneous: false, spacing: resources.PADDING);
            resource_manager = resources;
            this.visit = visit;
            this.visit_page = visit_page;
            
            setup_widget ();
        }
            
        private void setup_widget () {
            
            file_store = resource_manager.data_provider.get_file_store ();
            
            if (file_store == null) {
                return;
            }
            
            /* Add a Attach button */
            attach_button = new Button.with_label (_("Attach file"));
            attach_button.clicked.connect (on_attach_button_clicked);
            attach_button.set_size_request (-1, 45);
            pack_start (attach_button, false, true);
            
            /* Add a browser folder button */
            if (resource_manager.settings.get_boolean ("show-visit-file-manager") &&
               (file_store != null)) {
                browser_button = new Button.with_label (_("Browse files"));
                browser_button.clicked.connect (on_browse_button_clicked);
                pack_start (browser_button, false, true);
            }
            
            files = new GLib.List<FileDetail> ();
            
            reload_file_list ();
            set_size_request (width, -1);
            
            file_store.changed.connect (on_file_store_changed);
            
            show_all ();
        }
        
        public void on_attach_button_clicked (Button button) {
            var fd = new FileChooserDialog (_("Select a file to attach"), resource_manager.user_interface.window, 
                    FileChooserAction.OPEN, Gtk.Stock.CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.Stock.SAVE, Gtk.ResponseType.ACCEPT);
            fd.set_transient_for (resource_manager.user_interface.window);
            if (fd.run () == Gtk.ResponseType.ACCEPT) {
                file_store.store_file (visit.id, fd.get_filename ());
            }
            
            fd.destroy ();
            reload_file_list ();
        }
        
        /**
         * @brief Open a file manager to show the files associated with the visit.
         */
        private void on_browse_button_clicked (Button button) {
            try {
                string path = file_store.get_id_path (visit.id);
                Gtk.show_uri (null, "file://" + path, Gdk.CURRENT_TIME);
            } catch (GLib.Error e) {
                warning (_("Unable to open the browser on the filestore position"));
            }
        }
        
        /**
         * @brief Callback for the FileStore changed() event, that
         * reload the files if the file changed were the ones that 
         * belong to this visit.
         */
        private void on_file_store_changed (int64 id) {
            if (visit != null && id == visit.id)
                reload_file_list ();
        }
        
        public void reload_file_list () {
        
            if (file_store == null)
                return;
            
            foreach (FileDetail detail in files) {
                remove (detail);
            }
            
            /* Reset the list */
            files = new GLib.List<FileDetail> ();
        
            if (visit == null) {
                attach_button.set_sensitive (false);
                if (browser_button != null)
                    browser_button.set_sensitive (false);
                (attach_button.get_child () as Label).set_text (_("Save the visit\n to attach files"));
                return;
            }
            else {
                attach_button.set_sensitive (true);
                if (browser_button != null)
                    browser_button.set_sensitive (true);
                (attach_button.get_child () as Label).set_text (_("Attach files"));
            }

            foreach (FileObject obj in file_store.get_files (visit.id)) {
                var file_detail = new FileDetail (resource_manager, visit_page, obj);
                pack_start (file_detail, false, true);
                file_detail.show_all ();
                files.append (file_detail);
            }
        }
        
        public void update_visit (Visit visit) {
            this.visit = visit;
            reload_file_list ();
        }
    
    }
 
 }
