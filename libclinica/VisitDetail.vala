/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 using Gdk;
 
namespace Clinica {
 
    /**
     * @brief An EventBox is a small widget that represent
     * a Event in a discrete way.
     *
     * It is thought to be stacked in a list of events
     * for a day, or any similar use.
     */
    public class VisitDetail : Gtk.EventBox {
    
        private ResourceManager resource_manager { get; private set; }
        
        private Visit associated_visit { get; private set; }
        
        public VisitDetail (ResourceManager resources, Visit visit) {
            GLib.Object ();
            resource_manager = resources;
            associated_visit = visit;
            
            var main_box = new Box (Orientation.VERTICAL, resources.PADDING);
			events |= EventMask.BUTTON_PRESS_MASK;
            
            /* First adding hour and title in a row, followed by action buttons */
            var box = new Box (Orientation.HORIZONTAL, resources.PADDING);
            
            /* Hour, aligned on top. */
            var hour_label = new Label("%.2d:%.2d".printf (visit.date.get_hour(), 
                visit.date.get_minute()));
            hour_label.set_valign (Align.START);
            box.pack_start (hour_label, false, true);
            
            /* Title, with multiline enabled. */
            var title_label = new Label("");
            title_label.set_markup ("Visit of <b>%s</b>".printf (visit.patient.get_complete_name ()));
            title_label.set_alignment (0.0f, 0.5f);
            title_label.set_line_wrap (true);
            title_label.set_size_request (161, -1);
            title_label.set_valign (Align.START);
            box.pack_start (title_label);
            
            /* Action delete this visit */
            var edit_button = new Button ();
            edit_button.add (new Image.from_resource (Config.RESOURCE_BASE + "ui/icons/calendar_edit.png"));
            edit_button.activate.connect ((button) => on_open_visit_menu_item_activate ());
            edit_button.clicked.connect ((button) => on_open_visit_menu_item_activate ());
			edit_button.set_tooltip_text (_("Edit visit"));            
            edit_button.set_relief (ReliefStyle.NONE);            
            box.pack_start (edit_button, false, true);
            edit_button.set_valign (Align.START);
            
            /* Action show visit details */            
            var remove_button = new Button ();
            remove_button.add (new Image.from_resource (Config.RESOURCE_BASE + "ui/icons/calendar_delete.png"));
            remove_button.activate.connect ((button) => on_remove_menu_item_activate ());
            remove_button.clicked.connect ((button) => on_remove_menu_item_activate ());
            remove_button.set_tooltip_text (_("Delete event"));            
            remove_button.set_relief (ReliefStyle.NONE);
            remove_button.set_valign (Align.START);
            box.pack_start (remove_button, false, false);
            
            /* Packing it up */
            main_box.pack_start (box, false, true);
            
            
            /* Connect the right click to make event deletable and/or modifiable */
            button_press_event.connect (on_button_press_event);
            
            add (main_box);
        }
        
        /**
         * @brief Callback called on the button_press_event of the widget, that pops
         * the menu up and shows options to edit and/or delete the event.
         */
        private bool on_button_press_event (Widget widget, EventButton event) {
            if (event.button == 3 && event.type == EventType.BUTTON_PRESS) {
                var popup_menu = new Gtk.Menu ();
                popup_menu.attach_to_widget (this, null);
                
                var open_visit_menu_item = new Gtk.MenuItem.with_label (_("Show details"));
                open_visit_menu_item.activate.connect (on_open_visit_menu_item_activate);
                popup_menu.append (open_visit_menu_item);
                
                /* Show the menu */
                popup_menu.show_all ();
                popup_menu.popup (null, null, null, event.button, event.time);
            }
            
            return false;
        }
        
        private void on_open_visit_menu_item_activate (Gtk.MenuItem? item = null) {
            var page = resource_manager.user_interface.show_visit_window (associated_visit.patient);
            page.select_visit (associated_visit);
        }
        
        private void on_remove_menu_item_activate (Gtk.MenuItem? item = null) {
            /* Ask before deleting a visit */
		    var dialog = new MessageDialog (null, DialogFlags.DESTROY_WITH_PARENT | DialogFlags.MODAL, 
		                                    MessageType.QUESTION, ButtonsType.YES_NO, "%s", 
		                                    _("Deleting a visit will cause all its data to be lost.\nDo you really want to continue?"));
		    if (dialog.run () == ResponseType.YES) {
		        resource_manager.data_provider.remove_visit (associated_visit);
            }
            
            dialog.destroy ();
        }
    }
 }
