/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

	/**
     * @brief Extension of PatientListView that is filtered based on the
     * content of the widget find_entry. 
     */
    public class PatientFilteredListStore : TreeModelFilter {
    
    	/**
    	 * @brief Gtk.Entry used for filtering the PatientListStore.
    	 */
    	private Entry find_entry;
    	
		private ResourceManager resource_manager;
    	
    	public PatientFilteredListStore (ResourceManager resources, Entry find_entry) {
    		GLib.Object (child_model: resources.patient_list_store);
    	    resource_manager = resources;
    		this.find_entry = find_entry;
    		
    		find_entry.icon_release.connect (on_icon_release);
    		find_entry.key_press_event.connect (on_escape_key);
            	find_entry.changed.connect (on_find_entry_changed);
            	find_entry.set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, null);
    		
    		/* If we change the content of the find_entry 
    		 * the model must be refiltered */
    		find_entry.changed.connect ((t) => refilter ());
    		
    		/* Init the filtered model */
    		set_visible_func(filter_tree);
    	}
    	
        /**
         * @brief Clear the find_entry. 
         */
    	private void clear_find_entry() {
            find_entry.set_text("");
	    find_entry.set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, null);
        }
	
	/**
         * @brief Clear icon in the find_entry. 
         */
    	private void on_icon_release(Gtk.EntryIconPosition pos, Gdk.Event event) {
            if (Gtk.EntryIconPosition.SECONDARY == pos)
	       	clear_find_entry();
        }
        
        /**
         * @brief Possibility to clear the
         * find_entry by typing 'Esc'. 
         */
        private bool on_escape_key(Gdk.EventKey e) { 
            if(Gdk.keyval_name(e.keyval) == "Escape")
		clear_find_entry();
            // Continue processing this event, since the 
            // text entry functionality needs to see it too. 
            return false; 
        }

        public void on_find_entry_changed (Editable e) {
            find_entry.set_icon_from_stock (Gtk.EntryIconPosition.SECONDARY, "gtk-clear");
        }
            	
    	/**
    	 * @brief Actually filter the treemodel based on the content
    	 * of the find_entry
    	 */
    	private bool filter_tree (TreeModel model, TreeIter iter) {
			/* Retrieve data from the model */
			GLib.Value value;
			model.get_value (iter, 0, out value);
			Patient p = value as Patient;
			
			/* Cope with the condition where the liststore isn't really
			 * active and so no entry are returned... */
			if (p == null)
				return true;
				
			/* Return "visible" if what the user has type is in the complete
			 * name of the patient */
			return (find_entry.get_text ().up()  in p.get_complete_name ().up ());
    	}
    }
    
}
