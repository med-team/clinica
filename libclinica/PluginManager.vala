/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;
using PeasGtk;

namespace Clinica {

    public class PluginManager : Alignment {
        
        public PluginManager (ResourceManager resources) {
            GLib.Object (xalign: 0.5F, yalign: 0.5F, xscale: 1.0F, yscale: 1.0F);
            
            var vbox = new Box (Orientation.VERTICAL, 6);
            var pm = new PeasGtk.PluginManager (resources.plugin_engine);
            
            vbox.pack_start (new Label (
                _("Clinica is extensible via plugins and you can easily enable\n and disable them using this window.")),
                false, false);
            vbox.pack_start (pm);
            
            var align = new Alignment (0.5F, 0.5F, 1, 1);
            align.set_padding (resources.PADDING, resources.PADDING, 
                resources.PADDING, resources.PADDING);
            
            align.add (vbox);
            add (align);
        }
    }
}
