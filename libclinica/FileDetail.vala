/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 using Gdk;
 
 namespace Clinica {
 
    public class FileDetail : Gtk.EventBox {
    
        /**
         * @brief Signal emitted when an error occurs.
         */
        public signal void error (string message);
    
        /**
         * @brief The FileObject that describes the file that we are displaying.
         */
        private FileObject file_object;
        
        /** 
         * @brief The resource manager used to retrieve data about files, patients
         * and more.
         */
        private ResourceManager resource_manager;
        
        /**
         * @brief The maixmum number of characters shown in the file name before
         * starting to elide characters.
         */
        private const int max_chars = 18;
        
        /**
         * @brief The VisitWindow in which we are packed, that will be the parent
         * of all dialogs.
         */
        private VisitPage visit_page;
        
        /**
         * @brief The current FileStore found in the DataProvider implementation.
         */
        private FileStore file_store;
    
        public class FileDetail (ResourceManager resources, VisitPage page, FileObject object) {
            resource_manager = resources;
			events |= EventMask.BUTTON_PRESS_MASK;
			file_object = object;
			visit_page = page;
			
			file_store = resource_manager.data_provider.get_file_store ();
			
			error.connect ((t,l) => resource_manager.error_callback (t,l));
			
			var box = new Box (Orientation.HORIZONTAL, resource_manager.PADDING);
			
			/* Elide strings if longer than maximum allowed chars */
			string button_label = object.get_file_name ();
			if (button_label.length > max_chars) {
			    button_label = button_label[0:max_chars - 2] + "...";
            }
            
            
			var open_button = new Button();
			var button_box = new Box (Orientation.HORIZONTAL, resource_manager.PADDING);
            
            /* First get a suitable icon for the file */        
            try {
                Icon icon = file_object.get_file ().query_info(FileAttribute.STANDARD_ICON, FileQueryInfoFlags.NONE).get_icon ();
                button_box.pack_start (new Gtk.Image.from_gicon (icon, IconSize.BUTTON), false, true);
            } catch (GLib.Error e) {
                // Nothing to do for now.
                warning (_("Error while loading icon for file %s: %s").printf (file_object.get_file_name (), e.message));
            }
            
			/* Create the Open File button */
			var button_text = new Label (button_label);
			button_text.set_alignment (0.0f, 0.5f);
			button_box.pack_start (button_text, true, true);
			
			open_button.set_relief (ReliefStyle.NONE);
			open_button.set_tooltip_text (_("Open file"));
			open_button.clicked.connect (on_open_button_clicked);
			open_button.activate.connect (on_open_button_clicked);
			open_button.add (button_box);
			
			box.pack_start (open_button, true, true);
			
			/* Create the remove file button */
			var remove_button = new Button ();
            remove_button.add (new Image.from_resource (Config.RESOURCE_BASE + "ui/icons/calendar_delete.png"));
            remove_button.activate.connect (on_remove_button_clicked);
            remove_button.clicked.connect (on_remove_button_clicked);
            remove_button.set_tooltip_text (_("Delete file"));            
            remove_button.set_relief (ReliefStyle.NONE);
            box.pack_start (remove_button, false, false);
            
            /* Add the box */
			add (box);
        }
        
        private void on_remove_button_clicked (Button button) {
            var dialog = new MessageDialog (resource_manager.user_interface.window,
                DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT, 
                MessageType.QUESTION, ButtonsType.YES_NO, "%s", 
                _("Do you really want to delete the file %s?\nIf you proceed it will be definitively lost.").printf (file_object.get_file_name ()));
            dialog.set_transient_for (resource_manager.user_interface.window);
            if (dialog.run () == Gtk.ResponseType.YES) {
                file_store.remove_file (file_object.get_id (), file_object.get_file_name ());
            }
                
            dialog.destroy ();
        }
        
        private void on_open_button_clicked (Button button) {
            try {
                Gtk.show_uri (null, "file://" + file_object.get_file_path (), Gdk.CURRENT_TIME);
            } catch (GLib.Error e) {
                error (_("Error while opening the file %s").printf (file_object.get_file_name ()));
            }
        }
    
    }
 }
