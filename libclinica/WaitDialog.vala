/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Gtk;
 
namespace Clinica {

    public class WaitDialog : Dialog {
    
        private ResourceManager resource_manager;
        
        private Label label;
        
        private ProgressBar progress_bar;
    
        public WaitDialog (ResourceManager resources, string title) {
            resource_manager = resources;
            
            var builder = new Clinica.Builder.with_filename (resources, "wait_dialog.glade");
            builder.load_into_dialog (this);
            
            label = builder.get_object ("label") as Label;
            progress_bar = builder.get_object ("progressbar") as ProgressBar;
            
            // Don't interact with the rest of the app while performing
            // this operation. 
            set_modal (true);
            
            // Don't allow to close the window
            delete_event.connect ((event) => true);
            
            set_title (title);
        }
        
        private void gtk_flush () {
            // Flush remaining GTK events to make sure that this dialog 
            // is shown to the user. 
            while (Gtk.events_pending ())
                Gtk.main_iteration_do (false);        
        }
        
        public void set_message (string message) {
            label.set_markup (message);
            gtk_flush ();
        }
        
        public void set_progress (double progress) {
            progress_bar.set_fraction (progress);
            gtk_flush ();
        }
    
    }

}
