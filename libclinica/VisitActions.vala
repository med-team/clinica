/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

	public class VisitActions : Clinica.Toolbar {
	
		/* Buttons */
		private ToolbarItem show_details_item;
		private ToolbarItem save_item;
		private ToolbarItem pdf_item;
		private ToolbarItem print_item;
		private ToolbarItem delete_item;
		private ToolbarItem close_item;
	
		/* Signals */
		public signal void save_visit ();
		public signal void save_as_pdf ();
		public signal void print_visit ();
		public signal void show_details ();
		public signal void delete_visit ();

		/**
		 * @brief Id of the connection to the saved signal of the
		 * Visit
		 */
		private ulong visit_connection_id = 0;
		
		/**
		 * @brief The visit to which the VisitAction refers.
		 */
        private VisitTab visit_tab;
	
		public VisitActions (ResourceManager resources) {
			base (resources);
			
			/* Show more details on this patient */
			show_details_item = new ToolbarItem (resource_manager, 
			    Config.RESOURCE_BASE + "ui/icons/small_pencil.png");
			show_details_item.activated.connect ((button) => show_details ());
			show_details_item.set_tooltip_text (_("Edit details of patient"));
			insert (show_details_item, -1);
			
			/* Delete Visit button */
			delete_item = new ToolbarItem (resource_manager, 
			    Config.RESOURCE_BASE + "ui/icons/small_remove_2.png");
			delete_item.activated.connect ((button) => delete_visit ());
			delete_item.set_tooltip_text (_("Remove this visit"));
			insert (delete_item, -1);
			
			/* Save the visit as a PDF file */
			pdf_item = new ToolbarItem (resource_manager, 
			    Config.RESOURCE_BASE + "ui/icons/small_pdf.png");
			pdf_item.activated.connect ((button) => save_as_pdf ());
			pdf_item.set_tooltip_text (_("Export this visit as PDF"));
			insert (pdf_item, -1);
			
			/* Directly print the visit */
			print_item = new ToolbarItem (resource_manager,
			    Config.RESOURCE_BASE + "ui/icons/small_print.png");
			print_item.activated.connect ((button) => print_visit ());
			print_item.set_tooltip_text (_("Print a report of this visit"));
			insert (print_item, -1);
		
			/* The save button */
			save_item = new ToolbarItem (resource_manager, 
			    Config.RESOURCE_BASE + "ui/icons/small_ok_2.png");
			save_item.activated.connect ((button) => save_visit ());
			save_item.set_tooltip_text (_("Save this visit"));
			insert (save_item, -1);
			
			/* The close button */
			close_item = new ToolbarItem (resource_manager,
			    Config.RESOURCE_BASE + "ui/icons/small_close.png");
			close_item.activated.connect ((button) => { 
			    resource_manager.user_interface.unregister_page (
    			    "patients/%s".printf (visit_tab.visit.patient.id.to_string ()));
    			resource_manager.user_interface.set_page ("patients");
    	    }); 
    	    close_item.set_tooltip_text (_("Close the visits of this patient"));
			insert (close_item, -1);
			
			show_all ();
		}
		
		/**
		 * @brief Set active visit of which the actions refers. This is used mainly
		 * to set the status of the delete buttons.
		 */
		public void set_active_tab (VisitTab visittab) {
		    /* Disconnect the old visit, if still connected */
		    if (visit_connection_id != 0)
                this.visit_tab.disconnect (visit_connection_id);
                
            /* Set the sensitivity of the button according to the changed
             * visit. */
            this.visit_tab = visittab;
            visit_connection_id = visit_tab.saved.connect ((visit) => delete_item.set_active (true));
            if (visit_tab.visit.id != 0)
                delete_item.set_active (true);
            else
                delete_item.set_active (false);
		}
	}
	
}
