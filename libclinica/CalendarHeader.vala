/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 *            Maxwell Barvian from Maya - http://launchpad.net/maya/
 */

using Gtk;
using Cairo;

namespace Clinica {

	public class CalendarHeader : Gtk.EventBox {
	
		private Grid table;
		private Label[] labels;
		private CalendarView calendar_view;
	
		public CalendarHeader (CalendarView view) {
		    this.calendar_view = view;
			
			table = new Grid ();
			table.row_homogeneous = table.column_homogeneous = true;
		
			// EventBox properties
			set_visible_window (true); // needed for style
			get_style_context ().add_provider (calendar_view.style_context, 600);
			get_style_context ().add_class ("header");
			
			labels = new Label[7];
			for (int c = 0; c < 7; c++) {
				labels[c] = new Label ("");
				labels[c].draw.connect (on_draw);
				table.attach (labels[c], c, 0, 1, 1);
			}
			update_columns ();
			
			add (table);
		}
		
		private void update_columns () {
			
			var date = new DateTime.now_local ();
			date = date.add_days (1 - date.get_day_of_week ());
			foreach (var label in labels) {
				label.label = date.format ("%A");
				date = date.add_days (1);
			}
		}
		
		private bool on_draw (Widget widget, Context cr) {
		
			Allocation size;
			widget.get_allocation (out size);
			
			// Draw left border
			cr.move_to (0.5, size.height); // start in bottom left. 0.5 accounts for cairo's default stroke offset of 1/2 pixels
			cr.line_to (0.5, 0.5); // move to upper left corner
			
			cr.set_source_rgba (0.0, 0.0, 0.0, 0.25);
			cr.set_line_width (1.0);
			cr.set_antialias (Antialias.NONE);
			cr.stroke ();
			
			return false;
		}
		
	}

}

