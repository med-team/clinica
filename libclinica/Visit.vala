/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Tommaso Bianucci <churli@gmail.com>
 */
 
 
using Jansson;
 
namespace Clinica {

    public class Visit : Object {
   
        public DataProvider? provider = null;
        
        public int64 id { get; set; default = 0; }
    
        public string anamnesis { get; set; default = ""; }
            
        public string physical_examination { get; set; default = ""; }
            
        public string laboratory_exam { get; set; default = ""; }
            
        public string histopathology { get; set; default = ""; }
            
        public string diagnosis { get; set; default = ""; }
            
        public string topical_therapy { get; set; default = ""; }
            
        public string systemic_therapy { get; set; default = ""; }
            
        public string subsequent_checks { get; set; default = ""; }
            
        public DateTime date { get; set; }
            
        public Patient patient { get; set; default = null; }
        
        /**
         * @brief Get a Visit from a JSON coded object.
         */
        internal Visit.from_json (Json o, DataProvider? source_provider = null) {
            
            provider = source_provider;
            id = o.object_get ("id").integer_value ();
            anamnesis = o.object_get("anamnesis").string_value ();
            physical_examination = o.object_get ("physical_examination").string_value ();
            laboratory_exam = o.object_get ("laboratory_exam").string_value ();
            histopathology = o.object_get ("histopathology").string_value ();
            diagnosis = o.object_get ("diagnosis").string_value ();
            topical_therapy = o.object_get ("topical_therapy").string_value ();
            systemic_therapy = o.object_get ("systemic_therapy").string_value ();
            subsequent_checks = o.object_get ("subsequent_checks").string_value ();
            date = SqliteDataProvider.string_to_datetime (
                o.object_get ("date").string_value ());
            
            if (provider != null) {
                var patient_id = o.object_get ("patient").integer_value ();
                if (patient_id != 0) {
                    patient = provider.get_patient (patient_id);
                }
            }
        }
        
        internal Json to_json () {
            var object = Json.object ();
            
            object.object_set ("id", Json.integer (id));
            object.object_set ("anamnesis", Json.string (anamnesis));
            object.object_set ("physical_examination", Json.string (physical_examination));
            object.object_set ("laboratory_exam", Json.string (laboratory_exam));
            object.object_set ("histopathology", Json.string (histopathology));
            object.object_set ("diagnosis", Json.string (diagnosis));
            object.object_set ("topical_therapy", Json.string (topical_therapy));
            object.object_set ("systemic_therapy", Json.string (systemic_therapy));
            object.object_set ("subsequent_checks", Json.string (subsequent_checks));
            object.object_set ("date", Json.string (
                SqliteDataProvider.datetime_to_string (date)));
            if (patient != null)
                object.object_set ("patient", Json.integer (patient.id));
            else
                object.object_set ("patient", Json.integer (0));
                
            return object;
        }
    
    }

}
