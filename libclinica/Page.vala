/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    /**
     * @brief Page is an interface inherited by all the pages
     * that will be put in the main widget. 
     *
     * Some common methods are provided here and inheritance
     * from Gtk.Alignment is enforced to make sure that the widget
     * is add-able in the main-align. 
     */
    public interface Page : Alignment {
    
        /**
         * @brief Return the title to be displayed in the Clinica
         * window when this page is loaded. 
         */
        public abstract string get_title ();
        
        /**
         * @brief Get a SidebarEntry to be displayed when this page is
         * loaded.
         *
         * Return null if you do not want a SidebarEntry for this
         * page (this is the default implementation). 
         */
        public virtual SidebarEntry? get_sidebar_entry () {
            return null;
        }
        
        /**
         * @brief Return the Parent entry of the Sidebar entry 
         * obtained from the get_sidebar_entry () method.
         *
         * The new sidebar will be attached below its parent, 
         * or on top-level if null is returned (this is the default
         * implementation). 
         */
        public virtual SidebarEntry? get_parent_entry () {
            return null;
        }
        
        public abstract ResourceManager resource_manager { get; set; }
    }

}
