/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 
 namespace Clinica {
 
    public class VisitSchedulerDialog : Gtk.Dialog {
    
        public enum Response {
            CANCEL,
            SAVE,
        }
    
        /**
         * @brief ResourceManager associated with this clinica instance.
         */
        private ResourceManager resource_manager;
        
        /**
         * @brief Builder used to get the content area.
         */
        private Clinica.Builder builder;
    
        public VisitSchedulerDialog (ResourceManager resources, Patient? patient) {
            /* Setup the data passed as argument */
            this.resource_manager = resources;
            
            /* Add Save and Cancel buttons */
            add_buttons (Stock.CANCEL, Response.CANCEL, 
                         Stock.SAVE,   Response.SAVE);
                         
            /* Set up the container */
            builder = new Clinica.Builder.with_filename (resource_manager, "visit_scheduler.glade");
            builder.load_into_dialog (this);
            
            /* Set up a title and connect a callback to change it when the user select another patient */
            var p_entry = builder.get_object ("patient_entry") as PatientEntry;
            p_entry.selection_changed.connect (on_patient_entry_changed);
            if (patient != null)
                p_entry.select_patient (patient);
            else {
                set_title (_("Patient not selected"));
                set_response_sensitive (Response.SAVE, false);
            }
        }
        
        public void select_date (DateTime date) {
            var date_picker = builder.get_object ("date_picker") as DateTimePicker;
            date_picker.set_date (date);
        }
        
        private void on_patient_entry_changed (PatientEntry entry) {    
            if (entry.is_valid ()) {
                set_title (_("Schedule a vist for the patient %s").printf (entry.get_patient ().get_complete_name ()));
                set_response_sensitive (Response.SAVE, true);
            }
            else {
                set_title (_("Patient not selected"));
                set_response_sensitive (Response.SAVE, false);
            }
        }
        
        /**
         * @brief Callback called when the user clicks SAVE or CANCEL
         */
        public new Response run () {
            /* Get the widgets from the builder */
            PatientEntry p_entry = builder.get_object ("patient_entry") as PatientEntry;
            var picker = builder.get_object ("date_picker") as DateTimePicker;
        
            if (base.run () == Response.SAVE) {
                /* Saving the new visit in the database */                
                Visit visit = new Visit ();
                visit.patient = p_entry.get_patient ();
                visit.date = picker.get_datetime ();
                
                /* And finally save the visit in the database */
                resource_manager.data_provider.save_visit (visit);
            }
            
            return Response.CANCEL;
        }
    
    }
    
    
 }
