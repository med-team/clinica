/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Soup;

namespace Clinica {

    public class AuthDomain : AuthDomainBasic {
    
        private ResourceManager resource_manager { get; private set; }
    
        public AuthDomain (ResourceManager resources) {
            Object (realm: "Clinica RPC");
            resource_manager = resources;
            
            // We need to authenticate the RPC part of the server, 
            // while the rest should be publicy available. 
            add_path ("/rpc");
            
            // Set the authentication method
            set ("auth_callback", perform_authentication);
        }
        
        public bool perform_authentication (Soup.Message msg, string username, string password) {
            return (
                username == resource_manager.settings.get_string ("data-server-username") &&
                Checksum.compute_for_string (ChecksumType.SHA1, password) == 
                    resource_manager.settings.get_string ("data-server-password"));
        }
    
    }

}
