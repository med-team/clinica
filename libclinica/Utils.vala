/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica.Utils {

	/**
	 * @brief Returned the capitalized version of input
	 */
	public static string capitalize (string input) {
	    string [] pieces = input.split(" ");
    	string [] new_pieces = {};
    	foreach (string str in pieces) {
    		if (str.length > 1)
				new_pieces += str[0].toupper().to_string() + str[1:str.length];
			else
				new_pieces += str[0].toupper ().to_string ();
    	}
    	return string.joinv(" ", new_pieces);
	}
	
	/* 
	 * @brief Set width and height compatible to the screen 
	 */
	public static void set_widget_size (Widget widget, int width, int height) {
		Gdk.Screen screen = Gdk.Screen.get_default ();
		if (width > screen.get_width ()) {
			width = screen.get_width () - 100;
		}
		if (height > screen.get_height ()) {
			height = screen.get_height () - 100;
		}
		widget.set_size_request (width, height);
	}
	
	/**
	 * @brief Set the alert state of a widget.
	 *
	 * It paints the background of a reddish color to make the
	 * user understand that the content of the widget is wrong.
	 */
	public static void set_alert_state (Gtk.Widget entry, bool alert) {
        var alert_color = Gdk.RGBA ();
        if (alert)
        {
            alert_color.parse ("#dd4444");
        }
        else
            alert_color.parse ("#000000");
            
        entry.override_color (Gtk.StateFlags.NORMAL, alert_color);
	}
	
	
    /**
     * @brief Show error message to the user
     */
    public static void show_error_message (GLib.Object? source, string message) {
		
		/* Print the error message to stdout */    	        
		print(_("\033[31;1mERROR\033[0m => ") + message + "\n");
        
        var md = new MessageDialog (null, DialogFlags.MODAL, 
                        MessageType.ERROR, ButtonsType.OK,
                        "%s", _("Clinica encountered an error"));
        md.set_title (_("Clinica encountered an error"));
        md.format_secondary_markup (message);
        md.run ();
        md.destroy ();
    }
    
    /**
     * @brief Check patient's birthday
     */
    public bool check_date (int day, int month, int year) {
    	    	
	    int[] thirtyone = {1, 3, 5, 7, 8, 10, 12}; // months of 31 day
	   	       	
	    if ( ((day == 31) && (month in thirtyone)) || ((day <= 30) && (month != 2)) || (day <= 28) )
	    	return true; 
	    else {
	    	if ( (day == 29) && (month == 2) ) {
	    		// check if year is a leap year
	    		if ( ((year % 100 != 0) && (year % 4 == 0)) || (year % 400 == 0) ) 
	    			return true;	    		
	    	}
	    	return false;
	    }
    }
    
    public Gdk.Pixbuf pixbuf_from_svg_resource (string resource, int width, int height) {
        try {
            var stream = GLib.resources_open_stream (resource, ResourceLookupFlags.NONE);
            uint32 flags;
            size_t count;
            GLib.resources_get_info (resource, ResourceLookupFlags.NONE, out count, out flags);
            uint8[] buffer = new uint8[count];
            stream.read_all (buffer, out count);
            stream.close ();
            var handle = new Rsvg.Handle.from_data (buffer);
            var pixbuf = handle.get_pixbuf ();
            return pixbuf.scale_simple (width, height, Gdk.InterpType.BILINEAR);
        } catch (GLib.Error e) {
            return new Gdk.Pixbuf (Gdk.Colorspace.RGB, false, 16, 0, 0);
        }
    }

}
