/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    /**
     * @brief Customized TreeView that displays the list of the Patients
     * in the database. 
     */
    public class PatientListView : TreeView {
 
 		/**
 		 * @brief PatientListStore associated with the treeview
 		 * that is statically stored in the ResourceManager
 		 */
        private unowned PatientListStore store;
        private PatientFilteredListStore filtered_store;
		private TreeModelSort sortable_model;
        
        private CellRendererText name_cell_renderer;
        private CellRendererText surname_cell_renderer;
        
        public signal void error (string message);
        
		private ResourceManager resource_manager;
    
        /**
         * @brief Create a new PatientListPage
         */
        public PatientListView (ResourceManager resources, Entry find_entry) {
            resource_manager = resources;
        	
        	error.connect((t,l) => resource_manager.error_callback (t,l));
            
            /* Create a PatientListStore and filter with the find_entry */
            store = resource_manager.patient_list_store;
            filtered_store = new PatientFilteredListStore (resource_manager, find_entry);
            
            /* Make it sortable to allow further sorting by the user */
            sortable_model = new TreeModelSort.with_model (filtered_store);
            set_model (sortable_model);
            
            /*  Create name cell renderer and make it editable */
            name_cell_renderer = new CellRendererText ();
            name_cell_renderer.width = 240;
            /* name_cell_renderer.editable = true; */
            name_cell_renderer.edited.connect ((t,l,u) => on_name_cell_edited (t,l,u));
            
            /*  Create surname cell renderer and make even this editable */
            surname_cell_renderer = new CellRendererText ();
            surname_cell_renderer.width = 240;
            /* surname_cell_renderer.editable = true; */
            surname_cell_renderer.edited.connect ((t,l,u) => on_surname_cell_edited (t,l,u));
            
            /*  Insert column in the treeview */
            TreeViewColumn column;
            column = new TreeViewColumn.with_attributes (_("Name"), name_cell_renderer, "text", 
            					PatientListStore.Field.GIVEN_NAME);
            column.set_sort_column_id (PatientListStore.Field.GIVEN_NAME);
            append_column (column);
            column = new TreeViewColumn.with_attributes (_("Surname"), surname_cell_renderer, "text", 
            					PatientListStore.Field.SURNAME);
            column.set_sort_column_id (PatientListStore.Field.SURNAME);
            append_column (column);
            
            /* Connect right click callback */
            button_press_event.connect(on_button_press_event);
            
            /* Connect the row-activated callback */
            row_activated.connect (on_row_activated);
            
            /* Connect the popup menu signal */
            popup_menu.connect (on_popup_menu);
            
            /* Some styling, row in alternating colors */                
            set_rules_hint (true);
            
            /* Drag and drop support */
            TargetEntry patient_target = { "PATIENT", 0, 1 };
            enable_model_drag_source (Gdk.ModifierType.BUTTON1_MASK, { patient_target }, Gdk.DragAction.COPY);
            drag_begin.connect ((context) => {
                resource_manager.dragging_patient = get_selected_patient ();
            });
        }
        
        /**
         * @brief Callback called when the user activates a row, for example double
         * clicking or pressing enter on it.
         */
        private void on_row_activated (TreePath path, TreeViewColumn column) {
            TreeIter iter;
            filtered_store.get_iter (out iter, path);
                
            Value patient;
            TreeIter child_iter;
            filtered_store.convert_iter_to_child_iter (out child_iter, iter);
            store.get_value (child_iter, PatientListStore.Field.PATIENT, out patient);
            
            resource_manager.user_interface.show_visit_window (patient as Patient);
        }
        
        private void on_name_cell_edited (CellRenderer renderer, string path, string new_text) {
            GLib.Value value;
            Patient patient;
            TreeIter iter;
            TreeIter child_iter;
            
            /*  Get modified iter from the store to retrieve Patient object */
            filtered_store.get_iter_from_string(out iter, path);
            filtered_store.convert_iter_to_child_iter (out child_iter, iter);
            store.get_value(child_iter, PatientListStore.Field.PATIENT, out value);
            
            /*  Cast patient and update database data */
            patient = value as Patient;
            patient.given_name = new_text;
            resource_manager.data_provider.save_patient (patient);
        }
        
        private void on_surname_cell_edited (CellRenderer renderer, string path, string new_text) {
            GLib.Value value;
            Patient patient;
            TreeIter iter;
            TreeIter child_iter;
            
            /*  Get value from the store */
            filtered_store.get_iter_from_string (out iter, path);
            filtered_store.convert_iter_to_child_iter (out child_iter, iter);
           	store.get_value (child_iter, PatientListStore.Field.PATIENT, out value);
            
            /*  Set new data in the Patient, save and update treeview */
            patient = value as Patient;
            patient.surname = new_text;
            resource_manager.data_provider.save_patient (patient);
        }
        
        /**
         * @brief Remove the patient selected in the PatientListView
         * or do nothing if there is no patient selected.
         */
        public void remove_selected_patient () {
    		TreeSelection selection = get_selection ();
        	TreeIter iter;
        	TreeModel model;
        	
        	if (!selection.get_selected(out model, out iter)) {
        		error (_("Select a patient to delete it!"));
        	}
        	else {
        		var question = new MessageDialog (resource_manager.user_interface.window, 
        				DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT, 
        				MessageType.QUESTION, ButtonsType.YES_NO,
        				"%s", 
        				_("Really delete this patient? All information about him/her and the associated visits will be lost."));
        	    question.set_transient_for (resource_manager.user_interface.window);
        		if (question.run () != ResponseType.YES) {
        			question.destroy ();
        			return;
        		}
        		question.destroy ();
        	
        		TreeIter it;
				sortable_model.convert_iter_to_child_iter (out it, iter);
        		filtered_store.convert_iter_to_child_iter (out iter, it);
        		
		    	/* Delete patient from the database */
		    	Value value;
		    	store.get_value (iter, PatientListStore.Field.PATIENT, out value);
		    	
		    	/* Removing the visits associated with this patient */
		    	foreach (Visit visit in (value as Patient).visits ()) {
		    	    resource_manager.data_provider.remove_visit (visit);
		    	}
		    	
		    	resource_manager.data_provider.remove_patient (value as Patient);
		    }
    	}
    	
    	private bool on_button_press_event (Widget widget, Gdk.EventButton event) {
    		/* Check if this is a right click */
			if (event.type == Gdk.EventType.BUTTON_PRESS && event.button == 3) {
	    		do_popup_menu (this, event);
	    		return true;
	    	}
	    	
	    	return false;
    	}
    	
    	/**
    	 * @brief Callback called when the user press the "Popup a menu" button
    	 * on its keyboard.
    	 */
    	private bool on_popup_menu (Widget widget) {
    	    do_popup_menu (widget);
    	    return false;
    	}
    	
    	/**
    	 * @brief Popup a menu on user cursor
    	 *
    	 * @param event The event that has generated this popup.
    	 */
    	private void do_popup_menu (Widget widget, Gdk.EventButton? event = null) {
			/* Get selection and set it on the selected item */
			TreeSelection selection = get_selection ();
			TreePath path;
			if (event != null) {
    			get_path_at_pos ((int) event.x, (int) event.y, out path, null, null, null);
    			selection.select_path (path);
    	    }
			
			/* Create the menu and show it */
			var patient = get_selected_patient ();
			var menu = new PatientContextMenu (resource_manager, this, patient);
			
			/* Show! */
			menu.attach_to_widget(widget, null);
			menu.popup(null, null, null, (event != null) ? event.button : 0, 
			           (event != null) ? event.time : Gtk.get_current_event_time ());
    	}
    	
    	/**
    	 * @brief Get the selected patient in the PatientListView
    	 * or null if there is no selected patient.
    	 *
    	 * @return the selected patient, or null. 
    	 */
    	public Patient? get_selected_patient () {
    		TreeSelection selection = get_selection ();
    		TreeIter iter;
    		TreeModel model;
    		
    		if (!selection.get_selected(out model, out iter)) {
    			return null;
    		}
    		else {
    			Value value;
    			model.get_value(iter, 0, out value);
    			var p = value as Patient;
    			return p;
    		}
    	}
    }

}
