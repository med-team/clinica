/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
 namespace Clinica {
 
     public interface CoreActivatable : Object {
        
        /** 
         * @brief The ResourceManager used to interact with clinica.
         */
        public abstract ResourceManager resource_manager { get; set; }

        /**
         * @brief Activate method that will be called when the extension
         * is loaded. Special hooking up to callbacks and similar can be
         * done here (registering as data_provider, for example).
         */        
        public abstract void activate ();
        
        /**
         * @brief Deactivation method. Here the extension should unplug itself
         * from the Clinica application.
         */
        public abstract void deactivate ();
        
        public abstract void update_state ();
        
     }
     
}
