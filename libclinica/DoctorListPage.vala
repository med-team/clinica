/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    public class DoctorListPage : Alignment, Page {
        
        private DoctorListView view;
        private VBox left_vbox;
        private ScrolledWindow scrolled_window;
        
        public ResourceManager resource_manager { get; set; }
        
        private SidebarEntry sidebar_entry;
        
        /**
         * @brief Page with the list of doctors. 
         */
        public class DoctorListPage (ResourceManager resources) {
            resource_manager = resources;
            
            var builder = new Builder (resources);
            
            try {
                builder.add_from_resource ("doctor_list_toolbar.glade");
            } catch (Error e) {
                error ("Error loading doctor_list_toolbar.glade. Check your installation.");
            }
            
            left_vbox = builder.get_object ("left_vbox") as VBox;
            scrolled_window = builder.get_object ("treeview_scrolled_window") as ScrolledWindow;
            
            /* Get find_entry to filter the doctors */
            var find_entry = builder.get_object ("find_entry") as Entry;
            view = new DoctorListView (resource_manager, find_entry);
            scrolled_window.add (view);
            
            add (left_vbox);
            builder.connect_signals (this);
            
            sidebar_entry = new SidebarPageEntry (resource_manager, this, _("Doctors"),
                Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/doctors.svg", 
                Sidebar.ICON_SIZE, Sidebar.ICON_SIZE));
                
            var toolbar = new Clinica.Toolbar (resource_manager);
            populate_toolbar (toolbar);
           
            var box = builder.get_object ("top_box") as Gtk.Box;
            box.pack_start (toolbar, false, true);
            show_all ();
        }
        
        private void populate_toolbar (Toolbar toolbar) {
            var add_item = new ToolbarItem (resource_manager, 
                Config.RESOURCE_BASE + "ui/icons/small_plus.png");
            add_item.activated.connect (on_add_item_activated);
            add_item.set_tooltip_text (_("Add a doctor"));
            toolbar.insert (add_item, -1);
            
            var edit_item = new ToolbarItem (resource_manager,
                Config.RESOURCE_BASE + "ui/icons/small_pencil.png");
            edit_item.activated.connect (on_modify_item_activated);
            edit_item.set_tooltip_text (_("Edit the selected doctor"));
            toolbar.insert (edit_item, -1);
            
            var remove_item = new ToolbarItem (resource_manager,
                Config.RESOURCE_BASE + "ui/icons/small_remove_2.png");
            remove_item.activated.connect (on_remove_item_activated);
            remove_item.set_tooltip_text (_("Remove the selected doctor"));
            toolbar.insert (remove_item, -1);
        }
        
        public string get_title () {
            return _("Doctors");
        }
        
        public SidebarEntry? get_sidebar_entry () {
            return sidebar_entry;
        }
        
        private void on_add_item_activated (ToolbarItem item) {
        	var new_doc_dialog = new DoctorEditor (resource_manager);
        	new_doc_dialog.set_transient_for (resource_manager.user_interface.window);
        	new_doc_dialog.run ();
			new_doc_dialog.destroy ();
        }
        
        private void on_modify_item_activated (ToolbarItem item) {
        	Doctor? doc = view.get_selected_doctor ();
        	if (doc == null)
        		return;
        	var edit_doc_dialog = new DoctorEditor.with_doctor (resource_manager, doc);
        	edit_doc_dialog.set_transient_for (resource_manager.user_interface.window);
        	edit_doc_dialog.run ();
        	edit_doc_dialog.destroy ();
        }
        
        /**
         * @brief Callback called when a user try to delete a doctor
         * clicking on the remove button. It handles removal asking
         * user if associated patients should be deleted. 
         */
        public void on_remove_item_activated (ToolbarItem item) {
 			view.remove_selected_doctor ();
        }
    }
    
}
