/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    public class PatientListStore : ListStore {
    
    	enum Field {
    		PATIENT,
    		GIVEN_NAME,
    		SURNAME,
    		COMPLETE_NAME,
    	}
    
        public signal void error (string message);
        
		private ResourceManager resource_manager;
		
		private DataProvider provider;
		
		private ulong patient_added_id = 0;
		private ulong patient_changed_id = 0;
		private ulong patient_removed_id = 0;
        
        /**
         * @brief Create a new PatientListStore starting
         * from the passed ResourceManager.
         */
        public PatientListStore (ResourceManager resources) {
            resource_manager = resources;
            /* Keep a reference to the resource manager and connect
             * callback error */
            error.connect ((t,l) => resources.error_callback(t,l));
        
            /* Init new list, with type Patient, name, surname and
             * Complete name */
            Type [] column_headers = { typeof (Patient) , typeof (string), 
                                       typeof (string) , typeof (string)};
            set_column_types (column_headers);
            
            load_data ();
        }
        
        public void load_data () {            
            /* Asynchronous loading of patients */
            Idle.add(() => {
		        foreach (Patient p in resource_manager.data_provider.patients ()) {
		        	add_patient (p.id);
		        }
		        
		        provider = resource_manager.data_provider;
		        update_signals ();
		        
		        resource_manager.notify["data_provider"].connect ((provider) => update_signals ());
		        
		        /* We don't need to reiterate this function any more */
		        return false;
		    });        
        }
        
        public void update_signals () {
            /* Remove old callbacks if present */
            if (patient_added_id > 0)
                provider.disconnect (patient_added_id);
            if (patient_changed_id > 0)
                provider.disconnect (patient_changed_id);
            if (patient_removed_id > 0)
                provider.disconnect (patient_changed_id);
                
            provider = resource_manager.data_provider;
            
	        /* Setup callbacks to add or remove patient */
	        resource_manager.data_provider.patient_added.connect (
	            (id) => add_patient (id));
	        resource_manager.data_provider.patient_changed.connect (
	            (id) => reload_patient (id));
	        resource_manager.data_provider.patient_removed.connect (
	            (id) => remove_patient (id));
        }
        
        public void remove_patient (int64 id) {
            TreeIter iter = id_to_iter (id);
            remove (iter);
        }
        
        /**
         * @brief Append the given patient to the liststore
         */
        private void add_patient (int64 id) {
            Patient p = resource_manager.data_provider.get_patient (id);
        	TreeIter it;
            append (out it);
            set_value (it, Field.PATIENT,    p);    
            set_value (it, Field.GIVEN_NAME, p.given_name);
            set_value (it, Field.SURNAME,    p.surname);
            set_value (it, Field.COMPLETE_NAME, p.get_complete_name ());
	    }
	    
	    /**
	     * @brief Reload information about patient p that
	     * should be already present in the database
	     */
	    private void reload_patient (int64 id) {
	        Patient p = resource_manager.data_provider.get_patient (id);
	    	TreeIter iter;
	    	Value patient;
	    	
	    	if (!get_iter_first (out iter)) {
				error(_("Patients database seems corrupted. This is likely to be a bug in the application"));
	    	}
	    	
	    	do {
	    		get_value (iter, Field.PATIENT, out patient);
	    		if ((patient as Patient).id == p.id) {
	    			set_value(iter, Field.PATIENT, p);
	    			set_value(iter, Field.GIVEN_NAME, p.given_name);
	    			set_value(iter, Field.SURNAME, p.surname);
	    			set_value(iter, Field.COMPLETE_NAME, p.get_complete_name ());
	    			return;
	    		}
	    	} while (iter_next (ref iter));
	    	
	    	// We hope to find the patient :)
	    	assert_not_reached ();
	    }
	    
	    private TreeIter id_to_iter (int64 id) {
	    	TreeIter it;
	    	Value patient;
	    	if (!get_iter_first (out it))
				error(_("Patients database seems corrupted. This is likely to be a bug in the application"));
	    	do {
	    		get_value (it, Field.PATIENT, out patient);
	    		if ((patient as Patient).id == id) {
	    			return it;
	    		}
	    	} while (iter_next (ref it));
	    	
	    	assert_not_reached ();
	    }
	}
}
