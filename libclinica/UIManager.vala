/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

namespace Clinica {

    public class UIManager : Gtk.UIManager {
    
        private Gtk.ActionGroup action_group;
        
        private ResourceManager resource_manager;
        
        public UIManager (ResourceManager resources) {
            resource_manager = resources;
        
            // Load the XML definining the UI from XML in the GResource bundle
            try {
                add_ui_from_resource (Config.RESOURCE_BASE + "ui.xml");
                action_group = new Gtk.ActionGroup ("MainMenu");
            } catch (GLib.Error e) {
                error ("Error loading UI files: %s".printf (e.message));
            }
            
            populate ();
        }
        
        private void populate () {
            // File menu
            action_group.add_action (new Gtk.Action ("FileAction", _("_File"), null, null));
            action_group.add_action_with_accel (new Gtk.Action ("NewPatientAction", _("New _patient"), 
                _("Create a new patient"), null), "<control>N");
            action_group.add_action_with_accel (new Gtk.Action ("NewDoctorAction", _("New _doctor"),
                _("Create a new doctor"), null), "<control>D");
            action_group.add_action_with_accel (new Gtk.Action ("QuitAction", _("_Quit"), 
                _("Quit clinica"), null), "<control>Q");
                
            // View menu
            action_group.add_action (new Gtk.Action ("ViewAction", _("_View"), null, null));
            action_group.add_action_with_accel (new Gtk.Action ("StartPageAction", _("_Start page"),
                _("Go to the start page of clinica"), null), "Home");
            action_group.add_action_with_accel (new Gtk.Action ("PatientsAction", _("_Patients"), 
                _("Go to the list of patients"), null), "<control><alt>P");
            action_group.add_action_with_accel (new Gtk.Action ("DoctorsAction", _("_Doctors"), 
                _("Go to the list of doctors"), null), "<control><alt>D");
            action_group.add_action_with_accel (new Gtk.Action ("SearchMedicinesAction", _("_Search medicines"), 
                _("Search medicines online"), null), "<control><alt>M");
                
            // Tools menu
            action_group.add_action (new Gtk.Action ("ToolsAction", _("_Tools"), null, null));
            action_group.add_action_with_accel (new Gtk.Action ("SettingsAction", _("_Settings"), 
                _("Customize clinica behaviour"), null), "<control>S");
            action_group.add_action (new Gtk.Action ("BackupAction", _("Backup"),
                _("Backup clinica data to file"), null));
            action_group.add_action (new Gtk.Action ("ImportDataAction", _("Import"),
                _("Import data from backup files"), null));
            action_group.add_action (new Gtk.Action ("AddMedicinesAction", _("Add medicine"), 
                _("Add a new medicine to the local database"), null));
            action_group.add_action (new Gtk.Action ("BrowseMedicinesAction", _("Browse local medicines"), 
                _("Browse the medicine saved locally"), null));
                
            // Help menu
            action_group.add_action (new Gtk.Action ("HelpAction", _("_Help"), null, null));
            action_group.add_action_with_accel (new Gtk.Action ("ContentsAction", _("_Contents"), 
                _("Open the help system"), null), "F1");
            action_group.add_action (new Gtk.Action ("ReportABugAction", _("_Report a bug"), 
                _("Report a bug online"), null));
            action_group.add_action (new Gtk.Action ("AboutAction", _("_About"), 
                _("Get to know the development team of Clinica"), null));
                
            insert_action_group (action_group, 0);
        }
    
    }

}
