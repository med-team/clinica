/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
 using Gtk;
 
 namespace Clinica {
 
    /**
     * @brief Clinica version of the GtkBuilder that handles all sort 
     * of setup needed by Clinica* objects.
     */
    public class Builder : Gtk.Builder {
    
        private ResourceManager resource_manager;
        
        public signal void error (string message);
        
        public Builder (ResourceManager resources) {
            resource_manager = resources;
            error.connect ((t,l) => resource_manager.error_callback (t,l));
        }
        
        public Builder.with_filename (ResourceManager resources, string filename) {
            this (resources);
            
            try {
                add_from_resource (filename);
            } catch (GLib.Error e) {
                error (_("Failed to load UI file: %s. Please check your installation.\n%s").printf (filename, e.message));
            }
        }
        
        public new void add_from_resource (string filename) throws GLib.Error {
            base.add_from_resource (Config.RESOURCE_BASE + "ui/" + filename);
            
            /* Sort of a brute heuristic, but I think it works */
            foreach (Object obj in get_objects ()) {
                Type type = obj.get_type ();
                if ("Clinica" in type.name ()) {
                    debug ("Calling setup on %s".printf (type.name ()));
                    (obj as Clinica.Buildable).setup (resource_manager);
                }
            }
        }
        
        public void load_into_window (Window window) {
            window.add (get_object ("content_area") as Widget);
        }
        
        /**
         * @brief Load the Widget named content_area into the given Dialog.
         */
        public void load_into_dialog (Dialog dialog) {
            (dialog.get_content_area ()).add (get_object ("content_area") as Widget);
        }
    }
 
 }
