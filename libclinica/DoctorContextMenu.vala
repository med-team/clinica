/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

	public class DoctorContextMenu : Gtk.Menu {
	
		/**
		 * @brief The doctor associated to this popup menu 
		 */
		private Doctor doctor;
		
		/**
		 * @brief DoctorListView where the menu will 
		 * be popupped.
		 */
		private DoctorListView view;
		
		/**
		 * @brief Gtk.MenuItem associated to doctor deletion
		 */
		private Gtk.MenuItem delete_menuitem;
		
		/**
		 * @brief Gtk.MenuItem that triggers doctor edit
		 * dialog.
		 */
		private Gtk.MenuItem edit_menuitem;
		
		private ResourceManager resource_manager;
	
		public DoctorContextMenu (ResourceManager resources, DoctorListView v, Doctor d) {
		    resource_manager = resources;
			doctor = d;
			view = v;
			
			/* Create menu items, connect them to their callback
			 * and add it to the menu */
			delete_menuitem = new Gtk.MenuItem.with_label (_("Delete"));
			edit_menuitem   = new Gtk.MenuItem.with_label (_("Edit"));
			
			delete_menuitem.activate.connect (on_delete_menuitem_activated);
			edit_menuitem.activate.connect (on_edit_menuitem_activated);
			
			append (edit_menuitem);
			append (delete_menuitem);
			
			show_all ();
		}
		
		private void on_delete_menuitem_activated (Gtk.MenuItem item) {
			view.remove_selected_doctor ();
		}
		
		private void on_edit_menuitem_activated (Gtk.MenuItem item) {
			var doctor_editor = new DoctorEditor.with_doctor (resource_manager, doctor);
			doctor_editor.set_transient_for (resource_manager.user_interface.window);
			doctor_editor.run ();
			doctor_editor.destroy ();
		}
	}

}
