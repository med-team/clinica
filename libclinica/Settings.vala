/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;

namespace Clinica {

    private bool has_gsettings = false;
    
    public class Settings : GLib.Settings {
    
        private ResourceManager resource_manager { get; set; }
        
        /**
         * @brief A pointer to the selected search engine for the medicines,
         * or null if not search engine is available nor selected.
         */
        internal unowned List<MedicineSearchEngine> selected_search_engines;
        
        public Settings (ResourceManager resources) {
            string? my_schema = null;
            
            /* Check if clinica's schema is insatlled system-wide and, 
             * if it's found, use it. */
            has_gsettings = true;
            my_schema = "org.phcteam.clinica";
            
            GLib.Object (
                schema: my_schema
            );
            
            resource_manager = resources;
            
            /* Select the right search engines according to user preferences */
            var se_names = get_strv_safe ("medicine-search-engines");
            foreach (var se_name in se_names) {
                foreach (var engine in resource_manager.medicine_search_engines) {
                    if (engine.get_name () == se_name) {
                        selected_search_engines.append (engine);
                        break;
                    }
                }
            }
        }
        
        public unowned List<MedicineSearchEngine> get_medicine_search_engines () {
            return selected_search_engines;
        }
        
        /**
         * @brief Return a list of string with the name of the active
         * plugins as modules.
         */
        public string [] get_active_plugins () {
            return get_strv_safe ("active-plugins");
        }
        
        /**
         * @brief Check if the plugin with the given name has been activated.
         */
        public bool is_plugin_active (string plugin_name) {
            if (schema != null)
            {
                string [] active_plugins = get_active_plugins ();
                return (plugin_name in active_plugins);
            }
            else
                return false;
        }
        
        private string [] get_strv_safe (string key) {
            string [] results = {};
            foreach (var res in get_strv (key)) {
                results += res;
            }
            
            return results;
        }
        
        public void activate_medicine_search_engine (string name) {
            if (has_gsettings)
                debug ("Activating search engine: %s", name);
                if (! (name in get_strv_safe ("medicine-search-engines"))) {
                    string [] active_engines = get_strv_safe ("medicine-search-engines");
                    active_engines += name;
                    set_value ("medicine-search-engines", active_engines);
                }
        }
        
        public void deactivate_medicine_search_engine (string name) {
            debug ("Deactivating search engine: %s", name);
            if (has_gsettings && name in get_strv_safe ("medicine-search-engines")) 
            {
                string [] active_engines = get_strv_safe ("medicine-search-engines");
                string [] new_active_engines = {};
                foreach (string pl in active_engines) {
                    if (pl != name)
                        new_active_engines += pl;
                }
                set_value ("medicine-search-engines", new_active_engines);
            }
        }
        
        public void activate_plugin (string module_name) {
            if (has_gsettings)
                if (! (module_name in get_active_plugins ())) {
                    string [] active_plugins = get_strv_safe ("active-plugins");
                    active_plugins += module_name;
                    set_value ("active-plugins", active_plugins);
                }
        }
        
        
        public void deactivate_plugin (string module_name) {
            string [] active_plugins = get_strv_safe ("active-plugins");
            if (has_gsettings && module_name in active_plugins) 
            {
                string [] new_active_plugins = {};
                foreach (string pl in active_plugins) {
                    if (pl != module_name)
                        new_active_plugins += pl;
                }
                set_value ("active-plugins", new_active_plugins);
            }
        }
    }
}
