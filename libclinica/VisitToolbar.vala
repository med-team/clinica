/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

	/**
	 * @brief A toolbar for the VisitWindow window
	 */
	public class VisitToolbar : Box {
	
		/* References to useful objects */
		private Patient patient;
		
		private Label patient_label;
		
		/* Action area */
		internal VisitActions visit_actions;
		
		/* Signals */
		public signal void save_visit ();
		public signal void save_visit_as_pdf ();
		public signal void print_visit ();
		public signal void delete_visit (); // set sensitive
		
		private ResourceManager resource_manager;
		private VisitPage       visit_page;
	
		public VisitToolbar (ResourceManager resources, VisitPage parent, Patient patient) {
			/* Set default properties */
			GLib.Object (orientation: Orientation.HORIZONTAL, spacing: 6);
		    	resource_manager = resources;
			this.patient = patient;
			visit_page = parent;
			
			/* Get image of the patient */
			var patient_image = new Gtk.Image.from_pixbuf (
			    Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/patient.svg", 48, 48));
			append (patient_image);
						
			/* A label to identify the patient, aligned to the left */
			patient_label = new Label (null);
			patient_label.set_alignment (0, 0.5F);
			pack_start (patient_label, true, true, resource_manager.PADDING);
			
			/* Create a new visit action and connect the but4ton for saving
			 * the visit and the one for changing the view mode */
			visit_actions = new VisitActions (resource_manager);
			visit_actions.delete_visit.connect ((va) => delete_visit ());
			visit_actions.save_visit.connect ((va) => save_visit ());
			visit_actions.save_as_pdf.connect ((va) => save_visit_as_pdf ());
			visit_actions.print_visit.connect ((va) => print_visit ());
			visit_actions.show_details.connect ((va) => on_show_details_button_clicked ());
			pack_start (visit_actions, false, true, resource_manager.PADDING);
			
			show_patient_details ();
		}
		
		private void on_show_details_button_clicked () {
            /*  Create the patient browser */
            var edit_patient_dialog = new PatientEditor.with_patient (resource_manager, patient);
            edit_patient_dialog.set_transient_for (resource_manager.user_interface.window);
            edit_patient_dialog.run ();
            edit_patient_dialog.destroy ();
        	
        	/* Since the patient could have been modified we need to reload its data
        	 * in the labels */
            show_patient_details ();
		}
		
		/**
		 * @brief Show patient details in the labels.
		 */
		private void show_patient_details () {		
			string doctor_name = _("This patient has not a doctor");
			if (patient.doctor != null)
			    doctor_name = patient.doctor.get_complete_name ();
			patient_label.set_markup ("%s: <b>%s</b>\n%s: <b>%s</b>".printf(_("Patient"), patient.get_complete_name (),
				_("Doctor"), doctor_name));

		}
		
		/**
		 * @brief Append a widget to the toolbar
		 */
		private void append (Widget widget) {
			pack_start (widget, false, false, 6);
		}
	}
	
}
