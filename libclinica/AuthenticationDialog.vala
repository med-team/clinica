/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Gtk;
 
namespace Clinica {

    public class AuthenticationDialog : Dialog {
    
        private ResourceManager resource_manager;
        
        private Builder builder;
    
        public AuthenticationDialog (ResourceManager resources) {
            resource_manager = resources;
            
            add_buttons (Stock.CANCEL, ResponseType.CANCEL,
                         Stock.OK, ResponseType.OK);
            set_default_response (Gtk.ResponseType.OK);
                         
            builder = new Builder.with_filename (resource_manager, "authentication_dialog.glade");
            builder.load_into_dialog (this);
            
            set_title (_("Authentication required"));
        }
        
        public string get_username () {
            var username_entry = builder.get_object ("username_entry") as Entry;
            return username_entry.get_text ();
        }
        
        public string get_password () {
            var password_entry = builder.get_object ("password_entry") as Entry;
            return password_entry.get_text ();
        }
    
    }

}
