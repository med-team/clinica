/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 *            Maxwell Barvian from Maya - http://launchpad.net/maya/
 */

using Gtk;
using Gdk;
using Cairo;
		
namespace Clinica {
				
	public class Day : Gtk.EventBox {
	
	    public signal void selected (DateTime date);
	
		/* private VBox vbox;
		private Label label; */
		private DrawingArea area;
		
		public DateTime date { get; set; }
		private CalendarView calendar_view { get; set; }
		private ResourceManager resource_manager { get; set; }
		
		private double highlight_level = 0;
		
		private GLib.List<Visit> visit_list;
		private GLib.List<Event> event_list;
	 
		public Day (CalendarView view) {
		    this.calendar_view = view;
		    this.resource_manager = view.resource_manager;
			
			area = new DrawingArea ();
			
			/* EventBox Properties */
			can_focus = true;
			set_visible_window (true);
			events |= EventMask.BUTTON_PRESS_MASK;
			
            get_style_context ().add_provider (calendar_view.style_context, 600);
			get_style_context ().add_class ("cell");
			set_size_request (90, 90);
			
			area.draw.connect (on_area_draw);
			add (area);
			
			/* Signals and handlers */
			button_press_event.connect (on_button_press);
			focus_in_event.connect (on_focus_in);
			focus_out_event.connect (on_focus_out);
			draw.connect (on_draw);
			
			notify["date"].connect (on_date_changed);
			
			/* Drag and drop support */
			TargetEntry patient_target = { "PATIENT", 0, 1 };
			drag_dest_set (this, DestDefaults.ALL | DestDefaults.HIGHLIGHT , 
			    { patient_target }, DragAction.COPY);
			drag_drop.connect (on_drag_drop);
			drag_motion.connect (on_drag_motion);
			drag_leave.connect ((context, time) => { highlight_level = 0.0; queue_draw (); });
		}
		
		private bool on_drag_drop (Widget widget, Gdk.DragContext context, int x, int y, uint time)
		{
            var patient = resource_manager.dragging_patient;
            if (patient == null) {
                debug ("Not doing drag and drop since resource_manager.dragging_patient is null");
                return false;
            }
            debug ("You dropped patient: %s on day %s", patient.get_complete_name (),
                date.format ("%F"));
		    drag_finish (context, true, false, time);
		    resource_manager.dragging_patient = null;
		    
		    // Create a VisitScheduler and schedule a new visit for the patient dropped here
		    var visit_scheduler = new VisitSchedulerDialog (resource_manager, patient);
		    visit_scheduler.set_transient_for (resource_manager.user_interface.calendar_window);
		    visit_scheduler.select_date (new DateTime.local (date.get_year (), date.get_month (), date.get_day_of_month (),
		        9, 30, 0));
		    visit_scheduler.run ();
		    visit_scheduler.destroy ();
		    return true;
		}
		
		private bool on_drag_motion (Widget widget, Gdk.DragContext context, int x, int y, uint time) {
		    int height = widget.get_allocated_height();
		    int width = widget.get_allocated_width ();
		    highlight_level = 16.0 * (1.0 * width - x) * x * y * (height - y) / (width*width) / (height * height);
		    queue_draw ();
		    return false;
		}
		
		private void on_date_changed () {
		    if (calendar_view.calendar == null)
		        return;
		    if (date.get_month () != calendar_view.calendar.handler.current_month)
		        can_focus = false;
		}
		
		private bool on_area_draw (Widget w, Context ctx) {
		    TextExtents extents;
		
		    /* Get allocation */
		    var height = w.get_allocated_height ();
		    var width  = w.get_allocated_width  ();
		    
		    /* Draw border around the cell */
		    ctx.set_line_width (0.1);
  		    ctx.set_source_rgb (0.04, 0.04, 0.07);
		    ctx.rectangle (0, 0, width, height);
		    ctx.stroke ();
		    
		    /* Draw dark background shadow if we are inactive */
		    if (!can_focus) {
		        ctx.set_source_rgba (0.1, 0.1, 0.1, 0.2);
		        ctx.rectangle (0, 0, width, height);
		        ctx.fill ();
		    } else {
		        ctx.set_source_rgba (0.1, 0.1, 0.1, 0.07);
		        ctx.rectangle (0, 0, width, height);
		        ctx.fill ();
		    }
		    
		    /* Draw the background if dragging over this, red if it is 
		     * saturday or sunday, blue otherwise! */
            if (highlight_level != 0 && can_focus) {
		        if (date.get_day_of_week () > 5) {
	                ctx.set_source_rgba (0.5 + 0.5 * highlight_level, 0, 0, 0.2 + 0.5 * highlight_level);
		        }
		        else {
		            ctx.set_source_rgba (0, 0,  0.5 * (1 + highlight_level), 0.2 + 0.5 * highlight_level);
		        }
                ctx.rectangle (0, 0, width, height);
                ctx.fill ();
            }
		    
		    /* If we have the focus draw the light :) */
		    if (has_focus) {
		        ctx.set_source_rgba (0.1 + 0.9 * highlight_level, 0.1, 1, 0.2);
		        ctx.rectangle (0, 0, width, height);
		        ctx.fill ();
		    }
		    
		    /* Draw another green layer if there are events on this day, we should
		     * also render events in the calendar */
		    var event_number = visit_list.length () + event_list.length ();
			if (can_focus && event_number > 0) {
			
			    /* Set the color of the day according to how much is busy */
			    if (event_number < 2)
    			    ctx.set_source_rgba (0.2, 1.0, 0.2, 0.3);
    			else if (event_number < 4)
    			    ctx.set_source_rgba (1.0, 0.55, 0.0, 0.3);
    			else
    			    ctx.set_source_rgba (1.0, 0.2 , 0.2, 0.3);
    			
			    ctx.rectangle (0, 0, width, height);
			    ctx.fill ();
			    
			    ctx.set_source_rgba (0.1, 0.1, 0.1, 0.7);
                ctx.set_font_size (10.0);
                double line_position = 20;
                double line_height = 13.0;
                Gee.LinkedList<int64?> displayed_patients_id = new Gee.LinkedList<int64?> ();
    	        ctx.rectangle (0, 0, width - 4, height - 4);
		        ctx.clip ();
			    
			    /* Drawing events with their scheduled time */
			    foreach (var event in event_list) {
			        /* Time of the event */
			        double h_space = 0.0;
			        ctx.set_source_rgba (0.1, 0.1, 0.1, 0.5);
			        string text = event.date.format ("%H:%M");
			        ctx.text_extents (text, out extents);
			        ctx.move_to (4 - extents.x_bearing, 
			            line_position + line_height / 2);
			        ctx.show_text (text);
			        h_space += extents.width + 3.0;
			        
			        /* Title of the event */
      			    ctx.set_source_rgba (0.1, 0.1, 0.1, 0.7);
			        text = event.title;
			        ctx.text_extents (text, out extents);
			        ctx.move_to (4 - extents.x_bearing + h_space,
			            line_position + line_height / 2);
			        line_position += line_height;
			        ctx.show_text (text);
			    }
			    
			    /* Drawing performed visits */
			    foreach (var visit in visit_list) {
			        if (visit.patient.id in displayed_patients_id)
			            continue;
			        displayed_patients_id.add (visit.patient.id);
			        string text = "Visit: " + visit.patient.get_complete_name ();;
			        ctx.text_extents (text, out extents);
			        ctx.move_to (4 - extents.x_bearing,
			            line_position + line_height / 2);
			        line_position += line_height;
			        ctx.show_text (text);
			    }
			}
		    
		    /* Draw number with the date with the usual switch on can_focus
		     * to render properly hidden date (of the past and future month). */
		    string date = date.get_day_of_month ().to_string ();
		    ctx.set_font_size (10.0);
		    if (can_focus)
    		    ctx.set_source_rgba (0.1, 0.1, 0.1, 0.7);
    		else
    		    ctx.set_source_rgba (0.1, 0.1, 0.1, 0.4);
		    ctx.text_extents (date, out extents);
		    ctx.move_to (width + extents.x_bearing - extents.width - 8,
		                 8 - extents.y_bearing);
		    ctx.show_text (date);
		    return true;
		}
		
		private bool on_button_press (EventButton event) {
			grab_focus ();
			return true;
		}
		
		private bool on_focus_in (EventFocus event) {
		    /* Call the selected signal to notify the calendarview
		     * that the day has been selected and related information
		     * need to be displayed */
		    selected (date);
		    area.queue_draw ();
			return false;
		}
		
		private bool on_focus_out (EventFocus event) {
		    area.queue_draw ();
			return false;
		}
		
		private bool on_draw (Widget widget, Context cr) {
			Allocation size;
			widget.get_allocation (out size);
			
			// Draw left and top black strokes
			cr.move_to (0.5, size.height); // start in bottom left. 0.5 accounts for cairo's default stroke offset of 1/2 pixels
			cr.line_to (0.5, 0.5); // move to upper left corner
			cr.line_to (size.width + 0.5, 0.5); // move to upper right corner
			
			cr.set_source_rgba (0.0, 0.0, 0.0, 0.95);
			cr.set_line_width (1.0);
			cr.set_antialias (Antialias.NONE);
			cr.stroke ();
			
    		cr.set_source_rgba (1.0, 1.0, 1.0, 0.2);
			cr.rectangle (1.5, 1.5, size.width - 1.5, size.height - 1.5);
			cr.stroke ();
			
			return false;
		}
		
		/**
		 * @brief Add a new visit for this day
		 */
		public void add_visit (Visit visit) {
		    visit_list.append (visit);
		    area.queue_draw ();
		}
		
		/** 
		 * @brief Remove a visit from the list
		 */
		public void remove_visit (Visit visit) {
		    foreach (var v in visit_list) {
		        if (v.id == visit.id) {
		            visit_list.remove (v);
        		    area.queue_draw ();
		            return;
		        }
		    }
		}
		
		/** 
		 * @brief Add a new event for this day
		 */
		public void add_event (Event event) {
		    event_list.append (event);
		    area.queue_draw ();
		}
		
		/**
		 * @brief Remove an event in the list.
		 */
		public void remove_event (Event event) {
		    foreach (var e in event_list) {
		        if (e.id == event.id) {
		            event_list.remove (e);
		            area.queue_draw ();
		            return;
		        }
		    }
		}
		
		public void reset_events () {
		    event_list = new List<Event> ();
		    visit_list = new List<Visit> ();
		}
		
	}
	
}

