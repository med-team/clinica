/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Sqlite;

namespace Clinica {

    public class LocalMedicineIter {
    
        public int64 id;
        
        public Medicine medicine;
        
        public LocalMedicineIter (int64 id, Medicine medicine) {
            this.id = id;
            this.medicine = medicine;
        }
        
    }

    /**
     * @brief This class implements a local medicines database than can
     * be mantained by the user. 
     *
     * In general this database will use a different Sqlite database from
     * the current DataProvider, and the medicines will always be local. 
     * 
     * In every case they will be backupped by the BackupEngine, and 
     * recopied in place after an import.
     */
    public class LocalMedicinesDatabase : Object, Gtk.TreeModel, MedicineSearchEngine {

        /**
         * @brief ResourceManager associated to this Clinica instance. 
         */
        public ResourceManager resource_manager { get; set; } 
        
        /**
         * @brief Sqlite.Database where the medicines will stay. Please not
         * that is a different database from the one with the patient's data. 
         */
        private Database db;
        
        private int stamp = 0;
    
        public LocalMedicinesDatabase (ResourceManager resources) {
            resource_manager = resources;
            string db_path = Path.build_filename(resource_manager.get_data_path (),
                                                 "medicines.db");
            Sqlite.Database.open (db_path, out db);
            
            // Create the table if it does not exists
            db.exec("""CREATE TABLE IF NOT EXISTS medicines (
            id INTEGER PRIMARY KEY,
            name VARCHAR(80),
            description TEXT,
            active_ingredient VARCHAR(150),
            storage_reccomendations TEXT,
            price VARCHAR(30),
            other_notes TEXT)""");
        }
        
        public void search_medicine (string key, MedicineTreeView treeview) {
            // Build the SQL query needed to retrieve the medicines
            Statement stmt;
            string sql = """SELECT id FROM medicines WHERE name LIKE "%""" + key + """%" """;
            
            db.prepare (sql, -1, out stmt);
            stmt.bind_text (1, key);
            
            while (stmt.step () == ROW) {
                int64 id = stmt.column_int64 (0);
                treeview.push_medicine (build_medicine (id));
            }
        }
        
        /**
         * @brief Build a medicine from its ID in the database. 
         */
        private Medicine? build_medicine (int64 id) {
            Statement stmt;
            
            db.prepare ("""SELECT name, description, active_ingredient, storage_reccomendations, 
            price, other_notes FROM medicines WHERE id=:id""", -1, out stmt);
            
            stmt.bind_int64 (1, id);
            
            if (stmt.step() != ROW) {
                warning(_("Medicine with id = %lld not found in DB: %s"), id, db.errmsg ());
                return null;
            }
            else {
                var medicine = new Medicine ();
                medicine.name = stmt.column_text (0);
                medicine.description = stmt.column_text (1);
                medicine.active_ingredient = stmt.column_text (2);
                medicine.storage_reccomendations = stmt.column_text (3);
                medicine.price = stmt.column_text (4);
                medicine.other_notes = stmt.column_text (5);
                
                return medicine;
            }
        }
        
        public void add_medicine (Medicine medicine) {
            Statement stmt;
            db.prepare ("""INSERT INTO medicines (name, description, active_ingredient, storage_reccomendations, 
            price, other_notes) VALUES (?, ?, ?, ?, ?, ?);""", -1, out stmt);
            
            stmt.bind_text (1, medicine.name);
            stmt.bind_text (2, medicine.description);
            stmt.bind_text (3, medicine.active_ingredient);
            stmt.bind_text (4, medicine.storage_reccomendations);
            stmt.bind_text (5, medicine.price);
            stmt.bind_text (6, medicine.other_notes);
            
            if (stmt.step() != DONE) {
                warning (_("Error inserting medicine in the database: %s"), db.errmsg ());
            }
            
            stamp++;
            
            var iter = Gtk.TreeIter ();
            iter.user_data = new LocalMedicineIter (db.last_insert_rowid (), medicine);
            iter.stamp = stamp;
            
            row_inserted (get_path (iter), iter);
        }
        
        public void update_medicine (LocalMedicineIter it, Medicine medicine) {
            Statement stmt;
            db.prepare ("""INSERT OR REPLACE INTO medicines (id, name, description, active_ingredient, storage_reccomendations, 
            price, other_notes) VALUES (?, ?, ?, ?, ?, ?, ?);""", -1, out stmt);
            
            stmt.bind_int64 (1, it.id);
            stmt.bind_text (2, medicine.name);
            stmt.bind_text (3, medicine.description);
            stmt.bind_text (4, medicine.active_ingredient);
            stmt.bind_text (5, medicine.storage_reccomendations);
            stmt.bind_text (6, medicine.price);
            stmt.bind_text (7, medicine.other_notes);
            
            if (stmt.step() != DONE) {
                warning (_("Error inserting medicine in the database: %s"), db.errmsg ());
            }
            
            stamp++;
            
            var iter = Gtk.TreeIter ();
            iter.user_data = new LocalMedicineIter (it.id, medicine);
            iter.stamp = stamp;
            
            row_changed (get_path (iter), iter);
        }
        
        public void delete_medicine (LocalMedicineIter it) {
            Statement stmt;
            
            // Create the TreeIter pointing at that medicine
            var iter = Gtk.TreeIter ();
            iter.user_data = it;
            iter.stamp = stamp;
            var path = get_path (iter);
            
            db.prepare ("""DELETE FROM medicines WHERE id=:id""", -1, out stmt);
            
            stmt.bind_int64 (1, it.id);
            
            if (stmt.step () != DONE) {
                warning ("Error while deleting medicine from DB");
            }
            
            stamp++;
            
            row_deleted (path);
        }
        
        public void abort_search () {
            // Nothing to do, since our request will return immediately
            // not having network request to perform. 
        }
        
        public string get_name () {
            return "Local Medicines Database";
        }
        
        /* IMPLEMENTATION OF THE TREEMODEL INTERFACES */
        
        public Type get_column_type (int index_) {
            if (index_ <= 6)
                return typeof (string);
            else
                return Type.INVALID;
        }
        
        public Gtk.TreeModelFlags get_flags () {
            return Gtk.TreeModelFlags.LIST_ONLY;
        }
        
        private int get_nth_id (int n) {
            Statement stmt;
            
            db.prepare ("""SELECT id FROM medicines ORDER BY id LIMIT 1 OFFSET :off""", 
                        -1, out stmt);
            stmt.bind_int (1, n);
            if (stmt.step () == ROW) {
                return stmt.column_int (0);
            }
            else {
                return -1;
            }
        }
        
        public bool get_iter (out Gtk.TreeIter iter, Gtk.TreePath path) {
            int [] indices = path.get_indices ();
            iter = Gtk.TreeIter ();
            
            if (indices.length > 1) {
                return false;
            }
            
            if (indices.length == 1)
                iter.stamp = stamp;
            else
                iter.stamp = -1;
                
            if (indices[0] >= 0) {
                int64 id = get_nth_id (indices[0]);
                Medicine? medicine = build_medicine (id);
                if (medicine == null) {
                    iter.stamp = -1;
                    iter.user_data = null;
                }
                else
                    iter.user_data = new LocalMedicineIter (id, medicine);
                
            }

            return iter.stamp != -1;
        }
        
        public int get_n_columns () {
            return 6;
        }
        
        public Gtk.TreePath? get_path (Gtk.TreeIter iter) {
            if (iter.user_data == null) {
                return null;
            }
            else {
                LocalMedicineIter? it = iter.user_data as LocalMedicineIter;
                int64 position = -1;
                
                Statement stmt;
                db.prepare ("SELECT COUNT(id) FROM medicines WHERE id < :id", -1, out stmt);
                stmt.bind_int64 (1, it.id);
                
                if (stmt.step () != ROW) {
                    warning ("Error while fetching position of medicine with id = %lld", it.id);
                }
                else {
                    position = stmt.column_int64 (0);
                }
                
                return new Gtk.TreePath.from_indices (position);
            }
        }
        
        public void get_value (Gtk.TreeIter iter, int column, out GLib.Value value) {
                
            var it = iter.user_data as LocalMedicineIter;
            Medicine? medicine = it.medicine;
            
            value = GLib.Value (typeof (string));
        
            switch (column) {
                case 0:
                    value = medicine.name;
                    break;
                case 1:
                    value = medicine.description;
                    break;
                case 2:
                    value = medicine.active_ingredient;
                    break;
                case 3:
                    value = medicine.storage_reccomendations;
                    break;
                case 4:
                    value = medicine.price;
                    break;
                case 5:
                    value = medicine.other_notes;
                    break;
                default:
                    value = GLib.Value (Type.INVALID);
                    break;
            }
        }
        
        public bool iter_children (out Gtk.TreeIter iter, Gtk.TreeIter? parent) {
            iter = Gtk.TreeIter ();
            if (parent == null)  {
                get_iter (out iter, new Gtk.TreePath.first ());
                return iter.stamp != -1;
            }
            else
                return false;
        }
        
        public bool iter_has_child (Gtk.TreeIter iter) {
            return false;
        }
        
        public int iter_n_children (Gtk.TreeIter? iter) {
            Statement stmt;
            db.prepare ("""SELECT COUNT(id) FROM medicines""", -1, out stmt);
            
            if (stmt.step () == ROW) {
                return stmt.column_int (0);
            }
            else {
                warning ("Failing query on database: %s", db.errmsg ());
                return 0;
            }
        }
        
        public bool iter_next (ref Gtk.TreeIter iter) {
            Statement stmt;
            db.prepare ("""SELECT id FROM medicines WHERE id > :base LIMIT 1""", -1, 
                        out stmt);
            LocalMedicineIter? it = iter.user_data as LocalMedicineIter;
            stmt.bind_int64 (1, it.id);
                        
            if (stmt.step () == ROW) {
                int64 new_id = stmt.column_int64(0);
                iter.user_data = new LocalMedicineIter (new_id, build_medicine (new_id));
                return true;
            }
            else {
                iter.stamp = -1;
                return false;
            }
        }
        
        public bool iter_nth_child (out Gtk.TreeIter iter, Gtk.TreeIter? parent, int n) {
            if (parent == null) {
                get_iter (out iter, new Gtk.TreePath.from_indices (n));
            }
            else {
                iter = Gtk.TreeIter ();
                iter.stamp = -1;
            }
            
            return iter.stamp != -1;
        }
        
        public bool iter_parent (out Gtk.TreeIter iter, Gtk.TreeIter child) {
            iter = Gtk.TreeIter ();
            iter.stamp = -1;
            return false;
        }
        
           
    }

}
