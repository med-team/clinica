/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Tommaso Bianucci <churli@gmail.com>
 */
  
using Jansson;

namespace Clinica {

    public class Doctor : Object {
    
        public DataProvider? provider = null;
    
        public int64 id { get; set; default = 0; }
        
        public string given_name { get; set; }
        
        public string surname { get; set; }
        
        public string phone { get; set; }
        
        public string mobile { get; set; }
        
        public string get_complete_name () {
        	return string.join (" ", this.given_name, this.surname);
        }
        
        public PatientIterator patients () {
            return provider.patients (this);
        }
        
        public bool has_patients () {
            return (patients ().next ());
        }
        
        internal Doctor.from_json (Json object, DataProvider? source = null) {
            this.provider = source;
            
            id = object.object_get ("id").integer_value ();
            given_name = object.object_get ("given_name").string_value ();
            surname = object.object_get ("surname").string_value ();
            phone = object.object_get ("phone").string_value ();
            mobile = object.object_get ("mobile").string_value ();
        }
        
        internal Json to_json () {
            var object = Json.object ();
            
            object.object_set ("id", Json.integer (id));
            object.object_set ("given_name", Json.string (given_name));
            object.object_set ("surname", Json.string (surname));
            object.object_set ("phone", Json.string (phone));
            object.object_set ("mobile", Json.string (mobile));
            
            return object;
        }
       
    }

}
