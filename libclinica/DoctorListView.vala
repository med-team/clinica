/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {
    
    public class DoctorListView : TreeView {
    
        private unowned DoctorListStore doctor_list_store;
        private DoctorFilteredListStore filtered_store;
		private TreeModelSort sortable_store;
		
		public signal void error (string message);
		
		private ResourceManager resource_manager;
    
        public DoctorListView (ResourceManager resources, Entry find_entry) {
            resource_manager = resources;
            this.error.connect ((t,l) => resource_manager.error_callback (t,l));
        	TreeViewColumn column;
            
            /* Get the model and filter it with the treeview */
            doctor_list_store = resource_manager.doctor_list_store;
            filtered_store = new DoctorFilteredListStore (resource_manager, find_entry);
            
            /* Wrap the model in a sortable one to allow sorting */
            sortable_store = new TreeModelSort.with_model (filtered_store);
            set_model (sortable_store);
            
            /* Create the Given Name column */
            var name_cell_renderer = new CellRendererText ();
            name_cell_renderer.width = 240;
            column = new TreeViewColumn.with_attributes (_("Name"), name_cell_renderer, "text", DoctorListStore.Field.GIVEN_NAME);
            column.set_sort_column_id (DoctorListStore.Field.GIVEN_NAME);
            append_column (column);
            
            /* ...and then the surname column */
            column = new TreeViewColumn.with_attributes (_("Surname"), new CellRendererText (), "text", DoctorListStore.Field.SURNAME);
            column.set_sort_column_id (DoctorListStore.Field.SURNAME);
            append_column (column);
            
            /* Some callbacks */
            button_press_event.connect (on_button_press_event);
            row_activated.connect (on_row_activated);
            popup_menu.connect (on_popup_menu);
         
            /* Some styling, row in alternating colors */                
            set_rules_hint (true);   
        }
        
       /**
         * @brief Callback called when the user activates a row, for example double
         * clicking or pressing enter on it.
         */
        private void on_row_activated (TreePath path, TreeViewColumn column) {
            TreeIter iter;
            filtered_store.get_iter (out iter, path);
                
            Value doctor;
            TreeIter child_iter;
            filtered_store.convert_iter_to_child_iter (out child_iter, iter);
            doctor_list_store.get_value (child_iter, DoctorListStore.Field.DOCTOR, out doctor);
            
            DoctorEditor doctor_editor = new DoctorEditor.with_doctor (resource_manager, doctor as Doctor);
            doctor_editor.set_transient_for (resource_manager.user_interface.window);
            doctor_editor.run ();
            doctor_editor.destroy ();
        }
        
                
        [CCode (instance_pos = -1)]
        private bool on_button_press_event(Widget view, Gdk.EventButton event) {
        	if (event.type == Gdk.EventType.BUTTON_PRESS && event.button == 3) {
                do_popup_menu (view, event);
        		return true;
        	}
        	
        	return false;
        }
        
        private void do_popup_menu (Widget widget, Gdk.EventButton? event = null) {
       		/* Select the doctor under the cursor */
    		TreePath path;
       		TreeSelection selection = get_selection();
    		if (event != null) {
        		get_path_at_pos ((int) event.x, (int) event.y, out path, null, null, null);
        		selection.select_path (path);
            }
    	
    		var doctor = get_selected_doctor ();
    		var menu = new DoctorContextMenu (resource_manager, this, doctor);
    		menu.attach_to_widget (this, null);
    		menu.popup (null, null, null, 
    		    (event != null) ? event.button : 0, 
    		    (event != null) ? event.time : Gtk.get_current_event_time ());
        }
        
        /**
    	 * @brief Callback called when the user press the "Popup a menu" button
    	 * on its keyboard.
    	 */
    	private bool on_popup_menu (Widget widget) {
    	    do_popup_menu (widget);
    	    return false;
    	}
        
        /**
         * @brief Return an iter in the DoctorListStore associated
         * with the current selection, or null
         */
        public TreeIter? get_selected_iter () {
        	TreeSelection selection = get_selection ();
        	TreeIter iter;
        	TreeModel model;
        	
        	if (!selection.get_selected (out model, out iter)) {
        		return null;
        	}
        	
        	TreeIter real_iter;
			sortable_store.convert_iter_to_child_iter (out real_iter, iter);
        	filtered_store.convert_iter_to_child_iter (out iter, real_iter);
        	return iter;
        }
        
        /**
         * @brief Retrieve Doctor object associated to the selected
         * element or null if no doctor is selected.
         */
        public Doctor? get_selected_doctor () {        	        	
        	TreeSelection selection = get_selection ();
    		TreeIter iter;
    		TreeModel model;
    		
    		if (!selection.get_selected(out model, out iter)) {
    			return null;
    		}
    		else {
    			Value value;
    			model.get_value(iter, 0, out value);
    			var d = value as Doctor;
    			return d;
    		}
        }
        
        /**
         * @brief Remove the selected doctor from the treeview
         * and from the database. 
         */
        public void remove_selected_doctor () {
        	TreeSelection selection = get_selection ();
        	TreeIter iter;
        	TreeModel model;
        	Doctor? doc = get_selected_doctor ();
        	
        	if (!selection.get_selected(out model, out iter)) {
        		error (_("Select a doctor to delete it!"));
        	}
        	else {
	        	/* Check if the doctor if this patient is the same that we
	    		 * are trying to delete */
		    	if (doc.has_patients ()) {
		    		var warning_dialog = new MessageDialog (
					resource_manager.user_interface.window, 
		    			DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT,
		    			MessageType.QUESTION, ButtonsType.YES_NO, "%s", "");
		    			warning_dialog.set_markup(
		    				_("The doctor that you have selected for removal has some patients associated. \n") +
		    				_("It's not possible to remove it without disassociating all his patients.\n\n") +
		    				_("Do you really want to proceed?"));
		    			warning_dialog.set_title (_("Doctor has associated patients"));
		    			warning_dialog.set_transient_for (resource_manager.user_interface.window);
		    		
		    		if (warning_dialog.run () != ResponseType.YES) {
		    			warning_dialog.destroy ();
		    			return;
		    		}
		    		else {
		    			warning_dialog.destroy ();
		    			
		    			/* Get List of the patients to remove and delete them */
		    			PatientIterator patients = doc.patients ();
		    			
		    			/* Create an array with the names oft he patients 
		    			 * that will be deleted */
		    			string [] patient_names = {};
		    			
		    			foreach(Patient p in patients) {
		    				patient_names += " - <b>" + p.get_complete_name() + "</b>";
		    			}
		    			
		    			/* Warn another time the user that these patients will be deleted and
		    			 * check if the user is really sure about this operation. */
		    			warning_dialog = new MessageDialog(null, DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT,
		    				MessageType.QUESTION, ButtonsType.YES_NO, "%s", "");
		    			warning_dialog.set_markup (
		    				_("The following patients will be disassociated from this doctor by this action:\n") + 
		    				string.joinv("\n", patient_names) + "\n" +
		    				_("Do you really want to proceed?"));
		    			warning_dialog.set_title (_("Confirm disassociation of patients"));
		    			warning_dialog.set_transient_for (resource_manager.user_interface.window);
		    			
		    			if (warning_dialog.run () != ResponseType.YES) {
		    				warning_dialog.destroy ();
		    				return;
		    			}
		    			
		    			/* If we got here then the user is really sure, so delete the patients */
		    			warning_dialog.destroy ();
		    		}
		    	}
		    	else {
		    		var question = new MessageDialog (null, 
        				DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT, 
        				MessageType.QUESTION, ButtonsType.YES_NO,
        				"%s", 
        				_("Really delete this doctor? All information about him/her will be lost."));
        		    	question.set_transient_for (resource_manager.user_interface.window);
        			if (question.run () != ResponseType.YES) {
        				question.destroy ();
        				return;
        			}
        			question.destroy ();
		    	}
		    	
		    	/* Proceed with the removal of the doctor */
		    	resource_manager.data_provider.remove_doctor (doc);
		    }
        
        }
    }

}
