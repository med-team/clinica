/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Tommaso Bianucci <churli@gmail.com>
 */
 
using Jansson;

namespace Clinica {

    public class Event : Object {
    
        public DataProvider? provider = null;
        
        public int64 id { get; set; default = 0; }
    
        public string title { get; set; }
        
        public string description { get; set; }
        
        public string venue { get; set; }
        
        /** 
         * @brief Patient associated to this event, if one exists,
         * or null otherwise
         */
        public Patient? patient { get; set; }
        
        /** 
         * @brief Visit associated to this event, if one exists,
         * or null otherwise
         */
        public Visit? visit { get; set; }
        
        /**
         * @brief Date associated to this event.
         */
        public DateTime date { get; set; }
        
        internal Event.from_json (Json o, DataProvider? provider = null) {
            id = o.object_get ("id").integer_value ();
            title = o.object_get ("title").string_value ();
            description = o.object_get ("description").string_value ();
            venue = o.object_get ("venue").string_value ();
            date = SqliteDataProvider.string_to_datetime (
                o.object_get ("date").string_value ());
            
            if (provider != null) {
                var patient_id = o.object_get ("patient").integer_value ();
                if (patient_id != 0) {
                    patient = provider.get_patient (patient_id);
                }
                
                var visit_id = o.object_get ("visit").integer_value ();
                if (visit_id != 0) {
                    visit = provider.get_visit (visit_id);
                }
            }
        }
        
        internal Json to_json () {
            var object = Json.object ();
            
            object.object_set ("id", Json.integer (id));
            object.object_set ("title", Json.string (title));
            object.object_set ("description", Json.string (description));
            object.object_set ("venue", Json.string (venue));
            object.object_set ("patient", Json.integer (
                (patient != null) ? patient.id : 0));
            object.object_set ("visit", Json.integer (
                (visit != null) ? visit.id : 0));
            object.object_set ("date", Json.string (
                SqliteDataProvider.datetime_to_string (date)));
            
            return object;
        }
    
    }

}
