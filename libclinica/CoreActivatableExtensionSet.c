#include <libpeas/peas.h>
#include <clinica.h>
#include <stdio.h>

static void
on_extension_added (PeasExtensionSet *set,
		    PeasPluginInfo *info,
		    ClinicaCoreActivatable *activatable)
{
    clinica_core_activatable_activate (activatable);
}

static void
on_extension_removed (PeasExtensionSet *set,
		      PeasPluginInfo *info,
		      ClinicaCoreActivatable *activatable)
{
    clinica_core_activatable_deactivate (activatable);
}

PeasExtensionSet*
clinica_resource_manager_setup_extension_set (ClinicaResourceManager *rm,
                                              PeasEngine *engine)
{
    PeasExtensionSet *set;
    
    set = peas_extension_set_new (engine, CLINICA_TYPE_CORE_ACTIVATABLE,
				  "resource_manager", rm, NULL);

    g_signal_connect (set, "extension-added",
		      G_CALLBACK (on_extension_added), NULL);
    g_signal_connect (set, "extension-removed",
		      G_CALLBACK (on_extension_removed), NULL);
#ifdef HAVE_PEAS_EXTENSION_SET_FOREACH
     peas_extension_set_foreach (set,
				(PeasExtensionSetForeachFunc) on_extension_added,
				NULL);
#else
     peas_extension_set_call (set, "activate");
#endif

    return set;
}
