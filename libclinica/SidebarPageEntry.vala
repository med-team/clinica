/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

namespace Clinica {

    /**
     * @brief A SidebarEntry that loads the given page when
     * activated.
     */
    public class SidebarPageEntry : Object, SidebarEntry {
    
        /**
         * @brief The Page linked to this sidebar entry.
         */
        private Page page;
        
        private Gdk.Pixbuf _pixbuf;
        
        private string _name;
    
        public SidebarPageEntry (ResourceManager resources, Page page, string name, Gdk.Pixbuf pixbuf) {
            this.page = page;
            _name = name;
            _pixbuf = pixbuf;
            
            // Connet the activated signal to the loading of the
            // page. 
            activated.connect ((entry) => 
                resources.user_interface.load_page (page));
        }
        
        public string title () {
            return _name;
        }
        
        public Gdk.Pixbuf? pixbuf () {
            return _pixbuf;
        }
    
    }

}
