/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    public class AboutDialog : Gtk.AboutDialog {
        public AboutDialog (ResourceManager resources) {
            string [] authors = { "Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>",
                                   "Leonardo Robol <robol@poisson.phc.unipi.it>",
                                   "Maxwell Barvian (for the calendar taken from Maya)" };
            string [] artists = { "Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>", 
                                  "Francesco Del Prato <delprato@poisson.phc.unipi.it>" };
            /* Get the logo */
            var logo = new Image.from_pixbuf (Utils.pixbuf_from_svg_resource (
                Config.RESOURCE_BASE + "ui/icons/logo.svg", 128, 128));
            
            /* Init the Dialog with the data */
            GLib.Object (
                program_name: "Clinica",
                version: Config.VERSION,
                logo: logo.pixbuf,
                comments: _("Medical records manager"),
                website: "http://poisson.phc.unipi.it/clinica/",
                title: _("About Clinica"),
                artists: artists,	
                authors: authors,
                
                /* Credits of the translators. It starts with "stable"
                 * translators, the one that are parte of the clinica
                 * team, and then it shows the launchpad contributed
                 * translations. */
                translator_credits: 
                _("Leonardo Robol <robol@poisson.phc.unipi.it>\n") +
                _("Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>\n") +
                
                /* Start of LAUNCHPAD contributions. Each one is indented
                 * of two spaces */
                "zeugma https://launchpad.net/~sunder67\n" +
                "Enrico \"eNry\" Carafa https://launchpad.net/~mr.tennents\n" +
                "Guybrush88 https://launchpad.net/~guybrush\n" +
                "Beto1917 https://launchpad.net/~beto-mst\n" + 
                "Sonia Alenda https://launchpad.net/~soniaalenda\n" +
                "Мирослав Николић https://launchpad.net/~lipek\n" + 
                "Carlos C Soto https://launchpad.net/~csoto/\n" + 
                "Fitoschido https://launchpad.net/~fitoschido\n" + 
                "Jonay https://launchpad.net/~jonay-santana\n" +
                "Marco https://launchpad.net/~marcodefreitas\n"
            );
            
            set_modal (true);
        }
        
    }

}
