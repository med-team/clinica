/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Gtk;

namespace Clinica {

	public class UserInterface : GLib.Object {
	
	    private Gtk.ActionGroup entries;
	
	    /* Error signal and associated callback passed by main () */
	    public signal void error (string message);
	    
	    /* true if the graphic has been started */
	    public bool started { get; private set; default = false; }
	
	    /* Various objects needed for coordination */
		internal Window window;
		internal Clinica.UIManager manager;

        /* Gtk Objects */
		private Alignment main;
		
		/* Windows */
		internal SettingsManager? settings_manager = null;
		internal CalendarWindow?  calendar_window  = null;
		
#if HAVE_PLUGINS
		private extern Peas.ExtensionSet setup_extension_set (ResourceManager resources, Peas.Engine engine);
		internal Peas.ExtensionSet extension_set;
#endif
		
		/* ResourceManager */
		public ResourceManager resource_manager;
		
		internal HashTable<string, Page?> pages;
		
		private Sidebar sidebar;
			
		public UserInterface (ResourceManager rm) {
		    resource_manager = rm;
		    resource_manager.user_interface = this;
		    
		    pages = new HashTable<string, Page?> (GLib.str_hash, GLib.str_equal);
			
		    /* Keep resource_manager and connect callback signal */
		    error.connect ((t,l) => resource_manager.error_callback(t,l));
		    
		    /* Create a new Window and setup the menubar in it */
            window = new Gtk.Window ();
            window.destroy.connect (stop);
            window.delete_event.connect ((window) => { stop() ; return true; });
            
            sidebar = new Sidebar (resource_manager);
            
            var box = new Gtk.Paned (Gtk.Orientation.HORIZONTAL);
            var top_box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
            
            /* Load the UI manager and connect callbacks */
            manager = new UIManager (resource_manager);
            top_box.pack_start (manager.get_widget ("/MainMenu"), false, true);
            window.add_accel_group (manager.get_accel_group ());
            load_action_entries ();
            connect_menu_actions ();
            
            /* Create an Alignment to hold the pages */
            main = new Alignment (0.5f, 0.5f, 1.0f, 1.0f);
            main.set_padding (resource_manager.PADDING, resource_manager.PADDING, 
                    0U, 0U);
                    
            sidebar.set_margin_left (resource_manager.PADDING);
            main.set_margin_right (resource_manager.PADDING);

            box.pack1 (sidebar, false, false);
            box.pack2 (main, true, false);
            
            top_box.pack_start (box, true, true);
            
            box.set_position (220);
            sidebar.set_size_request (200, -1);

            window.add (top_box);
	        
	        /* Load pages and connect callbacks with errors */
            add_page ("start", new StartPage (resource_manager), true);
	        
	        /* Show starting page */
	        show_start_page ();
	        
#if HAVE_PLUGINS
	        /* Load plugins */
	        load_plugins.begin ();
#endif
	        
	        /* Load other pages and then connect signals so the user can
	         * visit them */
	        load_pages.begin ();
	        
	        /* Request a decent size for the StartPage otherwise it will be quite compressed
	         * and not so clear. */
	        window.realize.connect (() => (main.get_child ()).set_size_request (950, 550));
		}
		
		public void add_page (string key, Page page, bool show_in_sidebar = false) {
		    if (pages[key] == null) {
                pages[key] = page;
                if (show_in_sidebar)
                    sidebar.add_entry (page.get_sidebar_entry ());
            } else {
                warning ("Trying to overwrite a page instance, ignoring the request");
            }
        }
		
		private void connect_menu_actions () {
            manager.get_action ("/MainMenu/FileMenu/New Patient").activate.connect (on_new_patient_action_activate);
            manager.get_action ("/MainMenu/FileMenu/New Doctor").activate.connect (on_new_doctor_action_activate);
            manager.get_action ("/MainMenu/FileMenu/Quit").activate.connect ((item) => stop ());
            manager.get_action ("/MainMenu/ViewMenu/Start page").activate.connect ((item) => show_start_page ());
            manager.get_action ("/MainMenu/ViewMenu/Patients").activate.connect ((item) => show_patient_list_page ());
            manager.get_action ("/MainMenu/ViewMenu/Doctors").activate.connect ((item) => show_doctor_list_page ());
            manager.get_action ("/MainMenu/ViewMenu/Search medicines").activate.connect ((item) => show_medicines_search_page ());
            manager.get_action ("/MainMenu/ToolsMenu/Settings").activate.connect (on_settings_action_activate);
            manager.get_action ("/MainMenu/ToolsMenu/Backup").activate.connect (on_backup_action_activate);
            manager.get_action ("/MainMenu/ToolsMenu/ImportData").activate.connect (on_import_data_action_activate);
            manager.get_action ("/MainMenu/ToolsMenu/AddMedicines").activate.connect (on_add_medicines_action_activate);
            manager.get_action ("/MainMenu/ToolsMenu/BrowseMedicines").activate.connect (on_browse_medicines_action_activate);
            manager.get_action ("/MainMenu/HelpMenu/Contents").activate.connect (on_help_action_activate);
            manager.get_action ("/MainMenu/HelpMenu/Report a bug").activate.connect (on_report_a_bug_action_activate);
            manager.get_action ("/MainMenu/HelpMenu/About").activate.connect (on_about_action_activate);
		}
		
		/**
		 * @brief Populate the entries from GtkUIManager.
		 */
		internal void load_action_entries () {
		    entries = new Gtk.ActionGroup ("MainMenu");
		}
		
		/**
		 * @brief Hide user interface if it is not desired anymore.
		 */
		private void stop () {
		    window.hide ();
 		    resource_manager.application.release ();
        }
		
		internal async void load_pages () {
		    add_page ("patients", new PatientListPage (resource_manager), true);
		    add_page ("doctors", new DoctorListPage (resource_manager), true);
		    add_page ("medicines", new MedicineSearchPage (resource_manager), true);
		    sidebar.add_entry (new SidebarCalendarEntry (resource_manager));
		    sidebar.expand_all ();
		}
		
#if HAVE_PLUGINS
		internal async void load_plugins () {
		    extension_set = setup_extension_set (resource_manager,
		                                         resource_manager.plugin_engine);
		}
#endif
			
		public void error_callback (Page page, string message) {
			resource_manager.error_callback (page, message);
		}
		
		/**
		 * @brief Return the UI manager associated with the Clinica
		 * instance, that will be used to add menu items in plugins.
		 */
		public Clinica.UIManager get_ui_manager () {
		    return manager;
		}
		
		public void load_page (Page page) {
		    if (main.get_child () != null) {
		        if (main.get_child () == (page as Widget))
		            return;
		        else {
		        	main.get_child ().hide ();	
		            main.remove (main.get_child ());
		        }
		    } 
		    
	        main.add (page);
            page.show ();
            window.set_title (page.get_title ());

            sidebar.select_entry (page.get_sidebar_entry ());
		}
		
		/**
		 * @brief Set active page in the main window.
		 * @param page the page to be set as active.
		 */
		public Page? set_page (string key) {
		    var page = pages[key];
		    if (page == null) {
		        warning ("Requested page with key %s, but was not loaded. Ignoring the request", key);
		        return null;
		    }
		    
		    load_page (page);
		    
            return page;
		}
		
		/**
		 * @brief Show the button "Create new Patient"
		 * and nothing more on the main window. 
		 */
		public void show_start_page (string patient_name = "") {
		    set_page ("start");
		    pages["start"].set_name (patient_name);
		}
		
		/**
		 * @brief Show patient list in the main page.
		 */
		public void show_patient_list_page () {
		    set_page ("patients");
		}
		
		/**
		 * @brief Show doctor list in the main page.
		 */
		public void show_doctor_list_page (GLib.Object? source = null) {
		    set_page ("doctors");
		}
		
		/**
		 * @brief Show the search medicines page.
		 */
		[CCode (instance_pos = -1)]
		public void show_medicines_search_page (GLib.Object? source = null) {
		    set_page ("medicines");
		}
		
		/**
		 * @brief Show the calendar window.
		 */
		[CCode (instance_pos = -1)]
		public void show_calendar_window (GLib.Object? source = null) {
		    if (calendar_window == null) {
    		    calendar_window = new CalendarWindow (resource_manager);
    		    calendar_window.destroy.connect ((cw) => calendar_window = null);
    		}
            else {
                /* Bring the window to front if it's not there */		    
                calendar_window.present ();
            }
            
		    calendar_window.show_all ();
		}
		
		public void register_page (string identifier, Page page) {
		    pages[identifier] = page;
		    
		    var sidebar_entry = page.get_sidebar_entry ();
		    
		    if (sidebar_entry != null) {
		        sidebar.add_entry (page.get_sidebar_entry (), 
		            page.get_parent_entry ());
		    }
		}
		
		public void unregister_page (string identifier) {
		    var page = pages[identifier];
		    sidebar.remove_entry (page.get_sidebar_entry ());
		    pages[identifier] = null;
		}
		
		/**
		 * @brief Show the VisitiWindow for the given page. If it
		 * is already open than bring it on top.
		 */
		public VisitPage show_visit_window (Patient p) {
		    string identifier = "patients/%s".printf (p.id.to_string ());
		    var page = pages[identifier];
		    if (page == null) {
		        page = new VisitPage (resource_manager, p);
		        register_page (identifier, page);
		    }
		    
		    // Load the page and display the mainwindow, if it is not
		    // displayed in front of the user yet. 
		    load_page (page);
		    window.show_all ();
		    window.present ();
		    
		    return page as VisitPage;
		}
		
		public void close_visit_window (Patient p) {
		    string identifier = "patients/%s".printf (p.id.to_string ());
		    var page = pages[identifier];
		    if (page != null) {
		        
		    }
		}
		
		/* CALLBACKS */
		private void on_new_patient_action_activate (Gtk.Action action) {
			var new_patient_dialog = new PatientEditor (resource_manager);
			new_patient_dialog.set_transient_for (window);
			new_patient_dialog.run ();
			new_patient_dialog.destroy ();
		}
		
        private void on_new_doctor_action_activate (Gtk.Action item) {
        	var new_doc_dialog = new DoctorEditor (resource_manager);
        	new_doc_dialog.set_transient_for (window);
	       	new_doc_dialog.run ();
			new_doc_dialog.destroy ();
        }

		private void on_about_action_activate (Gtk.Action action) {
		    var about_dialog = new AboutDialog (resource_manager);
		    about_dialog.set_transient_for (window);
		    about_dialog.run (); 
		    about_dialog.destroy ();
		}
		
		private void on_settings_action_activate (Gtk.Action action) {
		    if (settings_manager == null){
    		    settings_manager = new SettingsManager (resource_manager);
    		    /* Dereference the window on release to make it be freed and
		     * reconstructed when necessary */
		    settings_manager.destroy.connect ((sm) => settings_manager = null);
		}
    		    else {
                	/* Bring the window to front if it's not there */		    
                	settings_manager.present ();
                	}
		    settings_manager.show_all ();
		}
		
		private void on_backup_action_activate (Gtk.Action action) {
		    var backup_engine = new BackupEngine (resource_manager);
		    
		    // Ask the user where he would like to backup its files. 
		    var chooser = new FileChooserDialog (_("Select the folder where the data should be saved"), 
		        this.window, FileChooserAction.SELECT_FOLDER, Gtk.Stock.CANCEL, ResponseType.CANCEL,
		        Gtk.Stock.SAVE, ResponseType.ACCEPT);
		    
		    // Set a reasonable default file name
		    chooser.set_filename ("Backup");
		    
		    if (chooser.run () == ResponseType.ACCEPT) {
		        debug ("Backing up user data");
		        var filename = chooser.get_filename ();
    	    	chooser.destroy ();
    	    	
    	    	string destination = Path.build_filename (filename, 
    	    	    "Clinica_Backup_%s".printf (new DateTime.now_local ().to_string ()));
    	    	debug ("Creating an empty folder in %s", destination);
    	    	
    	    	DirUtils.create (destination, -1);
    	    	
    		    backup_engine.backup (resource_manager.data_provider, 
	    	        destination);
	    	        
	    	    var dialog = new MessageDialog (window, DialogFlags.MODAL, 
	    	        MessageType.INFO,
	    	        ButtonsType.OK,
	    	        "%s", 
	    	        /* In this string %s refers to the path where the backup has been performed. */
	    	        _("Backup completed successfully in the folder %s").printf (filename));
	    	    dialog.set_title (_("Backup completed"));
	    	    dialog.format_secondary_markup (
	    	        _("Please take care of copying the folder in a safe place for future restore of the data."));
	    	    dialog.run ();
	    	    dialog.destroy ();
	    	}
	    	else {
	    	    chooser.destroy ();
	    	}
		}
		
		private void on_import_data_action_activate (Gtk.Action action) {
		    var import_dialog = new ImportDialog (resource_manager);
		    if (import_dialog.run () == ResponseType.ACCEPT) {
		        var filename = import_dialog.get_filename ();
		        import_dialog.destroy ();
		        
		        // Import the data using the BackupEngine. 
		        var engine = new BackupEngine (resource_manager);
		        engine.import.begin (filename);
            }
            else        
                import_dialog.destroy ();
		}
		
		private void on_add_medicines_action_activate (Gtk.Action action) {
		    var medicine_editor = new MedicineEditor (resource_manager);
		    medicine_editor.set_transient_for (this.window);
		    medicine_editor.run ();
		    medicine_editor.destroy ();
		}
		
		private void on_browse_medicines_action_activate (Gtk.Action action) {
		    var medicine_browser = new MedicineBrowserDialog (resource_manager);
		    medicine_browser.set_transient_for (this.window);
		    medicine_browser.run ();
		    medicine_browser.destroy ();
		}
		
		private void on_help_action_activate (Gtk.Action action) {
		    try {
    		    Gtk.show_uri (this.window.get_screen (), "ghelp:clinica", Gdk.CURRENT_TIME);
    		} catch (GLib.Error e) {
    		    error (_("Cannot open the help: %s").printf (e));
    		}
		}
		
		/**
		 * @brief Report a bug to launchpad.
		 *
		 * Tries launchpad-integration first, if that fails try the browser and if even that
		 * fails than give an error message to the user.
		 */
		private void on_report_a_bug_action_activate (Gtk.Action action) {
		    try {
		        Pid pid;
		        if (FileUtils.test ("/usr/bin/clinica", FileTest.EXISTS)) {
    		        Process.spawn_async (null, { "/usr/bin/launchpad-integration", "-b", "-Pclinica" }, null, 0, null, out pid);
    		    }
    		    else {
    		        throw new GLib.Error (Quark.from_string ("DEBUG"), 
    		            1, "Could not find /usr/bin/clinica, maybe clinica is not installed?");
    		    }
		    } catch (GLib.Error e) {
		        print ("Error: %s\n".printf (e.message));
		        try {
		            Gtk.show_uri (null, "https://bugs.launchpad.net/clinica-project/+filebug", Gdk.CURRENT_TIME);
		        } catch (GLib.Error e) {
		            error (_("An error occurred while opening the bug system. Please go to\nhttp://launchpad.net/clinica-project/ to file a bug"));
		        }
		    }
		}
		
		public void show_main_window () {
		    window.show_all ();
		    show_start_page ();
		    window.present ();
		}
		
		/**
		 * @brief start application
		 */
		public void start () {
			started = true;
		}
	}
}
