/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
 namespace Clinica {
 
    [DBus (name = "org.phcteam.clinica.Service")]
    public class Service : GLib.Object {
    
        /**
         * @brief The resource manager associated to this
         * Service.
         */
        internal ResourceManager resource_manager;
    
        /**
         * @brief Return the version of clinica that is running.
         */
        public string version () {
            return Config.VERSION;
        }
        
        /**
         * @brief Return a lists of patients id whose name
         * match the given string.
         */
        public int64 [] patients_filter (string filter) {
            string lfilter = filter.down();
            int64 [] patients = {};
            foreach (var patient in resource_manager.data_provider.patients ()) {
                if (lfilter in patient.get_complete_name ().down ()) {
                    patients += patient.id;
                }
            }
            
            return patients;
        }
        
        /**
         * @brief Get the patients name associated to the given ID.
         */
        public string get_patient_name (int64 id) {
           var patient = resource_manager.data_provider.get_patient (id);
            
            if (patient == null)
                return "";
                
           return patient.get_complete_name (); 
        }
        
        /**
         * @brief Return patient data as a dictionary.
         */
        public HashTable<string, string> get_patient_data (int64 id) {
            HashTable<string, string> table = new HashTable<string, string> (GLib.str_hash, GLib.str_equal);
            var patient = resource_manager.data_provider.get_patient (id);
            
            if (patient == null)
                return table;
            
            /* Fill the table with the patient data */
            table["given_name"] = patient.given_name;
            table["surname"] = patient.surname;
            table["birth_date"] = patient.birth_date.to_string();
            table["gender"] = (patient.gender == Gender.MALE) ? "MALE" : "FEMALE";
            table["residence_address"] = patient.residence_address;
            table["phone"] = patient.phone;
            table["identification_code"] = patient.identification_code;
            
            return table;
        }
        
        /**
         * @brief Retrieve the id of the patient's doctor. May be 0 if that patient
         * has no doctor. 
         */
        public int64 get_patient_doctor (int64 id) {
            var patient = resource_manager.data_provider.get_patient (id);
            
            if (patient == null)
                return -1;
                
            return (patient.doctor == null) ? 0 : patient.doctor.id;
        }
        
        /**
         * @brief Retrieve data associated to a given doctor.
         */
        public HashTable<string, string> get_doctor_data (int64 id) {
            HashTable<string, string> table = new HashTable<string, string> (GLib.str_hash, GLib.str_equal);
            var doctor = resource_manager.data_provider.get_doctor (id);
            
            if (doctor == null)
                return table;
            
            table["given_name"] = doctor.given_name;
            table["surname"] = doctor.surname;
            table["phone"] = doctor.phone;
            table["mobile"] = doctor.mobile;
            
            return table;
        }
        
        /** 
         * @brief Retrieve a list of IDs of the the visits of the given patient (or of all the patients
         * if id is 0). Optional time boundaries can be given in the internal format used by clinica, that
         * is "yyyy mm dd hh mm ss"
         */
        public int64 [] get_patient_visits (int64 id = 0, string start = "", string end = "") {
            int64 [] visits = {};
            Patient? patient = null;
            
            DateTime? start_time = null;
            DateTime? end_time = null;

            if (start != "")            
                start_time = SqliteDataProvider.string_to_datetime (start);
            if (end != "")
                end_time = SqliteDataProvider.string_to_datetime (end);
            
            if (id != 0) {
                patient = resource_manager.data_provider.get_patient (id);
                if (patient == null)
                    return visits;
            }
            else
                patient = null;
            
            foreach (var visit in resource_manager.data_provider.visits (patient, start_time, end_time)) {
                visits += visit.id;
            }
            
            return visits;
        }
        
        /**
         * @brief Retrieve the data associated with a visits.
         */
        public HashTable<string, string> get_visit_data (int64 id) {
            var table = new HashTable<string, string> (GLib.str_hash, GLib.str_equal);
            var visit = resource_manager.data_provider.get_visit (id);
            
            if (visit == null)
                return table;
                
            table["anamnesis"] = visit.anamnesis;
            table["physical_examination"] = visit.physical_examination;
            table["laboratory_exam"] = visit.laboratory_exam;
            table["histopathology"] = visit.histopathology;
            table["diagnosis"] = visit.diagnosis;
            table["topical_therapy"] = visit.topical_therapy;
            table["systemic_therapy"] = visit.systemic_therapy;
            table["subsequent_checks"] = visit.subsequent_checks;
            table["date"] = SqliteDataProvider.datetime_to_string (visit.date);
            table["patient"] = visit.patient.id.to_string ();
            
            return table;
        }
        
        /** 
         * @brief Retrieve a list of IDs of the the events with the optional
         * time boundaries that can be given in the internal format used by clinica, that
         * is "yyyy mm dd hh mm ss"
         */        
        public int64 [] get_events (string start = "", string end = "") {            
            int64 [] events = {};
            
            DateTime? start_time = null;
            DateTime? end_time = null;

            if (start != "")            
                start_time = SqliteDataProvider.string_to_datetime (start);
            if (end != "")
                end_time = SqliteDataProvider.string_to_datetime (end);
            
            foreach (var event in resource_manager.data_provider.events (start_time, end_time)) {
                events += event.id;
            }
            
            return events;
        }
        
        public HashTable<string, string> get_event_data (int64 id) {
            var table = new HashTable<string, string> (GLib.str_hash, GLib.str_equal);
            
            var event = resource_manager.data_provider.get_event (id);
            if (event == null)
                return table;
                
            table["title"] = event.title;
            table["description"] = event.description;
            table["venue"] = event.venue;
            table["patient"] = (event.patient == null) ? "" : event.patient.id.to_string ();
            table["visit"] = (event.visit == null) ? "" : event.visit.id.to_string ();
            table["date"] = SqliteDataProvider.datetime_to_string (event.date);
            
            return table;
        }
        
        /**
         * @brief Open the vists of the patients specified by the given
         * ID.
         */
        public void open_visits (int64 id) {
            Patient p = resource_manager.data_provider.get_patient (id);
            resource_manager.application.show_visits (p);
        }
        
        public Service (ResourceManager resources) {
            resource_manager = resources;
        }
        
    }
    
    
 }
