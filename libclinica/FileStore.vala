/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
namespace Clinica {

    public interface FileStore : GLib.Object {
    
        /**
         * @brief Signal emitted when the FileStore changes. This invalidates
         * all the FileObjects around, that must be reloaded.
         */
        public signal void changed (int64 id);
        
        /**
         * @brief Signal emitted when something goes wrong :)
         */
        public signal void error (string message);
        
        /**
         * @brief Get the path where the files related to the given ID
         * are stored.
         */        
        public abstract string get_id_path (int64 id);
        
        /** 
         * @brief Get the list of files associated to an ID.
         * This may even be an empty List.
         */
        public abstract GLib.List<FileObject> get_files (int64 id);
        
        /**
         * @brief Store a file called filename in the FileStore.
         */
        public abstract string store_file (int64 id, string filename);
        
        /**
         * @brief Remove a file from the store.
         */
        public abstract void remove_file (int64 id, string filename);
    }
}
