/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 
 namespace Clinica {
 
    public class EventEditor : Dialog {
    
        enum Response {
            CANCEL,
            SAVE,
        }
    
        /**
         * @brief Resource Manager used to access clinica
         * resources.
         */
        private ResourceManager resource_manager { get; set; }
    
        /** 
         * @brief event that is being edited by the EventEditor
         */
        internal Event selected_event { get; private set; }
        
        /**
         * @brief Entry for the title of the event.
         */
        private Entry title_entry;
        
        /**
         * @brief Entry for the venue.
         */
        private Entry venue_entry;
        
        /**
         * @brief Calendar used in the dialog to select the data of the event
         */
        private Gtk.Calendar calendar;
        
        /** 
         * @brief Selector of the hour of the event.
         */
        private SpinButton hour_spinbutton;
        
        /**
         * @brief Selection of the minute of the event.
         */
        private SpinButton minute_spinbutton;
        
        /**
         * @brief Multiline editor for the desription of the event
         */
        private TextView description_textview;
        
        /**
         * @brief Boolean value that tells if the user has filled the buffer
         * of the description, or if it is left with its default hint.
         *
         * In the first case the description should be saved, in the latter ignored.
         */
        private bool description_inserted = false;
        
        /**
         * @brief Entry used by the user to select the patient
         */
        private PatientEntry patient_entry;
        
        public EventEditor (ResourceManager resources, Event? event = null) {
            resource_manager = resources;
            
            /* If the event is null, let's create a new one, otherwise
             * store the event passed for editing. */
            if (event == null) {
                selected_event = new Event ();
                set_title (_("Create a new event"));
            }
            else {
                selected_event = event;
                set_title (_("Editing event: %s").printf (event.title));
            }
            
            /* Add Save and Cancel buttons */
            add_buttons (Stock.CANCEL, Response.CANCEL, 
                         Stock.SAVE,   Response.SAVE);
                         
            /* A vertical box to contain all the widgets */
            var structure_box = new Box (Orientation.VERTICAL, resource_manager.PADDING);
            
            /* The header, with a picture representing an event and title 
             * and venue. */
            var event_picture = new Image.from_pixbuf (
                Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/calendar.svg", 64, 64));
            var header_box = new Box (Orientation.HORIZONTAL, resource_manager.PADDING);
            header_box.pack_start (event_picture, false);
            
            /* Create the entry for title of the event and venue */
            var title_label = new Label (_("Title"));
            var venue_label = new Label (_("Venue"));
            
            title_entry = new Entry ();
            venue_entry = new Entry ();
            
            /* Packing them near the image */
            var table = new Grid ();
            table.column_spacing = table.row_spacing = resource_manager.PADDING;
            table.attach (title_label, 0, 0, 1, 1);
            title_label.hexpand = false; title_label.vexpand = false;
            
            table.attach (venue_label, 0, 1, 1, 1);
            venue_label.hexpand = venue_label.vexpand = false;
            
            table.attach (title_entry, 1, 0, 1, 1);
            title_entry.hexpand = true; 
            title_entry.vexpand = false;
            
            table.attach (venue_entry, 1, 1, 1, 1);
            venue_entry.hexpand = true;
            venue_entry.vexpand = false;
                
            header_box.pack_start (table);
            structure_box.pack_start (header_box, false);
            
            /* Box for calendar and description */
            var cal_desc_box = new Box (Orientation.HORIZONTAL, resource_manager.PADDING);
            
            /* Create the calendar used to select the date */
            calendar = new Gtk.Calendar ();
            cal_desc_box.pack_start (calendar, false);
                
            /* Create the hour and minute selector */
            var desc_hm_box = new Box (Orientation.VERTICAL, resource_manager.PADDING);
            var hm_box = new Box (Orientation.HORIZONTAL, resource_manager.PADDING);
            hm_box.pack_start (new Label (_("Time:")), false);
            hour_spinbutton = new SpinButton.with_range (0.0, 23.0, 1.0);
            minute_spinbutton = new SpinButton.with_range (0.0, 45.0, 15.0);
            hm_box.pack_start (hour_spinbutton);
            hm_box.pack_start (minute_spinbutton);
            desc_hm_box.pack_start (hm_box, false);
            
            /* Select the time that is now */
            hour_spinbutton.set_value (new DateTime.now_local ().get_hour ());
            
            /* Create the field for the description */
            description_textview = new TextView ();
            var scroll_w = new ScrolledWindow (null, null);
            scroll_w.add_with_viewport (description_textview);
            scroll_w.set_shadow_type (ShadowType.ETCHED_OUT);
            desc_hm_box.pack_start (scroll_w);
            description_textview.get_buffer ().set_text (_("Insert the description here..."));
            description_textview.set_wrap_mode (Gtk.WrapMode.WORD_CHAR);
            cal_desc_box.pack_start (desc_hm_box);
            
            /* Make the description view gray until the user select it, then reset
             * the text in the textview and let the user fill the description. 
             * 
             * FIXME: We are assuming here that the default background color is black, and
             * that's not always the case. I don't get why vala doesn't allow reset_color
             * to be null that, following Gtk documentation, should reset the color to
             * its default value.
             */
            if (event == null) {
                Gdk.RGBA hint_color = Gdk.RGBA ();
                hint_color.parse ("#bcbcbc");
                description_textview.override_color (StateFlags.NORMAL, hint_color);
                
                /* When the user insert some text the color has to be reset, so the text
                 * doesn't seem a hint anymore */
                description_textview.focus_in_event.connect ((widget, event) => {
                    if (description_inserted)
                        return false;
                    Gdk.RGBA reset_color = Gdk.RGBA ();
                    reset_color.parse ("#000000");
                    description_textview.override_color (StateFlags.NORMAL, reset_color);
                    description_textview.get_buffer ().set_text ("");
                    description_inserted = true;
                    return false;
                });
            }
            else
                description_inserted = true;
            
            structure_box.pack_start (cal_desc_box);
            
            /* Adding entries to select the patient and the visit */
            var patient_visit_box = new Grid ();
            
            var patient_label = new Label(_("Patient"));
            patient_entry = new PatientEntry (resource_manager);
            patient_visit_box.attach (patient_label, 0, 0, 1, 1);
            patient_label.vexpand = true; patient_label.hexpand = false;
            patient_visit_box.attach (patient_entry, 1, 0, 1, 1);
            patient_entry.expand = true;
            patient_entry.margin_left = resource_manager.PADDING;;
            structure_box.pack_start (patient_visit_box);
                
            /* Pack the table in the dialog, with some alignment */
            var alignment = new Alignment (0.5F, 0.5F, 1.0F, 1.0F);
            alignment.set_padding (resource_manager.PADDING, resource_manager.PADDING, 
                resource_manager.PADDING, resource_manager.PADDING);
            alignment.add (structure_box);
            var content_area = get_content_area ();
            content_area.add (alignment);
            alignment.show_all ();
            
            set_size_request (520, -1);
            
            response.connect (on_response);
            
            /* Fill the data if the selected event is not null */
            if (event != null) {
                title_entry.set_text (selected_event.title);
                venue_entry.set_text (selected_event.venue);
                description_textview.get_buffer ().set_text (selected_event.description);
                
                /* Calendar date */
                calendar.select_month (selected_event.date.get_month () - 1, 
                                       selected_event.date.get_year ());
                calendar.select_day (selected_event.date.get_day_of_month ());
                
                /* And the right time  */                
                hour_spinbutton.set_value (selected_event.date.get_hour ());
                minute_spinbutton.set_value (selected_event.date.get_minute ());
                
                /* The patient if exists */
                if (event.patient != null) {
                    select_patient (event.patient);
                }
            }
        }
        
        /**
         * @brief Create a new event editor with a preselected date in the calendar.
         */
        public EventEditor.with_date (ResourceManager resources, DateTime date) {
            this (resources);
            calendar.select_month (date.get_month () - 1, date.get_year ());
            calendar.select_day (date.get_day_of_month ());
        }
        
        /**
         * @brief Change selection to the given patient
         */
        public void select_patient (Patient patient) {
            patient_entry.select_patient (patient);
            if (selected_event == null)
                title_entry.set_text (_("Visit"));
        }
        
        private void on_response () {
            selected_event.title = title_entry.get_text ();
            selected_event.venue = venue_entry.get_text ();
            
            TextIter start, end;
            TextBuffer buffer = description_textview.get_buffer ();
            buffer.get_start_iter (out start);
            buffer.get_end_iter (out end);
            if (description_inserted)
                selected_event.description = buffer.get_text (start, end, false);
            else
                selected_event.description = "";
                
            /* Select patient if is valid */
            if (patient_entry.is_valid () && patient_entry.get_patient () != null) {
                selected_event.patient = patient_entry.get_patient ();
            }
            
            selected_event.date = new DateTime.local (calendar.year, calendar.month + 1,
                calendar.day, hour_spinbutton.get_value_as_int (), 
                minute_spinbutton.get_value_as_int (), 0);
        }
    }
 }
