/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

    public class PatientListPage : Alignment, Page {
    
        public  PatientListView view;
        private Box left_vbox;
        private ScrolledWindow scrolled_window;
        
        public ResourceManager resource_manager { get; set; }
        
        private SidebarEntry sidebar_entry;
        
        /**
         * @brief PatientListPage is the Page loaded when the user
         * asks for a list of the Patients in the database 
         */
        public PatientListPage (ResourceManager resources) {
            resource_manager = resources;
            
            /* Load files from the XML */
            var builder = new Builder (resources);
            
            try {
                builder.add_from_resource ("patient_list_toolbar.glade");
            } catch (Error e) {
                error ("Error opening patient_list_toolbar.glade. Check you installation.\n");
            }
            
            left_vbox = builder.get_object ("left_vbox") as Box;
            scrolled_window = builder.get_object ("treeview_scrolled_window") as ScrolledWindow;
            
            view = new PatientListView (resource_manager, builder.get_object ("find_entry") as Entry);
            scrolled_window.add (view);
            
            add (left_vbox);
            show_all ();
            
            /* Connect XML specified signals */
            builder.connect_signals (this);
            
            /* Create the SidebarEntry */
            sidebar_entry = new SidebarPageEntry (resource_manager, this, _("Patients"), 
                Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/patients.svg", 
                Sidebar.ICON_SIZE, Sidebar.ICON_SIZE));
                
            /* Create the Toolbar */
            var toolbar = new Toolbar (resource_manager);
            populate_toolbar (toolbar);
            var top_box = builder.get_object ("top_box") as Box;
            
            top_box.pack_start (toolbar, false, true, 0);
            show_all ();
        }
        
        private void populate_toolbar (Toolbar toolbar) {
            var add_item = new ToolbarItem (resource_manager,
                Config.RESOURCE_BASE + "ui/icons/small_plus.png");
            add_item.activated.connect (on_add_button_clicked);
            add_item.set_tooltip_text (_("Add a new patient"));
            toolbar.insert (add_item, -1);
            
            var remove_item = new ToolbarItem (resource_manager,
                Config.RESOURCE_BASE + "ui/icons/small_remove_2.png");
            remove_item.activated.connect (on_remove_button_clicked);
            remove_item.set_tooltip_text (_("Remove the selected patient"));
            toolbar.insert (remove_item, -1);
            
            var modify_item = new ToolbarItem (resource_manager,
                Config.RESOURCE_BASE + "ui/icons/small_pencil.png");
            modify_item.activated.connect (on_modify_button_clicked);
            modify_item.set_tooltip_text (_("Modify the selected patient"));
            toolbar.insert (modify_item, -1);
            
            var visit_item = new ToolbarItem (resource_manager,
                Config.RESOURCE_BASE + "ui/icons/small_notes_2.png");
            visit_item.activated.connect (on_edit_visit_button_clicked);
            visit_item.set_tooltip_text (_("Edit visits of the selected patient"));
            toolbar.insert (visit_item, -1);
            
            var schedule_visit = new ToolbarItem (resource_manager,
                Config.RESOURCE_BASE + "ui/icons/small_calendar.png");
            schedule_visit.activated.connect (on_schedule_a_visit_button_clicked);
            schedule_visit.set_tooltip_text (_("Schedule a visit with the selected patient"));
            toolbar.insert (schedule_visit, -1);
        }
        
        public string get_title () {
            return _("Patients");
        }
        
        public SidebarEntry? get_sidebar_entry () {
            return sidebar_entry;
        }
        
        /**
         * @brief Callback for the remove_button
         */
        private void on_remove_button_clicked (ToolbarItem item) {
        	view.remove_selected_patient ();
        }
        
        /**
         * @brief Callback for the add_button
         */
        private void on_add_button_clicked (ToolbarItem item) {
        	var new_patient_dialog = new PatientEditor (resource_manager);
        	new_patient_dialog.set_transient_for (resource_manager.user_interface.window);
        	new_patient_dialog.run ();
      		new_patient_dialog.destroy ();
        }
        
        private void on_modify_button_clicked (ToolbarItem item) {
        	var patient = view.get_selected_patient ();
        	if (patient == null)
        		return;
        	
        	/*  Create the patient browser */
        	var edit_patient_dialog = new PatientEditor.with_patient (resource_manager, patient);
        	edit_patient_dialog.set_transient_for (resource_manager.user_interface.window);
        	edit_patient_dialog.run ();
        	edit_patient_dialog.destroy ();
        }
        
        /**
         * @brief Callback for the Edit visits button that opens the default
         * VisitBrowser for the selected patient.
         */
        private void on_edit_visit_button_clicked (ToolbarItem item) {	
        	var patient = view.get_selected_patient ();
        	if (patient == null) {
        		return;
        	}
        	
            resource_manager.user_interface.show_visit_window (patient);
        }
        
        /**
         * @brief Callback for the Schedule a visit button, that schedule a new visit
         * with the selected patient.
         */
        private void on_schedule_a_visit_button_clicked (ToolbarItem item) {
            var patient = view.get_selected_patient ();
            var scheduler = new VisitSchedulerDialog (resource_manager, patient);
            scheduler.run ();
            scheduler.destroy ();
        }
    }
    
}
