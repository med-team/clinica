/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

using Gtk;

namespace Clinica {

    public class MedicineDetailDialog : Dialog {
    
        /**
         * @brief Medicine of which the details should be shown
         * in the dialog.
         */
        private Medicine medicine { get; set; }
        
        /**
         * @brief Reference to the resource manager used in the
         * Clinica application
         */
        private ResourceManager resource_manager { get; set; }
        
        /**
         * @brief Left margin used for labels.
         */
        private int left_margin = 15;
    
        public MedicineDetailDialog (ResourceManager resources, Medicine m) {
            /* Create the object and add the close button used
             * to dismiss the dialog */
            add_button (_("Close"), 0);
            medicine = m; resource_manager = resources;
            var content = get_content_area () as Box;
            set_modal (true);
            
            /* Create the content area with a title on top */
            title = medicine.name;
            var hbox = new Box (Orientation.HORIZONTAL, 6);
            hbox.pack_start (new Image.from_pixbuf (
                Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/search.svg", 64, 64)), false);
            var title_label = new Label ("");
            title_label.set_markup ("<big>" + medicine.name + "</big>");
            hbox.pack_start (title_label);
            content.pack_start (hbox, false, true, resource_manager.PADDING);
            
            
            var general_info_label = new Label ("");
            general_info_label.set_alignment (0.0F, 0.5F);
            general_info_label.set_markup (
                "\n<big><b>" + _("General information") + 
                "</b></big>");
            general_info_label.set_alignment (0.0f, 0.5f);
            content.pack_start (general_info_label, false, true, resource_manager.PADDING);
            
            /* And now the content for the others fields of the medicine */
            content.pack_start (do_label (_("Identification Code"), medicine.id));
            content.pack_start (do_label (_("Description"), medicine.description));
            content.pack_start (do_label (_("Active ingredient"), medicine.active_ingredient));
            content.pack_start (do_label (_("Storage reccomendations"), medicine.storage_reccomendations));
            content.pack_start (do_label (_("Price"), medicine.price));
            
            /* And finally other notes left by the search engine, if they are
             * present */
            if (medicine.other_notes != null) {
                var other_notes_label = new Label ("");
                other_notes_label.set_alignment (0.0F, 0.5F);
                other_notes_label.set_markup (
                    "\n<big><b>" + _("Additional notes") + 
                    "</b></big>");
                other_notes_label.set_alignment (0.0f, 0.5f);
                content.pack_start (other_notes_label, false, true, resource_manager.PADDING);
                
                var content_label = new Label ("");
                content_label.set_markup (medicine.other_notes);
                content_label.set_alignment (0.0f, 0.5f);
                content_label.set_margin_left (left_margin);
                content.pack_start (content_label, false, true);
            }
            
            /* Show all widget packed until now */
            content.set_margin_left (resource_manager.PADDING);
            content.set_margin_right (resource_manager.PADDING);
            content.set_margin_bottom (resource_manager.PADDING);
            show_all ();
        }
        
        private Label do_label (string field, string? desc) {
            var l = new Label ("");
            
            /* Set some left margin */
            l.set_alignment (0.0F, 0.5F);
            l.set_margin_left (left_margin);
            if (desc == null) {
                desc = _("Field empty");
            }
            l.set_markup ("<b>%s:</b> %s".printf (field, desc));
            return l;
        }
    }
}
