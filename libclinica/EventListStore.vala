/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 using Gtk;
 
 namespace Clinica {
    
    public class EventListStore : ListStore {
    
        /**
         * @brief Fields present in the store, i.e. header of
         * the columns.
         */
        enum Field {
            EVENT,
            TITLE,
            DESCRIPTION,
            VENUE,
            PATIENT,
            VISIT,
            DATE,
        }
    
        /**
         * @brief This signal is emitted if an error occur in the
         * list_store
         */
        public signal void error (string message);
        
        /**
         * @brief ResourceManager associated to this clinica application
         * that will be used to retrieve various informationa about the
         * other components running
         */
        private ResourceManager resource_manager { get; set; }
    
        public EventListStore (ResourceManager resources) {
            resource_manager = resources;
            error.connect ((widget, message) => resource_manager.error_callback (widget, message));
            
            Type [] column_headers = { typeof (Event), typeof (string), typeof (string), 
                typeof (string), typeof (Patient?), typeof (Visit?), typeof (DateTime) };
                
            set_column_types (column_headers);
            load_data ();
            
	        /* Setup callbacks to add or remove events */
	        resource_manager.data_provider.event_added.connect (
	            (id) => add_event (id));
	        resource_manager.data_provider.event_changed.connect (
	            (id) => reload_event (id));
	        resource_manager.data_provider.event_removed.connect (
	            (id) => remove_event (id));
        }
        
        public void load_data () {
            GLib.Idle.add (load_events_from_db);            
        }
        
        /**
         * @brief Reload information about an event
         */
        private void reload_event (int64 id) {
            var event = resource_manager.data_provider.get_event (id);
            TreeIter iter = id_to_iter (id);
            
            /* Set values in the treeview */
            set_value (iter, Field.EVENT, event);
            set_value (iter, Field.DATE, event.date);
            set_value (iter, Field.DESCRIPTION, event.description);
            set_value (iter, Field.PATIENT, event.patient);
            set_value (iter, Field.TITLE, event.title);
            set_value (iter, Field.VENUE, event.venue);
            set_value (iter, Field.VISIT, event.visit);
        }
        
        /**
         * @brief Add an event to the liststore.
         */
        public void add_event (int64 id) {
            var event = resource_manager.data_provider.get_event (id);
            TreeIter iter;
            append (out iter);
            
            /* Set values in the treeview */
            set_value (iter, Field.EVENT, event);
            set_value (iter, Field.DATE, event.date);
            set_value (iter, Field.DESCRIPTION, event.description);
            set_value (iter, Field.PATIENT, event.patient);
            set_value (iter, Field.TITLE, event.title);
            set_value (iter, Field.VENUE, event.venue);
            set_value (iter, Field.VISIT, event.visit);
        }
        
        public void remove_event (int64 id) {
            remove (id_to_iter (id));
        }
        
        private TreeIter id_to_iter (int64 id) {
            TreeIter iter;
            Value value;
            
            if (!get_iter_first (out iter)) {
                error (_("Events database seems corrupted. This is likely to be a bug in the application"));
            }
            
            do {
                get_value (iter, Field.EVENT, out value);
                if ((value as Event).id == id) {
                    return iter;
                }
            } while (iter_next (ref iter));
            
            return iter;
        }
        
        /**
         * @brief Function to be schedule with GLib.Idle_add () when loading of
         * the events from the db is needed at the start of the application.
         *
         * This function is now called by default from the constructor.
         */
        private bool load_events_from_db () {
            foreach (var event in resource_manager.data_provider.events ()) {
                add_event (event.id);
            }
            
            return false;
        }
    }
 }
