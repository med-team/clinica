/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
namespace Clinica {

    public class ToolbarItem : Gtk.ToolItem {
    
        public signal void activated ();
        
        private Gtk.Button button;
    
        public ToolbarItem (ResourceManager resources, string resource_path) {
            Object ();
            
            button = new Gtk.Button ();
            button.add (new Gtk.Image.from_resource (resource_path));
            button.set_relief (Gtk.ReliefStyle.NONE);
            
            button.clicked.connect ((button) => activated ());
            
            add (button);
        }
        
        public void set_active (bool sensitive) {
            button.set_sensitive (sensitive);
        }
        
        
    
    }

}
