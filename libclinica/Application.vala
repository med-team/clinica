/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
 namespace Clinica {
 
    /**
     * @brief The class of the clinica application running on the
     * system.
     */
    public class Application : Gtk.Application {
    
        internal ResourceManager resource_manager;
        
        internal UserInterface user_interface;
        
        internal Service service;

        internal DBusConnection connection = null;

        public static string [] args;

        public static bool daemon_mode;
        public static bool version;
        private static string program_name;
        private static bool logging_enabled;
        private static bool new_patient;
        private static bool open_calendar;

        public static const OptionEntry [] option_entries = {
            { "version", 'v', 0, OptionArg.NONE, ref version, "Print version and exit", null },
            { "daemon", 'd', 0, OptionArg.NONE, ref daemon_mode, "Run clinica in daemon mode", null },
            { "debug", 'l', 0, OptionArg.NONE, ref logging_enabled, "Enable debugging", null },
            { "new-patient", 'n', 0, OptionArg.NONE, ref new_patient, "Create a new patient", null },
            { "calendar", 'c', 0, OptionArg.NONE, ref open_calendar, "Open the calendar window", null },
            { null }
        };
        
        public Application (string [] args) {
            GLib.Object (application_id: "org.phcteam.clinica", 
                         flags: ApplicationFlags.HANDLES_COMMAND_LINE);
            
            /* Connect the activate signal to the startup function */
            this.activate.connect (do_activate);
            this.command_line.connect (on_command_line);
        }
        
        private void new_window () {
            /* Start interface and connect error signal */
            user_interface = new UserInterface (resource_manager);
            
            /* Actually start the application */
            user_interface.start ();
            
            /* Add the Window to the ones managed by this application */
            add_window (user_interface.window);
        }
        
        private void on_bus_activation (DBusConnection conn) {
            connection = conn;
            try {
                conn.register_object (Config.RESOURCE_BASE + "data", 
                                       service);              
            } catch (IOError err) {
                debug ("Error while registering /org/phcteam/clinica/data");
            }
        }
                
        private int on_command_line (ApplicationCommandLine command_line) {
            debug ("Parsing command line arguments");
            
            /* Set defaults */
            daemon_mode = false;
            version = false;
        
			/* Get an unowned pointer to the command line arguments to make
			 * OptionContext happy */
		    string [] args = command_line.get_arguments ();
			unowned string [] args_ptr = args;
			
			program_name = args[0];
            
            /* Parse options */
            try {
                OptionContext opt_context = new OptionContext ("- medical records manager");
                opt_context.set_help_enabled (true);
                opt_context.add_main_entries (option_entries, "clinica");
				opt_context.parse (ref args_ptr); 
            } catch (OptionError e) {
                command_line.printerr (_("Option parsing failed: %s\n").printf (e.message));
            }
            
            // If clinica is launched with a --debug switch than enable the debugging
            // handler. 
            if (logging_enabled) {
                Log.set_default_handler ((domains, level, message) => {
                    string color_format;
                    string level_name;                    
                    level_name = level.to_string ();
                    if (level_name == null) {
                        print ("%s\n", message);
                        return;
                     }
                     if ("G_LOG_LEVEL_" in level_name)
                        level_name = level_name.replace ("G_LOG_LEVEL_", "");
                    switch (level) {
                        case LogLevelFlags.LEVEL_INFO:
                            color_format = "34";
                            break;
                        case LogLevelFlags.LEVEL_DEBUG:
                            color_format = "32";
                            break;
                        case LogLevelFlags.LEVEL_WARNING:
                            color_format = "33";
                            break;
                        default:
                            color_format = "31";
                            break;
                    }
                    print ("[\033[%s;1m%s\033[0m] %s\n", color_format, level_name, message);
                });
            }
            
            if (version) {
                command_line.print(_("Clinica %s\n").printf (Config.PACKAGE_VERSION));
				return 0;
            }
            
			/* If we got there we need to activate Clinica */
			activate ();
			return 0;
        }
        
        internal void start_dbus () {
            if (service != null)
                return;
                
            debug ("Activating ClinicaService on DBus");
            resource_manager.application.hold ();
            service = new Service (resource_manager);
            /* Launch the DBus service used by clinica */
            Bus.own_name (BusType.SESSION, "org.phcteam.clinica", 
                BusNameOwnerFlags.NONE, 
                on_bus_activation, () => {},
                () => debug ("Could not acquire bus"));
        }
        
        internal void show_visits (Patient p) {
            if (user_interface == null) {
                new_window ();
                user_interface.start ();
            }
            
            user_interface.show_visit_window (p);
        }
        
        internal void show_user_interface () {
            if (user_interface != null) {
                user_interface.show_main_window ();
                user_interface.window.present ();
                return;
            }
            else
                new_window ();
                
            user_interface.show_main_window ();
        }
        
        public void do_activate () {
            bool cold_start = false;
            if (resource_manager == null) {
                cold_start = true;
                
                /* Init resource manager and connect error function
                 * to a message display for the user */
                resource_manager = new ResourceManager (this, program_name, Utils.show_error_message);
                
                /* Init gettext translation system */
                Intl.textdomain("clinica");
                
                /* Check where are the translations. At the moment being we are sure that Italian
                 * translations are present, so check for them in the /usr/share/locale/ directory.
                 * If they are not present there, type with /usr/local, and if not present there, 
                 * try with autodetection. */
                if (FileUtils.test("/usr/share/locale/it/LC_MESSAGES/clinica.mo", FileTest.IS_REGULAR))
	                Intl.bindtextdomain("clinica", "/usr/share/locale");
	            else if (FileUtils.test("/usr/local/share/locale/it/LC_MESSAGES/clinica.mo", FileTest.IS_REGULAR))
	            	Intl.bindtextdomain("clinica", "/usr/local/share/locale");
	            else
	            	Intl.bindtextdomain("clinica", null);

              debug ("Starting the DBus Clinica Service");
	          start_dbus ();
            }
            
            /* If daemon mode is not enable show the main user interface */
            if (!daemon_mode) {
                debug ("Showing Clinica user interface");
                show_user_interface ();
            }
            else {
                resource_manager.persistency_required = true;
            }
            
            /* If the user asks for the calendar window open it */
            if (open_calendar) {
                debug ("Opening calendar window");
                user_interface.show_calendar_window ();
                open_calendar = false;
            }

            /* If the user asked for a new Patient display the dialog to create one */
            if (new_patient) {
                var patient_editor = new PatientEditor (resource_manager);
                patient_editor.run ();
                patient_editor.destroy ();
                new_patient = false;
            }
        }
    }
 }
