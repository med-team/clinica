/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */
 
 namespace Clinica {
    
    public class DateTimePicker : Gtk.EventBox, Buildable, Gtk.Buildable {
    
        private Gtk.Calendar calendar;
        
        private Gtk.SpinButton hour_spinbutton;
        private Gtk.SpinButton minute_spinbutton;
        
        public void setup (ResourceManager resources) {
        }
    
        public DateTimePicker () {
        }
        
        construct {
            var vbox = new Gtk.Box (Gtk.Orientation.VERTICAL, 6);
            
            calendar = new Gtk.Calendar ();
            vbox.pack_start (calendar, true, true);
            
            /* We need an additional box to pack the hour
             * and minute spinbuttons together with their
             * labels. */
            var box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
            
            hour_spinbutton = new Gtk.SpinButton.with_range (-1.0, 234.0, 1.0);
            minute_spinbutton = new Gtk.SpinButton.with_range (-1.0, 60.0, 15.0);
            
            hour_spinbutton.set_value (new DateTime.now_local ().get_hour ());
            minute_spinbutton.set_value (0.0);
            
            hour_spinbutton.changed.connect (on_hour_spinbutton_changed);
            minute_spinbutton.changed.connect (on_minute_spinbutton_changed);
            
            box.pack_start (new Gtk.Label (_("Hour")), false, true);
            box.pack_start (hour_spinbutton, true, true, 6);
            box.pack_start (new Gtk.Label (_("Minute")), false, true, 6);
            box.pack_start (minute_spinbutton, true, true);
            
            vbox.pack_start (box, true, true);
            add (vbox);
            
            calendar.set_margin_bottom (6);
            
            show_all ();
        }
        
        private void on_hour_spinbutton_changed (Gtk.Editable spin) {
            if (hour_spinbutton.get_value () == 24.0)
                hour_spinbutton.set_value (0.0);
            if (hour_spinbutton.get_value () == -1.0)
                hour_spinbutton.set_value (23.0);
        }
        
        private void on_minute_spinbutton_changed (Gtk.Editable spin) {   
            if (minute_spinbutton.get_value () == 60.0)
                minute_spinbutton.set_value (0.0);
            if (minute_spinbutton.get_value () == -1.0)
                minute_spinbutton.set_value (59.0);
        }
        
        public void set_date (DateTime date) {
            calendar.select_month (date.get_month () - 1, date.get_year ());
            calendar.select_day (date.get_day_of_month ());
            
            hour_spinbutton.set_value (date.get_hour ());
            minute_spinbutton.set_value (date.get_minute ());
        }
        
        public DateTime get_datetime() {
                uint year, month, day;
                calendar.get_date (out year, out month, out day);
                
                /* We need a plus one offset on months since are represented differently
                 * in calendar and DateTime. */
                var date = new DateTime.local ((int) year, (int) month + 1, (int) day, 0, 0, 0);
                
                date = date.add_hours (hour_spinbutton.get_value_as_int ());
                date = date.add_minutes (minute_spinbutton.get_value_as_int ());
                
                return date;
        }
    
    }
    
 }
