/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
namespace Clinica {

    public class SidebarCalendarEntry : Object, SidebarEntry {
    
        private Gdk.Pixbuf _pixbuf;
        private string _title;
    
        public SidebarCalendarEntry (ResourceManager resources) {
            _pixbuf = Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/calendar.svg", 
            Sidebar.ICON_SIZE, Sidebar.ICON_SIZE);
            _title = _("Calendar");
            activated.connect ((entry) => 
                resources.user_interface.show_calendar_window ());
        }
        
        public string title () { return _title; }
        
        public Gdk.Pixbuf? pixbuf () { return _pixbuf; }
    
    }

}
