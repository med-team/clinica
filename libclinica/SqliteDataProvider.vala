/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

using Sqlite;
using Gtk;

namespace Clinica {

    public class SqliteDataProvider : Object, DataProvider {
    
        public signal void error (string message);
    
        /**
         * @brief Name of the table in the database where the visits
         * will be stored. Please note that changing this will require
         * a database upgrade, so should be handled correctly.
         */
        internal string visit_table = "visits";
        
        /**
         * @brief Name of the table in the database where the patients
         * will be stored. Please note that changing this will require
         * a database upgrade, so should be handled correctly.
         */
        internal string patient_table = "patients";
        
        /**
         * @brief Name of the table in the database where the doctors
         * will be stored. Please note that changing this will require
         * a database upgrade, so should be handled correctly.
         */
        internal string doctor_table = "doctors";
        
        /**
         * @brief Name of the table in the database where the events
         * will be stored. Please note that changing this will require
         * a database upgrade, so should be handled correctly.
         */
        internal string event_table = "events";
        
        /**
         * @brief The absolute path of the database (typically on Linux systems
         * will be ~/.local/share/clinica/clinica.db.
         */
        private string database_path;
        
        /**
         * @brief The ResourceManager of this instance of the Clinica Application.
         */
        private ResourceManager resource_manager { get; set; }
        
        /**
         * @brief The Sqlite.Database object - this is used to interact with the database
         * by the SqliteDataProvider and all the Sqlite*Iterator classes that are generated
         * from here.
         */
        internal Database db;
        
        /**
         * @brief The FileStore object associated to this DataProvider.
         *
         * In our case the implementation provided in LocalFileStore is
         * used.*/
        private FileStore file_store;
        
        /**
         * @brief An array containing the supported versione of the db. This should
         * contain the value db_version and means that this SqliteProvider is capable
         * of Database migration from any of these old version to the most recent one.
         */
        private const string [] supported_db_versions = { "0.1", "0.2" };
        
        /**
         * @brief The version of the database that is in use now, or that should
         * be used to compatbile with this SqliteDataProvider.
         *
         * Please note that Database version are not the same of Clinica versions.
         * Typically the same clinica versions will use the same Database version,
         * but the other way is not always true.
         */
        private const string db_version = "0.2";
    
        public SqliteDataProvider (ResourceManager resources) {
            resource_manager = resources;
            error.connect ((t,l) => warning (l));
            
            file_store = new LocalFileStore (resource_manager);
            
            /* Find the database in all the possible paths for the various
             * versions of Clinica, and then possibly do un upgrade, querying
             * the user if necessary. */
            check_database_path ();
            check_updates ();
            
            /* This is used to create the tables if they do not exists */
            init_resources ();
        }
        
        public FileStore? get_file_store () {
            return file_store;
        }
        
        /**
         * @brief Get the identifier for this DataProvider to be displayed
         * to the user.
         *
         * It has not to be translated since it will be used to detect which
         * will be used on startup.
         */
        public string get_name () { return "Local database"; }
        
        /**
         * @brief Check if there is the need to upgrade the database from on of
         * the supported version to the most recent one.
         */
        private void check_updates () {                    
            /* Read database version if existant, saved actual version otherwise.
             * This may be used in future to perform one-time upgrade of the database */
            var version_db_file = Path.build_filename (resource_manager.local_config_directory, 
            										   ".clinica_db_version");
            										   
        	if (!FileUtils.test (version_db_file, FileTest.IS_REGULAR)) {
        		/* Create the file with the version of the database */
        		try {
	        		FileUtils.set_contents(version_db_file, db_version);
	        	} catch (Error e) {
	        		error (_("Error creating some configuration files, check permission on %s").printf (version_db_file));
	        	}
        	}
        	else {
        		string content;
        		try {
        			FileUtils.get_contents(version_db_file, out content);
        		} catch (Error e) {
        			error (_("Error reading some configuration files, check permission on %s".printf(version_db_file)));
        		}
        		
        		bool upgraded_correcty = true;
        		if (db_version != content) {
        		    /* Ask the user if he really wants to upgrade */
        		    if (!ask_for_upgrade()) {
        		        debug ("The user doesn't want to upgrade, exiting.");
        		        Posix.exit (0);
        		    }
        		
        		    upgraded_correcty = false;
        		    /* Try to determine if the version of the local database is supported */
        		    if (content in supported_db_versions) {
        		        if (upgrade_database (content)) {
        		            upgraded_correcty = true;
        		            try {
            		            FileUtils.set_contents (version_db_file, db_version);
            		        } catch (GLib.Error e) {
            		            error (_("Failure while settings new database version to %s").printf (db_version));
                            }
        		        } else {
        		            error (_("Failure while upgrading database"));
        		        }
        		    }
        		    else
        			    error (_("Version of the database is not compatible"));
        		}
        		
        		if (!upgraded_correcty) {
      		        debug ("Upgrade failed, exiting.");
        		    Posix.exit (1);
        		}
        	}
        }
        
        /**
         * @brief This routine is used to ask confirmation to the user for the database
         * upgrade. Since the upgrade will make the older version of clinica unusable, we
         * should always ask before performing one.
         */
        private bool ask_for_upgrade () {
            var mb = new MessageDialog (null,
                DialogFlags.DESTROY_WITH_PARENT |
                DialogFlags.MODAL,
                MessageType.QUESTION,
                ButtonsType.YES_NO,
                "%s", "Database upgrade");
            mb.format_secondary_markup (_("This is a version of Clinica newer than the one that created the\npatients database installed on the system.\nUsing this version requires upgrading the database, and\n<b>the old version will not be able to use it anymore</b>.\nDo you wish to perform this one-time upgrade?\n"));
            mb.set_title (_("Upgrade database"));
            if (mb.run () == ResponseType.YES) {
                mb.destroy ();
                return true;
            }
            else {
                mb.destroy ();
                return false;
           }
        }
        
        /**
         * @brief Check if the database is in the right path. If it is not there
         * move it and the set database_path.
         */
        private void check_database_path () {
            database_path = Path.build_filename (resource_manager.local_data_directory,
                                                 "clinica.db");
            File db_file = File.new_for_path (database_path);
            if (!db_file.query_exists ()) {
                File old_db_file = File.new_for_path (Path.build_filename (resource_manager.local_config_directory, 
                        "clinica.db"));
                if (old_db_file.query_exists ()) {
                
                    var mb = new MessageDialog (null, DialogFlags.DESTROY_WITH_PARENT | DialogFlags.MODAL,
                        MessageType.QUESTION, ButtonsType.YES_NO, "%s", _("Database needs to be moved"));
                    mb.format_secondary_markup (_("An older version of clinica has been detected and the old database has to be moved\nto a new location to continue.\n<b>The older version of clinica won't work anymore with this setup</b>.\nDo you still want to continue?"));
                    if (mb.run () != Gtk.ResponseType.YES) {
                        mb.destroy ();
                        Posix.exit (1);
                    }
                    
                    mb.destroy ();
                
                    debug ("Moving old database to its new location (starting from clinica 0.2.9)");
                    try {
                        old_db_file.copy (db_file, FileCopyFlags.ALL_METADATA);
                        FileUtils.remove (old_db_file.get_path ());
                    } catch (GLib.Error e) {
                        error (_("Error while transferring the database to the new default location for clinica >= 0.2.9"));
                    }
                }
            }
        }

        /**
         * @brief Do upgrade the database starting from db_version.
         */
        private bool upgrade_database (string db_version) {
            /* Try to open the database first */
            if (!(Database.open (database_path, out db) == OK)) {
                error (_("Error upgrading database, please check your installation"));
            }
            
            string sql;
            Statement stmt;
            int64 rc;
            
            /* Performing upgrade from 0.1 */
            if (db_version == "0.1") {
                /* We need to create the new table for the events */
               sql = "CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY, title TEXT, description TEXT, venue TEXT, patient INTEGER, visit INTEGER, date TEXT);";
               db.prepare (sql, -1, out stmt, null);
               rc = stmt.step ();
               
               if (rc != DONE) {
                   error (_("Error upgrading the database from version 0.1 to 0.2, sqlite exit code: %s".printf (rc.to_string ())));
                   return false;
               } else {
                   db_version = "0.2";
               }
            }
            
            return true;
        }
        
        /**
         * @brief Check that all the resources needed by Clinica
         * are actually available, and if they are not create them.
         */
        public void init_resources () {
            /* Open database and, if it does not exists, create it */
            if (!(Database.open (database_path, out db) == OK)) {
                error ("Error opening database.");
            };
            
            /* Check if the required tables exists, and create them
             * if necessary. */
            init_database ();
        }
        
        
        /**
         * @brief Init the database with the required tables
         */
        private void init_database () {
            
            if (db.exec ("CREATE TABLE IF NOT EXISTS doctors (surname TEXT, ID INTEGER PRIMARY KEY, phone TEXT, mobile TEXT, given_name TEXT)") != 0)
                error ("Error creating the doctors table");
            if (db.exec ("CREATE TABLE IF NOT EXISTS patients (gender TEXT, doctor INTEGER, surname TEXT, ID INTEGER PRIMARY KEY, identification_code TEXT, phone TEXT, given_name TEXT, birth_date TEXT, residence_address TEXT)") != 0)
                error ("Error creating the patients table");
            if (db.exec ("CREATE TABLE IF NOT EXISTS visits (subsequent_checks TEXT, systemic_therapy TEXT, ID INTEGER PRIMARY KEY, laboratory_exam TEXT, diagnosis TEXT, histopathology TEXT, anamnesis TEXT, date TEXT, patient INTEGER, physical_examination TEXT, topical_therapy TEXT)") != 0)
                error ("Error creating the visits table");
            if (db.exec ("CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY, title TEXT, description TEXT, venue TEXT, patient INTEGER, visit INTEGER, date TEXT)") != 0)
                error ("Error creating the events table");
                
        }
            
        /**
         * @brief Internal routine used to convert DateTime objects
         * to strings that are to be saved in the database.
         */
        internal static string datetime_to_string (DateTime date) {
            return string.join(" ", 
        		date.get_year ().to_string (), 
        		"%.2d".printf (date.get_month ()), 
        		"%.2d".printf (date.get_day_of_month()), 
        		"%.2d".printf (date.get_hour ()), 
        		"%.2d".printf (date.get_minute ()), 
        		"%.2d".printf (date.get_second ()));
        }
        
       /**
        * @brief Convert strings encoded with datetime_to_string () back
        * to DateTime objects.
        */
       internal static DateTime? string_to_datetime (string input) {
       
            if (input == "") {
                return null;
            }
       
        	string [] fields = input.split(" ");
        	
        	int year = int.parse (fields[0]);
        	int month = int.parse (fields[1]);
        	int day = int.parse (fields[2]);
        	int hour = int.parse (fields[3]);
        	int minute = int.parse (fields[4]);
        	int seconds = int.parse (fields[5]);
        	
        	if (year < 1 || year > 9999 || 
        		month < 1 || month > 12 ||
        		day < 1 || day > 31 ||
        		minute < 0 || minute > 59 ||
        		seconds < 0 || seconds > 59)
        		return new DateTime.now_local ();
        	
        	return new DateTime.local (year, month, day, hour, minute, seconds);
        }
        
        /* IMPLEMENTATION OF THE ABSTRACT INTERFACES OF DataProvider */
        
        public Doctor? get_doctor (int64 id) {
            var doc = new Doctor ();
            Statement stmt;
            db.prepare (@"SELECT given_name, surname, phone, mobile FROM $(doctor_table) WHERE id=$(id);", -1, 
                out stmt, null);
            if (!(stmt.step () == ROW)) {
                error (_("Error while retrieving the doctor with id = %s").printf (id.to_string (), db.errmsg ()));
                return null;
            }
            
            /* Load data from the database into the fields */
            doc.id = id;
            doc.given_name = stmt.column_text (0).compress ();
            doc.surname = stmt.column_text (1).compress ();
            doc.phone = stmt.column_text (2).compress ();
            doc.mobile = stmt.column_text (3).compress ();
            
            doc.provider = this;
            
            return doc;
        }
        
        public int64 save_doctor (Doctor doctor) {
            Statement stmt;
            
            if (doctor.id != 0)
                db.prepare ("""INSERT OR REPLACE INTO %s (id, given_name, surname, phone, mobile)
                    VALUES (:id, :given_name, :surname, :phone, :mobile);""".printf (doctor_table),
                    -1, out stmt);
            else
                db.prepare ("""INSERT OR REPLACE INTO %s (given_name, surname, phone, mobile)
                    VALUES (:given_name, :surname, :phone, :mobile);""".printf (doctor_table),
                    -1, out stmt);
            
                
            
            /* Bind values present in doctor in the SQL statement */
            int i = 1;
            if (doctor.id != 0)
                stmt.bind_int64  (i++, doctor.id);
            stmt.bind_text (i++, doctor.given_name);
            stmt.bind_text (i++, doctor.surname);
            stmt.bind_text (i++, doctor.phone);
            stmt.bind_text (i++, doctor.mobile);
            
            if (stmt.step () != DONE) {
                error (_("An error occurred while saving the doctor with id %d: %s").printf (doctor.id, db.errmsg ()));
                return -1;
            }
            
            if (doctor.id == 0) {
                doctor.id = db.last_insert_rowid ();
                doctor_added (doctor.id);
            }
            else
                doctor_changed (doctor.id);

            return doctor.id;
        }
        
        public int64 remove_doctor (Doctor doctor) {
            Statement stmt;
            
        	/* Keep a list of the patient ID we need to notify */
        	var patients = new List<int64?> ();
        	foreach (var patient in doctor.patients ()) {
        	    patients.append (patient.id);
        	}
            
            /* Wrap all this in a transaction so the DB doesn't change until
             * we commit */
            db.exec ("BEGIN TRANSACTION");
            
        	string sql = @"DELETE from $(doctor_table) WHERE ID=$(doctor.id);";
        	
        	db.prepare (sql, -1, out stmt, null);
        	if (stmt.step () != DONE) {
        		error (@"Error deleting doctor with id $(doctor.id)");
        		db.exec ("ROLLBACK TRANSACTION");
        		return -1;
        	}
        	
        	/* Now we need to deassociate its patients from him/her */
            if (db.exec (@"UPDATE $(patient_table) SET doctor = 0 WHERE doctor = $(doctor.id);") != 0) {
                error (@"Error deassociating the patients from the doctor with ID = $(doctor.id)");
                db.exec ("ROLLBACK TRANSACTION");
                return -1;
            }
            
        	db.exec ("COMMIT TRANSACTION");
        	
        	doctor_removed (doctor.id);
        	
        	/* Notify changes on the patients */
        	foreach (var patient_id in patients) {
        	    patient_changed (patient_id);
        	}
        	return 0;
        }
        
        public PatientIterator patients (Doctor? doctor = null) {
            return new SqlitePatientIterator (this, (doctor == null) ? "" : @"WHERE doctor = $(doctor.id)");
        }
        
        public DoctorIterator doctors () {
            return new SqliteDoctorIterator (this);
        }
        
        public Patient? get_patient (int64 id) {
            var patient = new Patient ();
            Statement stmt;
            db.prepare ("""SELECT given_name, surname, birth_date, gender, phone, residence_address,
                    identification_code, doctor FROM %s WHERE id=%s;""".printf (patient_table, id.to_string ()), 
                    -1, out stmt, null);
            if (!(stmt.step () == ROW)) {
                error (_("Error while retrieving the patient with id = %s").printf (id.to_string (), db.errmsg ()));
                return null;
            }
            
            /* Load data from the database into the fields */
            patient.id = id;
            patient.given_name = stmt.column_text (0).compress ();
            patient.surname = stmt.column_text (1).compress ();
            patient.birth_date = string_to_datetime (stmt.column_text (2).compress ());
            patient.gender = (stmt.column_text (3) == "MALE") ? Gender.MALE : Gender.FEMALE;
            patient.phone = stmt.column_text (4).compress ();
            patient.residence_address = stmt.column_text (5).compress ();
            patient.identification_code = stmt.column_text (6).compress ();
            patient.doctor = (stmt.column_int64 (7) == 0) ? null : get_doctor (stmt.column_int64 (7));
            
            patient.provider = this;
            
            return patient;
        }
        
        public int64 save_patient (Patient patient) {
            Statement stmt;
            
            if (patient.id != 0)
                db.prepare ("""INSERT OR REPLACE INTO %s
                    (ID, given_name, surname, birth_date, gender, phone, residence_address, identification_code, doctor)
                    VALUES (:id, :given_name, :surname, :birth_date, :gender, :phone, :residence_address, :identification_code, :doctor);""".printf (patient_table),
                    -1, out stmt);
            else
                db.prepare ("""INSERT OR REPLACE INTO %s
                            (given_name, surname, birth_date, gender, phone, residence_address, identification_code, doctor)
                            VALUES (:given_name, :surname, :birth_date, :gender, :phone, :residence_address, :identification_code, :doctor);""".printf (patient_table),
                    -1, out stmt);
            
            
            /* Bind values present in doctor in the SQL statement */
            int i = 1;
            if (patient.id != 0)
                stmt.bind_int64  (i++, patient.id);
            stmt.bind_text   (i++, patient.given_name);
            stmt.bind_text   (i++, patient.surname);
            stmt.bind_text   (i++, datetime_to_string (patient.birth_date));
            stmt.bind_text   (i++, (patient.gender == Gender.MALE) ? "MALE" : "FEMALE");
            stmt.bind_text   (i++, patient.phone);
            stmt.bind_text   (i++, patient.residence_address);
            stmt.bind_text   (i++, patient.identification_code);
            stmt.bind_int64  (i++, (patient.doctor == null) ? 0 : patient.doctor.id);
            
            if (stmt.step () != DONE) {
                error (_("An error occurred while saving the doctor with id %s: %s").printf (patient.id.to_string (), db.errmsg ()));
                return -1;
            }
            
            if (patient.id == 0) {
                patient.id = db.last_insert_rowid ();
                patient_added (patient.id);
            }
            else
                patient_changed (patient.id);
                
            return patient.id;
        }
        
        public int64 remove_patient (Patient patient) {
        	
        	db.exec ("BEGIN TRANSACTION");
        	
        	if (db.exec (@"DELETE from $(patient_table) WHERE ID=$(patient.id)") != 0) {
        		error (@"Error deleting patient with id $(patient.id)");
        		db.exec ("ROLLBACK TRANSACTION");
        		return -1;
        	}
        	
        	/* Try to remove all the visits of the patient */
        	if (db.exec (@"DELETE FROM $(visit_table) WHERE patient=$(patient.id)") != 0) {
        	    error (@"Error deleting the visits associated to the patient");
        	    db.exec ("ROLLBACK TRANSACTION");
        	    return -1;
        	}
        	
        	/* Deassoociate all the events associated with this patient */
        	if (db.exec (@"UPDATE $(event_table) SET patient=0 WHERE patient=$(patient.id)") != 0) {
        	    error (@"Error deassociating the events from the deleted patient");
        	    db.exec ("ROLLBACK TRANSACTION");
        	    return -1;
            }
            
            db.exec ("COMMIT TRANSACTION");
        	
        	patient_removed (patient.id);
        	return 0;
        }
        
        public VisitIterator visits (Patient? patient, DateTime? start = null, DateTime? end = null, 
            DataProvider.SortOrder order = DataProvider.SortOrder.DESCENDING) {
            string sql_clause = (patient == null && start == null && end == null) ? "" : "WHERE";
            
            if (patient != null) {
                sql_clause += @" patient = $(patient.id)";
                if (start != null || end != null)
                    sql_clause += " AND ";
            }
            
            
            string sort = " ORDER BY date ";
            if (order == DataProvider.SortOrder.DESCENDING)
                sort += "DESC";
            else
                sort += "ASC";
             
            if (start == null && end == null)
                return new SqliteVisitIterator (this, sql_clause + sort);    
            else if (start != null && end == null)
                return new SqliteVisitIterator (this, sql_clause + @" date >= '$(datetime_to_string (start))'" + sort);
            else if (start == null && end != null)
                return new SqliteVisitIterator (this, sql_clause + @" date < '$(datetime_to_string (end))'" + sort);
            else
                return new SqliteVisitIterator (this, sql_clause + @" date BETWEEN " +
                                                      @"'$(datetime_to_string (start))' AND '$(datetime_to_string (end))'" + sort);
        }
        
        public Visit? get_visit (int64 id) {
            var visit = new Visit ();
            Statement stmt;
            db.prepare ("""SELECT anamnesis, physical_examination, laboratory_exam, histopathology, diagnosis,
                        topical_therapy, systemic_therapy, subsequent_checks, date, patient
                        FROM %s WHERE id=%s;""".printf (visit_table, id.to_string ()), -1,
                        out stmt, null);
            if (!(stmt.step () == ROW)) {
                error (_("Error while retrieving the visit with id = %s").printf (id.to_string (), db.errmsg ()));
                return null;
            }
            
            /* Load data from the database into the fields */
            visit.id = id;
            visit.anamnesis = stmt.column_text (0).compress ();
            visit.physical_examination = stmt.column_text (1).compress ();
            visit.laboratory_exam = stmt.column_text (2).compress ();
            visit.histopathology = stmt.column_text (3).compress ();
            visit.diagnosis = stmt.column_text (4).compress ();
            visit.topical_therapy = stmt.column_text (5).compress ();
            visit.systemic_therapy = stmt.column_text (6).compress ();
            visit.subsequent_checks = stmt.column_text (7).compress ();
            visit.date = string_to_datetime (stmt.column_text (8).compress ());
            visit.patient = get_patient (stmt.column_int64 (9));
            
            visit.provider = this;
            
            return visit;
        }
        
        public int64 save_visit (Visit visit) {
            Statement stmt;
            if (visit.id != 0)
                db.prepare ("""INSERT OR REPLACE INTO %s 
                    (id, anamnesis, physical_examination, laboratory_exam, histopathology, diagnosis,
                    topical_therapy, systemic_therapy, subsequent_checks, date, patient)
                    VALUES (:id, :anamnesis, :physical_examination, :laboratory_exam, :histopathology, :diagnosis,
                    :topical_therapy, :systemic_therapy, :subsequent_checks, :date, :patient);""".printf (visit_table),
                    -1, out stmt);
            else
                db.prepare ("""INSERT OR REPLACE INTO %s 
                    (anamnesis, physical_examination, laboratory_exam, histopathology, diagnosis,
                    topical_therapy, systemic_therapy, subsequent_checks, date, patient)
                    VALUES (:anamnesis, :physical_examination, :laboratory_exam, :histopathology, :diagnosis,
                    :topical_therapy, :systemic_therapy, :subsequent_checks, :date, :patient);""".printf (visit_table),
                    -1, out stmt);            
            
            /* Bind values present in doctor in the SQL statement */
            int i = 1;
            if (visit.id != 0)
                stmt.bind_int64  (i++, visit.id);
            stmt.bind_text (i++, visit.anamnesis);
            stmt.bind_text (i++, visit.physical_examination);
            stmt.bind_text (i++, visit.laboratory_exam);
            stmt.bind_text (i++, visit.histopathology);
            stmt.bind_text (i++, visit.diagnosis);
            stmt.bind_text (i++, visit.topical_therapy);
            stmt.bind_text (i++, visit.systemic_therapy);
            stmt.bind_text (i++, visit.subsequent_checks);
            stmt.bind_text (i++, datetime_to_string (visit.date));
            stmt.bind_int64  (i++, visit.patient.id);
            
            if (stmt.step () != DONE) {
                error (_("An error occurred while saving the doctor with id %s: %s").printf (visit.id.to_string (), db.errmsg ()));
                return -1;
            }
            
            if (visit.id == 0) {
                visit.id = db.last_insert_rowid ();
                visit_added (visit.id);
            }
            else
                visit_changed (visit.id);
            
            return visit.id;
        }
        
        public int64 remove_visit (Visit visit) {
            Statement stmt;
        	string sql = @"DELETE from $(visit_table) WHERE ID=$(visit.id);";
        	
        	db.prepare (sql, -1, out stmt, null);
        	if (stmt.step () != DONE) {
        		error (@"Error deleting visit with id $(visit.id)");
        		return -1;
        	}
        	
        	visit_removed (visit.id);
        	return 0;
        }
        
        public Event? get_event (int64 id) {
            var event = new Event ();
            Statement stmt;
            db.prepare (@"SELECT title, description, venue, patient, visit, date FROM $(event_table) WHERE id=$(id);", -1, 
                        out stmt, null);
            if (!(stmt.step () == ROW)) {
                error (_("Error while retrieving the event with id = %d").printf (id, db.errmsg ()));
                return null;
            }
            
            /* Load data from the database into the fields */
            event.id = id;
            event.title = stmt.column_text (0).compress ();
            event.description = stmt.column_text (1).compress ();
            event.venue = stmt.column_text (2).compress ();
            event.patient = (stmt.column_int64 (3) != 0) ? get_patient (stmt.column_int64 (3)) : null;
            event.visit = (stmt.column_int64 (4) != 0) ? get_visit (stmt.column_int64 (4)) : null;
            event.date = string_to_datetime (stmt.column_text (5).compress ());
            
            event.provider = this;
            
            return event;
        }
        
        public int64 save_event (Event event) {
            Statement stmt;
            
            if (event.id != 0)
                db.prepare ("""INSERT OR REPLACE INTO %s (id, title, description, venue, patient, visit, date)
                    VALUES (:id, :title, :description, :venue, :patient, :visit, :date);""".printf (event_table),
                    -1, out stmt);
            else
                db.prepare ("""INSERT OR REPLACE INTO %s (title, description, venue, patient, visit, date)
                    VALUES (:title, :description, :venue, :patient, :visit, :date);""".printf (event_table),
                    -1, out stmt);            
            
            /* Bind values present in doctor in the SQL statement */
            int i = 1;
            if (event.id != 0)
                stmt.bind_int64  (i++, event.id);
            stmt.bind_text (i++, event.title);
            stmt.bind_text (i++, event.description);
            stmt.bind_text (i++, event.venue);
            stmt.bind_int64  (i++, (event.patient == null) ? 0 : event.patient.id);
            stmt.bind_int64  (i++, (event.visit == null) ? 0 : event.visit.id);
            stmt.bind_text (i++, datetime_to_string (event.date));
            
            if (stmt.step () != DONE) {
                error (_("An error occurred while saving the event with id %s: %s").printf (event.id.to_string (), db.errmsg ()));
                return -1;
            }
            
            if (event.id == 0) {
                event.id = db.last_insert_rowid ();
                event_added (event.id);
            }
            else
                event_changed (event.id);
            
            return event.id;
        }
        
                
        public int64 remove_event (Event event) {
            Statement stmt;
        	string sql = @"DELETE from $(event_table) WHERE ID=$(event.id);";
        	
        	db.prepare (sql, -1, out stmt, null);
        	if (stmt.step () != DONE) {
        		error (@"Error deleting event with id $(event.id)");
        		return -1;
        	}
        	
        	event_removed (event.id);
        	return 0;
        }
        
        public EventIterator events (DateTime? start = null, DateTime? end = null) {
            string sql_clause = "";
        
            if (start == null && end == null)
                return new SqliteEventIterator (this, sql_clause);
            else if (start != null && end == null)
                return new SqliteEventIterator (this, sql_clause + @" WHERE date >= '$(datetime_to_string (start))'");
            else if (start == null && end != null)
                return new SqliteEventIterator (this, sql_clause + @" WHERE date < '$(datetime_to_string (end))'");
            else
                return new SqliteEventIterator (this, sql_clause + @" WHERE date BETWEEN " +
                                                      @"'$(datetime_to_string (start))' AND '$(datetime_to_string (end))'");
        }
    
    }

}
