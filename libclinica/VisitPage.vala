/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {
	
	public class VisitPage : Alignment, Page {
	
		internal Patient patient;
		private VisitBrowser visit_browser;
		private VisitToolbar  visit_toolbar;

		public signal void error (string message);
		
		public ResourceManager resource_manager { get; set; }
		
		private string title;
		
		private SidebarEntry sidebar_entry;
		
		public VisitPage (ResourceManager resources, Patient p) {
		  	resource_manager = resources;
			error.connect ((t,l) => resource_manager.error_callback (t,l));
		
			patient = p;
			visit_browser = new VisitBrowser (resource_manager, this, patient);
			visit_toolbar = new VisitToolbar (resource_manager, this, patient);
			
			var vbox = new Box (Orientation.VERTICAL, resource_manager.PADDING);
			
			/* Packing widgets */
			vbox.pack_start (visit_toolbar, false, true, 0);
			vbox.pack_start (visit_browser, true, true, 0);
			
			/* Final setup for the window, with an alignment to make padding */
			var alignment = new Alignment (0.5F, 0.5F, 1, 1);
			alignment.add (vbox);
			alignment.set_padding (0, 0, 6, 0);
			add (alignment);
			
			connect_signals ();
			
			/* Set title according to the patient that we have loaded and connect
			 * show startup signal to the focusing of the first field in the visit */
			title = _("Visit of the patient %s").printf (patient.get_complete_name ());
			show_all ();
			this.realize.connect ((widget) => visit_browser.focus_new_page ());
			
			/* Create the SidebarEntry to be showed in the Sidebar */
            sidebar_entry = new SidebarPageEntry (resource_manager, this,
                patient.get_complete_name (), 
                Utils.pixbuf_from_svg_resource (Config.RESOURCE_BASE + "ui/icons/patient.svg", 
                Sidebar.ICON_SIZE, Sidebar.ICON_SIZE));
		}
		
		public string get_title () {
		    return title;
        }
        
        public SidebarEntry? get_sidebar_entry () {
            return sidebar_entry;
        }
        
        /**
         * @brief Return the parent page of this Visit, that will
         * be the PatientListPage, so all Visits will be visible
         * in the sidebar as childs of it. 
         */
        public SidebarEntry? get_parent_entry () {
            return resource_manager.user_interface.pages["patients"].get_sidebar_entry ();
        }
		
		/**
		 * @brief Select the active visit.
		 */
		public void select_visit (Visit visit) {
		    int i;
		    for (i = 0; i < visit_browser.get_n_pages (); i++) {
		        VisitTab nth_visit_tab = visit_browser.get_nth_page (i) as VisitTab;
		        if (nth_visit_tab.get_visit_id () == visit.id) {
		            visit_browser.set_current_page (i);
		            return;
		        }
		    }
		}
		
		private void connect_signals () {
			visit_toolbar.save_visit.connect ((toolbar) => visit_browser.save_focused_visit ());
			visit_toolbar.save_visit_as_pdf.connect ((toolbar) => visit_browser.save_focused_visit_as_pdf ());
			visit_toolbar.print_visit.connect ((toolbar) => visit_browser.print_focused_visit ());
			visit_toolbar.delete_visit.connect ((toolbar) => visit_browser.delete_focused_visit ());
			
			/* Connect the callback of the visit selection to update the visit
			 * actions accordingly. */
			visit_browser.switch_page.connect ((notebook, page, page_id) => {
			        visit_toolbar.visit_actions.set_active_tab (page as VisitTab);
			    });
		}

		public void close () {
			destroy ();
		}
		
	}
}
