/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
 
using Gtk;

namespace Clinica {

    public class MedicineEditor : Gtk.Dialog {
    
        public enum Response {
            SAVE = 0,
            CANCEL
        }
    
        private ResourceManager resource_manager;
    
        private Clinica.Builder builder;        
        
        private LocalMedicineIter? iter;
    
        public MedicineEditor (ResourceManager resources, LocalMedicineIter? iter = null) {
            resource_manager = resources;
            this.iter = iter;
                        
            builder = new Builder.with_filename (resource_manager, "medicine_editor.glade");
            builder.load_into_dialog (this);
            add_buttons (Stock.CANCEL, Response.CANCEL, Stock.SAVE, Response.SAVE);

            if (iter == null)
                set_title (_("Create a new Medicine"));
            else {
                set_title (_("Editing medicine: %s").printf (iter.medicine.name));
                
                (builder.get_object ("name_entry") as Entry).set_text (iter.medicine.name);
                (builder.get_object ("description_entry") as Entry).set_text (iter.medicine.description);
                (builder.get_object ("active_ingredient_entry") as Entry).set_text (iter.medicine.active_ingredient);
                (builder.get_object ("price_entry") as Entry).set_text (iter.medicine.price);
                (builder.get_object ("storage_reccomendations_entry") as Entry).set_text (
                    iter.medicine.storage_reccomendations);
                var view = builder.get_object ("other_notes_textview") as TextView;
                
                view.get_buffer ().set_text (iter.medicine.other_notes);
                
            }
        }
        
        public new Response run () {
            if (base.run () == Response.SAVE) {
            
                var medicine = new Medicine ();
                medicine.name = (builder.get_object ("name_entry") as Entry).get_text ();
                medicine.description = (builder.get_object ("description_entry") as Entry).get_text ();
                medicine.active_ingredient = (builder.get_object ("active_ingredient_entry") as Entry).get_text ();
                medicine.price = (builder.get_object ("price_entry") as Entry).get_text ();
                medicine.storage_reccomendations = (builder.get_object ("storage_reccomendations_entry") as Entry).get_text ();
                
                var other_notes_textview = builder.get_object ("other_notes_textview") as Gtk.TextView;
                var buffer = other_notes_textview.get_buffer ();
                
                Gtk.TextIter start_iter;
                Gtk.TextIter end_iter;
                
                buffer.get_start_iter (out start_iter);
                buffer.get_end_iter   (out end_iter);
                
                medicine.other_notes = buffer.get_text (start_iter, end_iter, false);
            
                if (iter == null)
                    resource_manager.local_medicines_search_engine.add_medicine (medicine);
                else
                    resource_manager.local_medicines_search_engine.update_medicine (iter, medicine);
                
                return Response.SAVE;
            }
            else
                return Response.CANCEL;
        }
    
    }

}
