/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */

using Gtk;

namespace Clinica {
	
	public class VisitBrowser : Notebook {
		
		private Patient patient;
		private VisitTab new_page;
		private VisitListStore store;
		
		ulong new_tab_saved_connection_id;
		
		/**
		 * @brief Pointer to the resource manager used to retrieve
		 * various resources, such as database, stores and similar.
		 */
		private ResourceManager resource_manager;
		
		/**
		 * @brief Window parent of this object.
		 */
	    private VisitPage visit_page;
	    
	    private int old_pages = 0;
	    
	    construct {
	        tab_pos = PositionType.LEFT;
	    }
		
		/**
		 * @brief Browser for the Visits of a Patient
		 */
		public VisitBrowser (ResourceManager resources, VisitPage page, Patient p) {
		
		    resource_manager = resources;
		    visit_page = page;
			
			/* Get the visit store */
			store = resource_manager.visit_list_store;
			
			patient = p;
			
			Label l;
			
			/* Put in future visits */
			foreach (Visit v in p.visits (new DateTime.now_local ())) {
				l = new Label(v.date.format ("%F"));
				append_page (new VisitTab (resource_manager, visit_page, v, l), l);
				old_pages++;
			}
			
			/*  Then add add a new visit and keep a reference to new_page
			 *  to focus it on page loading. */
			l = new Label(_("New visit"));
			new_page = new VisitTab.with_patient (resource_manager, visit_page, patient, l);
			append_page (new_page, l);

			new_tab_saved_connection_id = new_page.saved.connect (on_new_page_saved);
				
			/* And then load all visits for the patient */
			foreach (Visit v in p.visits (null, new DateTime.now_local ())) {
				l = new Label(v.date.format ("%F"));
				append_page (new VisitTab (resource_manager, visit_page, v, l), l);
			}
			
			resource_manager.data_provider.visit_added.connect (on_visit_added);
		}
		
		public void save_focused_visit () {
			int page_id = get_current_page();
			VisitTab tab = get_nth_page (page_id) as VisitTab;
			tab.save_visit ();
		}
		
		public void delete_focused_visit () {
			int page_id = get_current_page();
			VisitTab tab = get_nth_page (page_id) as VisitTab;
			tab.delete_visit ();
		}
		
		/**
		 * @brief This callback gets called when a visit is added, probably
		 * in another VisitBrowser. 
		 */
		private void on_visit_added (int64 visit_id) {
		    /* Get the new visit */
		    Visit new_visit = resource_manager.data_provider.get_visit (visit_id);
            		
		    /* We may be the VisitBrowser that added the visit, or not.
		     * Let's find out traversing the tabs */
		    int i;
		    int n_pages = this.get_n_pages ();
		    
		    for (i = 0; i < n_pages; i++) {
		        VisitTab tab = get_nth_page (i) as VisitTab;
		        int64 visit_tab_id = tab.visit.id;
		        
		        if (visit_tab_id == visit_id) {
		            return;
		        }
		        
		        /* If this is a non-saved visit, than we shall continue */
		        if (visit_tab_id == 0)
		            continue;
		         
		        /* If we have reached the points such that visits are
		         * older than the new one, we have find the place to add
		         * the visit */   
		        if (new_visit.date.compare (tab.visit.date) > 0) {
		            Label l = new Label(new_visit.date.format ("%F"));
		            insert_page (new VisitTab (resource_manager, visit_page, new_visit, l), l, i);
		            show_all ();
		            return;
		        }
		    }
		    
		    /* If we got there we haven't found it, so let's add it to the bottom */
		    Label l = new Label(new_visit.date.format ("%F"));
		    append_page (new VisitTab (resource_manager, visit_page, new_visit, l), l);
		    
		    show_all ();
		}
		
		public void print_focused_visit () {
		    int page_id = get_current_page();
			VisitTab tab = get_nth_page (page_id) as VisitTab;
			
			/* Check if the visit is already in db. If it is, save it, otherwise
			 * ask the user to do it, since it may not be what he desires */
			if (tab.visit.id <= 0) {
			    Gtk.MessageDialog dialog = new MessageDialog.with_markup (null,
			        DialogFlags.MODAL, MessageType.QUESTION, ButtonsType.YES_NO, "%s", 
			        _("A visit need to be saved in order to be exported as PDF.\nDo you want to save the visit now?"));
			    dialog.set_transient_for (resource_manager.user_interface.window);
			    dialog.set_title ("Save is required");
			    if (dialog.run () == ResponseType.YES)
                    dialog.destroy ();
                else {
                    dialog.destroy ();
			        return;
			    }
			}
			
			/* If we got here we should save the visit */
			save_focused_visit ();
			
			/* Create the visit printer and ask the user where the
			 * PDF should be saved. */
			var printer = new VisitPrinter (resource_manager, tab.visit);
			
			/* Create a GtkPrintOperation to handle the printing */
			var operation = new Gtk.PrintOperation ();
			
			/* Connect the callbacks that will actually handle the printing work */
			operation.begin_print.connect ((operation, context) => {
			    // Make a test print to a ImageSurface to understand how many pages do we need
			    var surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, (int) context.get_width (), 
			       (int) context.get_height ());
			    var test_printer = new VisitPrinter (resource_manager, tab.visit);
			    int pages = test_printer.print_report (new Cairo.Context (surface));
			    
			    debug ("%d pages are needed for the print operation", pages);
			    printer.width = context.get_width ();
			    printer.height = context.get_height ();
			    operation.set_n_pages (pages);
			});
			
			operation.draw_page.connect ((operation, context, page_number) => {
			    var ctx = context.get_cairo_context ();
			    printer.handle_pagination = false;
			    printer.print_report (ctx, 1);
            });
			
			try {
    			operation.run (PrintOperationAction.PRINT_DIALOG, resource_manager.user_interface.window);
    	    } catch (GLib.Error e) {
    	        warning (_("Error while delivering the print operation"));
    	    }
		}
		
		public void save_focused_visit_as_pdf () {
		    int page_id = get_current_page();
			VisitTab tab = get_nth_page (page_id) as VisitTab;
			
			/* Check if the visit is already in db. If it is, save it, otherwise
			 * ask the user to do it, since it may not be what he desires */
			if (tab.visit.id <= 0) {
			    Gtk.MessageDialog dialog = new MessageDialog.with_markup (null,
			        DialogFlags.MODAL, MessageType.QUESTION, ButtonsType.YES_NO, "%s", 
			        _("A visit need to be saved in order to be exported as PDF.\nDo you want to save the visit now?"));
			    dialog.set_transient_for (resource_manager.user_interface.window);
			    dialog.set_title ("Save is required");
			    if (dialog.run () == ResponseType.YES)
                    dialog.destroy ();
                else {
                    dialog.destroy ();
			        return;
			    }
			}
			
			/* If we got here we should save the visit */
			save_focused_visit ();
			
			/* Create the visit printer and ask the user where the
			 * PDF should be saved. */
			var printer = new VisitPrinter (resource_manager, tab.visit);
			var fc_dialog = new FileChooserDialog ("Save visit as PDF", null, 
			    FileChooserAction.SAVE, Gtk.Stock.CANCEL, Gtk.ResponseType.CANCEL,
			    Gtk.Stock.SAVE, Gtk.ResponseType.ACCEPT);
			fc_dialog.set_transient_for (resource_manager.user_interface.window);
			fc_dialog.set_current_name ("%s on %s.pdf".printf (tab.visit.patient.get_complete_name (),
			                                                   tab.visit.date.format ("%F")));
			if (fc_dialog.run () != ResponseType.ACCEPT) {
			    fc_dialog.destroy ();
			    return;
			}
			
			var filename = fc_dialog.get_filename ();
			fc_dialog.destroy ();
			printer.print_report_to_pdf (filename);
		}
		
		/**
		 * @brief Callback called when the new page is saved
		 * that adds a new "new page".
		 */
		private void on_new_page_saved (VisitTab tab) {
			/* First disconnect the signal so we won't be called
			 * again */
			tab.disconnect (new_tab_saved_connection_id);
			
			/* Create a new one and connect it */
			var l = new Label(_("New visit"));
			new_page = new VisitTab.with_patient (resource_manager, visit_page, patient, l);
			prepend_page (new_page, l);
			new_page.show_all();
			
			new_tab_saved_connection_id = new_page.saved.connect (on_new_page_saved);
		}
		
		/**
		 * @brief Focus the first widget on the visittab so the user
		 * can start editing. 
		 */
		public void focus_new_page () {
		    set_current_page (old_pages);
			new_page.grab_focus ();
		}
	}
	
}
