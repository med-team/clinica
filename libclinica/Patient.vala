/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 */
  
using Jansson;

namespace Clinica {
 
    public enum Gender {
        MALE,
        FEMALE,
    }

    public class Patient : Object {
    
        public DataProvider? provider = null; 
    
        public int64 id { get; set; default = 0; }
    
        public string given_name { get; set; }
        
        public string surname { get; set; }

        public DateTime birth_date { get; set; }
        
        public Gender gender { get; set; }
        
        public string phone { get; set; }
        
        public string residence_address { get; set; }
        
        public string identification_code { get; set; }
        
        public Doctor doctor { get; set; }
        
        public string get_complete_name () {
        	return string.join (" ", this.given_name, this.surname);
        }
        
        public VisitIterator visits (DateTime? start = null, DateTime? end = null) {
            if (provider == null)
                return new EmptyVisitIterator ();
            else
                return provider.visits (this, start, end);
        }
        
        internal Patient.from_json (Json object, DataProvider? provider = null) {
            
            this.provider = provider;
            id = object.object_get ("id").integer_value ();
            given_name = object.object_get ("given_name").string_value ();
            surname = object.object_get ("surname").string_value ();
            birth_date = SqliteDataProvider.string_to_datetime (
                object.object_get ("birth_date").string_value ());
            residence_address = object.object_get ("residence_address").string_value ();
            identification_code = object.object_get ("identification_code").string_value ();
            phone = object.object_get ("phone").string_value ();
            
            if (provider != null) {
                var doctor_id = object.object_get ("doctor").integer_value ();
                if (doctor_id != 0) {
                    doctor = provider.get_doctor (doctor_id);
                    
                    // Check if the doctor is removed later, and in that case
                    // update the patient accordingly. 
                    provider.doctor_removed.connect ((removed_id) => {
                        if (doctor_id == removed_id)
                            doctor = null;
                    });
                }
            }
        }
        
        internal Json to_json () {
            var object = Json.object ();
            
            object.object_set ("id", Json.integer (id));
            object.object_set ("phone", Json.string (phone));
            object.object_set ("given_name", Json.string (given_name));
            object.object_set ("surname", Json.string (surname));
            object.object_set ("birth_date", Json.string (
                SqliteDataProvider.datetime_to_string (birth_date)));
            object.object_set ("residence_address", Json.string (residence_address));
            object.object_set ("identification_code", Json.string (identification_code));
            
            if (doctor != null)
                object.object_set ("doctor", Json.integer (doctor.id));
            else 
                object.object_set ("doctor", Json.integer (0));
            
            return object;
        }
    
    }

}
