/*
 *   This file is part of Clinica.
 *
 *   Clinica is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Clinica is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Clinica.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Authors: Leonardo Robol <leo@robol.it>
 *            Gianmarco Brocchi <brocchi@poisson.phc.unipi.it>
 */

using Gtk;

namespace Clinica {

	/** 
	 * @brief A generic page of the visit browser
	 */
	public class VisitTab : Alignment {
	
		public class TextMultiLine : Box {
		
			private TextView view;
			public TextMultiLine (string name) {
				GLib.Object (orientation: Orientation.VERTICAL, spacing: 5);
				
				/*  TextView to hold the content inserted by the user */
				view = new TextView ();
				
				/* Tabs should cycle through the various fields */
				view.set_accepts_tab (false);
				view.wrap_mode = Gtk.WrapMode.WORD_CHAR;
				
				/* Resize views on focus: EXPERIMENTAL FEATURE */
				view.focus_in_event.connect  (on_view_focus_in_event);
				view.focus_out_event.connect (on_view_focus_out_event);
				
				/* Add scrolledwindow to the textview to allow scrolling
				 * when writing long texts.. */
				var scrolled_window = new ScrolledWindow (null, null);
				scrolled_window.add (view);
				scrolled_window.set_shadow_type (ShadowType.OUT);
				
				/* Create a frame to hold the description of the "field"
				 * that we are inserting */
				var frame = new Frame (name);
				frame.add (scrolled_window);
				
				/* No shadow because there are quite a lot of borders
				 * in this UI... */
				frame.set_shadow_type (ShadowType.NONE);
				
				pack_start (frame, true, true);
			}
			
			public bool on_view_focus_in_event(Gdk.EventFocus e) {
				view.set_size_request (-1, 200);
				set_size_request (-1, 200);
				return false;
			}
			
			public bool on_view_focus_out_event(Gdk.EventFocus e) {
				view.set_size_request(-1, -1);
				set_size_request (-1, -1);
				return false;
			}
			
			public string get_content () {
				TextBuffer buffer = view.get_buffer ();
				TextIter start, end;
				buffer.get_start_iter (out start);
				buffer.get_end_iter   (out end);
				return buffer.get_text (start, end, true);
			}
			
			public void set_content(string content) {
				TextBuffer buffer = view.get_buffer ();
				buffer.set_text(content, -1);
			}
			
			public new void grab_focus () {
				view.grab_focus ();
			}
		}
		
		/* true if this visit is created from scratch (not a old one) */
		bool new_visit;
	
		/* Visit this Tab relates to */
		internal Visit visit;
		
		/* Label with our title */
		private Label label;
		
		/*  VBox holding the structure */
		private Box  main_vbox;
		
		/* Entries holding the data of the visit */
		private TextMultiLine anamnesis;
		private TextMultiLine physical_examination;
		private TextMultiLine laboratory_exam;
		private TextMultiLine histopathology;
		private TextMultiLine diagnosis;
		private TextMultiLine topical_therapy;
		private TextMultiLine systemic_therapy;
		private TextMultiLine subsequent_checks;
		
		/* Visit store of the application */
		private VisitListStore store;
		
		/* VisitFileManager */
		private VisitFileManager file_manager;
		
		/* SIGNALS */
		public signal void saved();
		public signal void deleted();
		
		private ResourceManager resource_manager;
		
		private VisitPage visit_page;
	
		public VisitTab (ResourceManager resources, VisitPage visit_page, Visit? v, Label title) {
		    resource_manager = resources;
		    this.visit_page = visit_page;
			if (v != null) {
				new_visit = false;
				visit = v;
			} else {
				new_visit = true;
			}
			
			store = resource_manager.visit_list_store;
			
			/* Hold a reference to the label associated with this
			 * tab so we can change it as the content here changes... */
			label = title;
				
			/*  Add main VBox */
			main_vbox = new Box (Orientation.VERTICAL, 10);
			
			/* FileStore display */
   			file_manager = new VisitFileManager (resource_manager, visit_page, visit);    	    
			
			/* Pack widgets into the table */
			pack_widgets ();
			
			/*  Add VBox here */
			var scroller = new ScrolledWindow (null, null);
			
			/* We can pack the file displayer on the right side
			 * of the visit summary */
			var box = new Box (Orientation.HORIZONTAL, resource_manager.PADDING);
			box.pack_start (main_vbox, true, true);
			
			box.pack_start (file_manager, false, true);
							
			/*  Create an align to make the fields look good */					
			var align = new Alignment (0.5F, 0.5F, 1, 1);
			align.set_padding (5,5,5,5);
			align.add (box);
			scroller.add_with_viewport (align);
			add (scroller);
			
			
			/* If this is not a new visit load data in the MultiLineEdits */
			if (!new_visit)
				fill_data ();
				
		    /* Connect callbacks */
		    resource_manager.data_provider.visit_removed.connect (on_visit_removed);
		    
		    if (resource_manager.data_provider.get_file_store () == null)
		        file_manager.hide ();
		        
		    resource_manager.notify["data-provider"].connect (() => {
		        debug ("Closing VisitTab since the provider has changed");
		        resource_manager.user_interface.close_visit_window (visit.patient);
		    });
		}
		
		public VisitTab.with_patient (ResourceManager resources, VisitPage page, Patient p, Label title) {
			this (resources, page, null, title);
			visit = new Visit ();
			visit.patient = p;
		}
		
		/**
		 * @brief Get the visit id.
		 */
		public int64 get_visit_id () {
		    return visit.id;
		}
		
		/**
		 * @brief Callback to the visit_removed event in the liststore.
		 */
		private void on_visit_removed (int64 visit_id) {
		    VisitBrowser parent = this.parent as VisitBrowser;
		    if (visit_id == visit.id)
		        parent.remove_page (parent.page_num (this));
		}
		
		public void fill_data () {
			anamnesis.set_content (visit.anamnesis);
			physical_examination.set_content (visit.physical_examination);
			laboratory_exam.set_content (visit.laboratory_exam);
			histopathology.set_content (visit.histopathology);
			diagnosis.set_content (visit.diagnosis);
			topical_therapy.set_content (visit.topical_therapy);
			systemic_therapy.set_content (visit.systemic_therapy);
			subsequent_checks.set_content (visit.subsequent_checks);
			
			/* Since we are loading the data from the visit, than the visit is
			 * not a new one. */
			new_visit = false;
		}
		
		public new void grab_focus () {
			anamnesis.grab_focus ();
		}
		
		/**
		 * @brief Callback called when the user press the button
		 * "Save" in the tab
		 */
		public void save_visit () {
			/* First fill the field of the visit with
			 * the input of the user... */
			if (new_visit)
				visit.date = new DateTime.now_local ();
				
			visit.anamnesis = anamnesis.get_content ();
			visit.physical_examination = physical_examination.get_content ();
			visit.laboratory_exam = laboratory_exam.get_content ();
			visit.histopathology = histopathology.get_content ();
			visit.diagnosis = diagnosis.get_content ();
			visit.topical_therapy = topical_therapy.get_content ();
			visit.systemic_therapy = systemic_therapy.get_content ();
			visit.subsequent_checks = subsequent_checks.get_content ();
			 
			/* ..and then actually store that data in the database, changing
			 * the label of the tab to reflect the change...              */
			label.set_text (visit.date.format ("%F"));
			resource_manager.data_provider.save_visit (visit);
			
		    file_manager.update_visit (visit);
			
			/* Emit saved() signal */
			saved();
		}
		
		/**
		 * @brief Callback called when the user press the button
		 * "Delete" in the tab
		 */
		public void delete_visit () {
		    /* Ask before deleting a visit */
		    var dialog = new MessageDialog (resource_manager.user_interface.window, DialogFlags.DESTROY_WITH_PARENT | DialogFlags.MODAL, 
		                                    MessageType.QUESTION, ButtonsType.YES_NO, "%s", 
		                                    _("Deleting a visit will cause all its data to be lost.\nDo you really want to continue?"));
		    if (dialog.run () == ResponseType.YES) {
			    resource_manager.data_provider.remove_visit (visit);
			
			    /* Emit deleted() signal */
			    deleted();
			}
			
			dialog.destroy ();
		}
		
		/**
		 * @brief Pack entries into the table, after that
		 * is loaded.
		 */
		private void pack_widgets () {
		
			anamnesis = new TextMultiLine (_("Anamnesis"));
			main_vbox.pack_start (anamnesis);
			
			physical_examination = new TextMultiLine (_("Physical examination"));
			main_vbox.pack_start (physical_examination);
			
			laboratory_exam = new TextMultiLine (_("Laboratory exam"));
			main_vbox.pack_start (laboratory_exam);
			
			histopathology = new TextMultiLine (_("Hystopathology"));
			main_vbox.pack_start (histopathology);
			
			diagnosis = new TextMultiLine (_("Diagnosis"));
			main_vbox.pack_start (diagnosis);
			
			topical_therapy = new TextMultiLine (_("Topical therapy"));
			main_vbox.pack_start (topical_therapy);
			
			systemic_therapy = new TextMultiLine (_("Systemic therapy"));
			main_vbox.pack_start (systemic_therapy);
			
			subsequent_checks = new TextMultiLine (_("Subsequent checks"));
			main_vbox.pack_start (subsequent_checks);
		}
	}
	
}
